let section = window.innerWidth / 9

const defaultPlayersRed = {
    "0": {
        "0": {
            "number": 1,
            "position": {
                "x": section * 4,
                "y": window.innerHeight - 70
            }
        }
    },
    "1": {
        "0": {
            "number": 1,
            "position": {
                "x": section * 3,
                "y": window.innerHeight - 70
            }
        }
    },
    "2": {
        "0": {
            "number": 1,
            "position": {
                "x": section * 5,
                "y": window.innerHeight - 70
            }
        }
    },
    "3": {
        "0": {
            "number": 1,
            "position": {
                "x": section * 1,
                "y": window.innerHeight - 70
            }
        }
    },
    "4": {
        "0": {
            "number": 1,
            "position": {
                "x": section * 2,
                "y": window.innerHeight - 70
            }
        }
    },
    "5": {
        "0": {
            "number": 1,
            "position": {
                "x": section * 6,
                "y": window.innerHeight - 70
            }
        }
    },
    "6": {
        "0": {
            "number": 1,
            "position": {
                "x": section * 7,
                "y": window.innerHeight - 70
            }
        }
    },
    "7": {
        "0": {
            "number": 1,
            "position": {
                "x": section * 8,
                "y": window.innerHeight - 70
            }
        }
    }
}

export default defaultPlayersRed