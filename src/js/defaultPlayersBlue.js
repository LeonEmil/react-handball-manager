let section = window.innerWidth / 9

const defaultPlayersBlue = {
    "0": {
        "0": {
            "number": 1,
            "position": {
                "x": section * 4,
                "y": 20
            }
        }
    },
    "1": {
        "0": {
            "number": 1,
            "position": {
                "x": section * 3,
                "y": 20
            }
        }
    },
    "2": {
        "0": {
            "number": 1,
            "position": {
                "x": section * 5,
                "y": 20
            }
        }
    },
    "3": {
        "0": {
            "number": 1,
            "position": {
                "x": section * 1,
                "y": 20
            }
        }
    },
    "4": {
        "0": {
            "number": 1,
            "position": {
                "x": section * 2,
                "y": 20
            }
        }
    },
    "5": {
        "0": {
            "number": 1,
            "position": {
                "x": section * 6,
                "y": 20
            }
        }
    },
    "6": {
        "0": {
            "number": 1,
            "position": {
                "x": section * 7,
                "y": 20
            }
        }
    },
    "7": {
        "0": {
            "number": 1,
            "position": {
                "x": section * 8,
                "y": 20
            }
        }
    }
}

export default defaultPlayersBlue