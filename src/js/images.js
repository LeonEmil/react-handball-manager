let configButton1Active = 'https://res.cloudinary.com/leonemil/image/upload/v1604867806/Handball%20manager/config-button-1-active_u5f3dz.png'
let configButton1Inactive = 'https://res.cloudinary.com/leonemil/image/upload/v1604867810/Handball%20manager/config-button-1-inactive_covj6j.png'
let configButton2Active = 'https://res.cloudinary.com/leonemil/image/upload/v1604845456/Handball%20manager/config-button-2-active_voefe3.png'
let configButton2Inactive = 'https://res.cloudinary.com/leonemil/image/upload/v1604845460/Handball%20manager/config-button-2-inactive_jot9kr.png'

export { 
    configButton1Active,
    configButton1Inactive,
    configButton2Active,
    configButton2Inactive
}