import { togglePanel } from './../redux/actionCreators'
import { auth } from './../firebase/firebase'
import store from './../redux/store'

let menuPanelList = [
    {
        name: 'Estrategías guardadas',
        action: () => { store.dispatch(togglePanel(0)) },
    },
    {
        name: 'Jugadores',
        action: () => { store.dispatch(togglePanel(2)) }
    },
    {
        name: 'Entrenamientos',
        action: () => { store.dispatch(togglePanel(4)) }
    },
    {
        name: 'Enviar mail',
        action: () => { store.dispatch(togglePanel(5)) }
    },
    {
        name: 'Usuario',
        action: () => { store.dispatch(togglePanel(7)) }
    },
    {
        name: 'Configuración',
        action: () => { store.dispatch(togglePanel(6)) }
    },
    {
        name: 'Salir',
        action: () => { auth.signOut() }
    },
    // {
    //     name: '',
    //     action: () => { }
    // },
]

export default menuPanelList