const strategyPanel = () => {
    const root = document.getElementById("root")
    const strategyPanelFragment = document.createDocumentFragment()
    const strategyPanel = document.createElement("div")
    const strategyPanelList = document.createElement("ul")
    const strategyPanelItem = document.createElement("li")

    strategyPanel.style.width = "500px"
    strategyPanel.style.height = "700px"
    strategyPanel.style.backgroundColor = "white"

    root.append(strategyPanel)

    return strategyPanel
}

export { strategyPanel }