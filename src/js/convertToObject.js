

const isObject = (obj) => {
	if(typeof obj === "object" && Array.isArray(obj) === false){
		return true
	}
	else {
		return false
	}
}

const toObject = (obj) => {
	let keys = Object.keys(obj)
    
	keys.forEach(key => {
		if(Array.isArray(obj[`${key}`])){
			toObject(obj[`${key}`])
			obj[`${key}`] = ArrayObject.assign(new ArrayObject, obj[`${key}`])
		}
		if(isObject(obj[key])){
			toObject(obj[key])
		}
	})

	return obj
}


// let obj = {
// 	lines: [0, 1, 2],
// 	players: ["Matt", "Fred", "Willaim", ["black", "white",{array: []}]],
// 	data: {name: "string", index: ['a', 'b', 'c', []]}
// }

// class ArrayObject extends Object {
// 	constructor(){
// 		super()
// 	}
// }


// const toArray = (obj) => {
// 	let keys = Object.keys(obj)
// 	keys.forEach(key => {
// 		if(obj[key] instanceof ArrayObject){
// 			toArray(obj[key])
			
// 			obj[`${key}`] = Object.values(obj[`${key}`])
// 			console.log("Array detectado//")
// 		}
// 	})
// 	return obj
// }

// console.log(toArray(toObject(obj)))