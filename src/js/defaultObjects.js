const section = (window.innerHeight - 60) / 13

const defaultObjects = {
    "0": {
        "0": {
            "position": {
                "x": 30,
                "y": section
            }
        }
    },
    "1": {
        "0": {
            "position": {
                "x": 30,
                "y": section * 2
            }
        }
    },
    "2": {
        "0": {
            "position": {
                "x": 30,
                "y": section * 3
            }
        }
    },
    "3": {
        "0": {
            "position": {
                "x": 30,
                "y": section * 4
            }
        }
    },
    "4": {
        "0": {
            "position": {
                "x": 30,
                "y": section * 5
            }
        }
    },
    "5": {
        "0": {
            "position": {
                "x": 30,
                "y": section * 6
            }
        }
    },
    "6": {
        "0": {
            "position": {
                "x": 30,
                "y": section * 7
            }
        }
    },
    "7": {
        "0": {
            "position": {
                "x": 30,
                "y": section * 8
            }
        }
    },
    "8": {
        "0": {
            "position": {
                "x": 30,
                "y": section * 9
            }
        }
    },
    "9": {
        "0": {
            "position": {
                "x": 30,
                "y": section * 10
            }
        }
    },
    "10": {
        "0": {
            "position": {
                "x": 30,
                "y": section * 11
            }
        }
    },
    "11": {
        "0": {
            "position": {
                "x": 30,
                "y": section * 12
            }
        }
    },
    "12": {
        "0": {
            "position": {
                "x": 30,
                "y": section * 13
            }
        }
    },
    "13": {
        "0": {
            "position": {
                "x": window.innerWidth - 120,
                "y": section
            }
        }
    },
    "14": {
        "0": {
            "position": {
                "x": window.innerWidth - 120,
                "y": section * 2
            }
        }
    },
    "15": {
        "0": {
            "position": {
                "x": window.innerWidth - 120,
                "y": section * 3
            }
        }
    },
    "16": {
        "0": {
            "position": {
                "x": window.innerWidth - 120,
                "y": section * 4
            }
        }
    },
    "17": {
        "0": {
            "position": {
                "x": window.innerWidth - 120,
                "y": section * 5
            }
        }
    },
    "18": {
        "0": {
            "position": {
                "x": window.innerWidth - 120,
                "y": section * 6
            }
        }
    },
    "19": {
        "0": {
            "position": {
                "x": window.innerWidth - 120,
                "y": section * 7
            }
        }
    },
    "20": {
        "0": {
            "position": {
                "x": window.innerWidth - 120,
                "y": section * 8
            }
        }
    },
    "21": {
        "0": {
            "position": {
                "x": window.innerWidth - 120,
                "y": section * 9
            }
        }
    },
    "22": {
        "0": {
            "position": {
                "x": window.innerWidth - 120,
                "y": section * 10
            }
        }
    },
    "23": {
        "0": {
            "position": {
                "x": window.innerWidth - 120,
                "y": section * 11
            }
        }
    },
    "24": {
        "0": {
            "position": {
                "x": window.innerWidth - 120,
                "y": section * 12
            }
        }
    }
}

export default defaultObjects