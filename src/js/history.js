let section = window.innerWidth / 9
let customSectionWidth = window.innerWidth / 24
let customSectionHeight

const circleBlue = new Image()
const circleRed = new Image()

window.addEventListener("load", () => {
    circleBlue.src = "https://res.cloudinary.com/leonemil/image/upload/v1602549511/Handball%20manager/player-blue-1_j6ofta.png"
    circleRed.src = "https://res.cloudinary.com/leonemil/image/upload/v1602549510/Handball%20manager/Player-red-1_bmexdy.png"
})


const initialHistory = [
    {
        "lines": {},
        "defaultPlayersBlue": {
            "0": {},
            "1": {},
            "2": {},
            "3": {},
            "4": {},
            "5": {},
            "6": {},
            "7": {}
        },
        "defaultPlayersRed": {
            "0": {},
            "1": {},
            "2": {},
            "3": {},
            "4": {},
            "5": {},
            "6": {},
            "7": {}
        },
        "defaultObjects": {
            "0": {},
            "1": {},
            "2": {},
            "3": {},
            "4": {},
            "5": {},
            "6": {},
            "7": {},
            "8": {},
            "9": {},
            "10": {},
            "11": {},
            "12": {},
            "13": {},
            "14": {},
            "15": {},
            "16": {},
            "17": {},
            "18": {},
            "19": {},
            "20": {},
            "21": {},
            "22": {},
            "23": {},
            "24": {}
        },
        "customAttackTeam": {
            "teamName": "",
            "players": {
                
            }
        },
        "customDefenseTeam": {
            "teamName": "",
            "players": {
                
            }
        }
    },
    {
        "lines": {},
        "defaultPlayersBlue": {
            "0": {
                "0": {
                    "number": 1,
                    "position": {
                        "x": section * 4,
                        "y": 20
                    }
                },
                "1": {
                    "number": 2,
                    "position": {
                        "x": section * 4,
                        "y": 20
                    }
                },
                "2": {
                    "number": 3,
                    "position": {
                        "x": section * 4,
                        "y": 20
                    }
                },
                "3": {
                    "number": 4,
                    "position": {
                        "x": section * 4,
                        "y": 20
                    }
                },
                "4": {
                    "number": 5,
                    "position": {
                        "x": section * 4,
                        "y": 20
                    }
                },
                "5": {
                    "number": 6,
                    "position": {
                        "x": section * 4,
                        "y": 20
                    }
                },
                "6": {
                    "number": 7,
                    "position": {
                        "x": section * 4,
                        "y": 20
                    }
                }
            },
            "1": {},
            "2": {},
            "3": {},
            "4": {},
            "5": {},
            "6": {},
            "7": {}
        },
        "defaultPlayersRed": {
            "0": {
                "0": {
                    "number": 1,
                    "position": {
                        "x": section * 4,
                        "y": window.innerHeight - 70
                    }
                },
                "1": {
                    "number": 2,
                    "position": {
                        "x": section * 4,
                        "y": window.innerHeight - 70
                    }
                },
                "2": {
                    "number": 3,
                    "position": {
                        "x": section * 4,
                        "y": window.innerHeight - 70
                    }
                },
                "3": {
                    "number": 4,
                    "position": {
                        "x": section * 4,
                        "y": window.innerHeight - 70
                    }
                },
                "4": {
                    "number": 5,
                    "position": {
                        "x": section * 4,
                        "y": window.innerHeight - 70
                    }
                },
                "5": {
                    "number": 6,
                    "position": {
                        "x": section * 4,
                        "y": window.innerHeight - 70
                    }
                },
                "6": {
                    "number": 7,
                    "position": {
                        "x": section * 4,
                        "y": window.innerHeight - 70
                    }
                }
            },
            "1": {},
            "2": {},
            "3": {},
            "4": {},
            "5": {},
            "6": {},
            "7": {}
        },
        "customAttackTeam": {
            "teamName": "",
            "players": {
                
            }
        },
        "customDefenseTeam": {
            "teamName": "",
            "players": {
                
            }
        },
        "defaultObjects": {
            "0": {},
            "1": {},
            "2": {},
            "3": {},
            "4": {},
            "5": {},
            "6": {},
            "7": {},
            "8": {},
            "9": {},
            "10": {},
            "11": {},
            "12": {},
            "13": {},
            "14": {},
            "15": {},
            "16": {},
            "17": {},
            "18": {},
            "19": {},
            "20": {},
            "21": {},
            "22": {},
            "23": {},
            "24": {}
        }
    }
]

export default initialHistory