import * as actions from './actionTypes'
import defaultPlayersBlue from './../js/defaultPlayersBlue'
import defaultPlayersRed from './../js/defaultPlayersRed'
import defaultObjects from './../js/defaultObjects'
import initialHistory from '../js/history'
import { produce } from 'immer'
import { db } from './../firebase/firebase'

const initialState = {
    history: initialHistory,
    historyIndex: 1,
    panels: [
        {
            panelName: "Strategies",
            showPanel: false,
        },
        {
            panelName: "Draw",
            showPanel: false,
        },
        {
            panelName: "Players",
            showPanel: false,
        },
        {
            panelName: "Send",
            showPanel: false,
        },
        {
            panelName: "Objects",
            showPanel: false,
        },
        {
            panelName: "Mail",
            showPanel: false,
        },
        {
            panelName: "Config",
            showPanel: false,
        },
        {
            panelName: "User",
            showPanel: false,
        },
        {
            panelName: "Video",
            showPanel: false,
        },
        {
            panelName: "Menu",
            showPanel: false,
        },
    ],
    recicleByn: false,
    eraser: false,
    enableDraw: false,
    isDrawing: false,
    currentObjectSize: undefined,
    currentLineType: "Line 1",
    currentLineColor: "black",
    currentBackgroundPath: "https://res.cloudinary.com/leonemil/image/upload/",
    currentBackgroundAngle: "a_0",
    currentBackgroundCanvas: `/v1602549201/Handball%20manager/canvas-1_rbigwq.jpg`,
    currentUser: {
        name: "",
        email: "",
        avatar: "",
        strategies: [],
        videos: [],
        customTeams: [],
        myTeam: {},
        otherData: [],
        id: ""
    },
    configData: {
        bannerUrl: "",
        bannerAlt: "",
        logoUrl: ""
    },
    selectedCustomTeam: undefined,
    userDatabase: {},
    userDatabaseId: '',
    usersData: []
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actions.CHANGE_LINE_COLOR:
            return {
                ...state,
                currentLineColor: action.color,
            }

        case actions.CHANGE_LINE_TYPE:
            return {
                ...state,
                currentLineType: action.lineType
            }

        case actions.SET_ENABLE_DRAW:
            return {
                ...state,
                enableDraw: action.boolean
            }

        case actions.TOGGLE_DRAWING:
            return {
                ...state,
                isDrawing: action.boolean
            }

        case actions.CREATE_LINE:
            let newHistory = state.history.slice()
            let newLines = Object.values(newHistory[state.historyIndex].lines).slice()
            newLines.splice(state.historyIndex + 1, newHistory.length - state.historyIndex + 1, action.line)
            let newHistoryObject = newHistory[state.historyIndex]
            const produceFunction = (newHistoryObject) => {
                return produce(newHistoryObject, (updated) => {
                    updated.lines = newLines
                })
            }
            newHistory.splice(state.historyIndex + 1, newHistory.length - state.historyIndex + 1, produceFunction(newHistoryObject))
            db.collection("users").where('email', '==', state.currentUser.email).get().then(response => {
                    let id = response.docs[0].id
                    let doc = response.docs[0].data()
                    doc.history = newHistory
                    db.collection('users').doc(id).update({
                        historyIndex: state.historyIndex + 1,
                        history: newHistory
                    })
                }
            )
            .catch(error => {
                alert(error)
            })

            return {
                ...state,
                history: newHistory,
                historyIndex: state.historyIndex + 1
            }

        case actions.SET_LINE:
            let newSetHistory = state.history.slice()
            newSetHistory[newSetHistory.length - 1] = produce(newSetHistory[newSetHistory.length - 1], updatedState => {
                updatedState.lines[updatedState.lines.length - 1].linePoints = [...updatedState.lines[updatedState.lines.length - 1].linePoints, action.x, action.y]
            })
            return {
                ...state,
                history: newSetHistory
            }

        case actions.ADD_STRATEGY:
            let currentUserWithNewStrategy = Object.assign({}, state.currentUser)
            currentUserWithNewStrategy.strategies.push(action.strategy)
            return {
                ...state,
                currentUser: currentUserWithNewStrategy
            }
        
        case actions.CLEAN_CANVAS:
            return {
                ...state,
                historyIndex: 0
            }

        case actions.SET_ERASER:
            return {
                ...state,
                eraser: action.boolean
            }

        case actions.TOGGLE_RECICLE_BYN:
            return {
                ...state,
                recicleByn: action.boolean
            }

        case actions.HIDE_TRANSFORM_BOX:
            const newTransformRef = state.transformRef
            newTransformRef.current.nodes([]);
            newTransformRef.current.getLayer().batchDraw();

            return {
                ...state,
                transformRef: newTransformRef
            }

        case actions.RESTORE_HISTORY:
            return {
                ...state,
                history: action.history,
                historyIndex: action.historyIndex
            }

        case actions.UNDO:
            let historyIndex = state.historyIndex
            historyIndex = historyIndex - 1
            if (historyIndex < 0) {
                historyIndex = historyIndex + 1
            }
            return {
                ...state,
                historyIndex: historyIndex
            }

        case actions.REDO:
            let historyLength = state.history.length
            let history = state.historyIndex
            history = history + 1
            if(history >= historyLength){
                history = history - 1
            }
            return {
                ...state,
                historyIndex: history
            }

        case actions.CHANGE_BACKGROUND:
            return {
                ...state,
                currentBackgroundCanvas: action.background
            }

        case actions.CHANGE_BACKGROUND_ANGLE:
            return {
                ...state,
                currentBackgroundAngle: action.backgroundAngle
            }

        case actions.TOGGLE_DEFAULT_PLAYERS_BLUE:
            let newDefaultPlayersBlue = Object.assign({}, state.history[state.historyIndex].defaultPlayersBlue)
            newDefaultPlayersBlue[action.playersType] = Object.assign({}, Object.values(newDefaultPlayersBlue[action.playersType]).concat(Object.values(defaultPlayersBlue[action.playersType])))
            let newHistoryWithPlayersBlue = state.history.slice()
            let newPlayersBlueHistory = produce(newHistoryWithPlayersBlue[state.historyIndex], playersBlueHistoryUpdated => {
                playersBlueHistoryUpdated.defaultPlayersBlue = newDefaultPlayersBlue
            })
            newHistoryWithPlayersBlue.splice(state.historyIndex + 1, newHistoryWithPlayersBlue.length - state.historyIndex + 1, newPlayersBlueHistory)

            db.collection("users").where('email', '==', state.currentUser.email).get().then(response => {
                let id = response.docs[0].id
                db.collection('users').doc(id).update({
                    historyIndex: state.historyIndex + 1,
                    history: newHistoryWithPlayersBlue
                })
            })

            return {
                ...state,
                history: newHistoryWithPlayersBlue,
                historyIndex: state.historyIndex + 1
            }

        case actions.TOGGLE_DEFAULT_PLAYERS_RED:
            let newDefaultPlayersRed = Object.assign({}, state.history[state.historyIndex].defaultPlayersRed)
            newDefaultPlayersRed[action.playersType] = Object.assign({}, Object.values(newDefaultPlayersRed[action.playersType]).concat(Object.values(defaultPlayersRed[action.playersType])))
            let newHistoryWithPlayersRed = state.history.slice()
            let newPlayersRedHistory = produce(newHistoryWithPlayersRed[state.historyIndex], playersRedHistoryUpdated => {
                playersRedHistoryUpdated.defaultPlayersRed = newDefaultPlayersRed
            })
            newHistoryWithPlayersRed.splice(state.historyIndex + 1, newHistoryWithPlayersRed.length - state.historyIndex + 1, newPlayersRedHistory)

            db.collection("users").where('email', '==', state.currentUser.email).get().then(response => {
                let id = response.docs[0].id
                db.collection('users').doc(id).update({
                    historyIndex: state.historyIndex + 1,
                    history: newHistoryWithPlayersRed
                })
            })

            return {
                ...state,
                history: newHistoryWithPlayersRed,
                historyIndex: state.historyIndex + 1
            }

        case actions.TOGGLE_DEFAULT_OBJECTS:
            let newDefaultObjects = Object.assign({}, state.history[state.historyIndex].defaultObjects)
            newDefaultObjects[action.objectType] = Object.assign({}, Object.values(newDefaultObjects[action.objectType]).concat(Object.values(defaultObjects[action.objectType])))
            let newHistoryWithObjects = state.history.slice()
            let newObjectHistory3 = produce(newHistoryWithObjects[state.historyIndex], objectHistoryUpdated => {
                objectHistoryUpdated.defaultObjects = newDefaultObjects
            })
            newHistoryWithObjects.splice(state.historyIndex + 1, newHistoryWithObjects.length - state.historyIndex + 1, newObjectHistory3)

            db.collection("users").where('email', '==', state.currentUser.email).get().then(response => {
                let id = response.docs[0].id
                db.collection('users').doc(id).update({
                    historyIndex: state.historyIndex + 1,
                    history: newHistoryWithObjects
                })
            })

            return {
                ...state,
                history: newHistoryWithObjects,
                historyIndex: state.historyIndex + 1
            }

        case actions.ADD_SELECTED_CUSTOM_TEAM:
            return {
                ...state,
                selectedCustomTeam: action.team
            }
        
        case actions.ADD_MY_TEAM:
            let user = {
                ...state.currentUser,
                myTeam: action.team
            }
            return {
                ...state,
                currentUser: user
            }

            case actions.ADD_CUSTOM_TEAM:
                let emptyPlayersBlue = Object.assign({}, state.history[0].defaultPlayersRed)
                let emptyPlayersRed = Object.assign({}, state.history[0].defaultPlayersBlue)
                let newHistoryWithEmptyPlayers = state.history.slice()
    
                if(action.teamSelected === "customAttackTeam"){
                    let newCustomAttackTeam = Object.assign({}, action.customTeam)
                    for(let i = 0; i <= Object.keys(newCustomAttackTeam.players).length - 1; i++){
                        newCustomAttackTeam.players[i].position.x = newCustomAttackTeam.players[i].initial && newCustomAttackTeam.players[i].role === "Portero" ? (window.innerWidth / 25) * 1 :
                                                                    newCustomAttackTeam.players[i].initial && newCustomAttackTeam.players[i].role === "Central" ? (window.innerWidth / 25) * 10 :
                                                                    newCustomAttackTeam.players[i].initial && newCustomAttackTeam.players[i].role === "Pivote" ? (window.innerWidth / 25) * 3 :
                                                                    newCustomAttackTeam.players[i].initial && newCustomAttackTeam.players[i].role === "Extremo izquierdo" ? (window.innerWidth / 25) * 8 :
                                                                    newCustomAttackTeam.players[i].initial && newCustomAttackTeam.players[i].role === "Extremo derecho" ? (window.innerWidth / 25) * 8 :
                                                                    newCustomAttackTeam.players[i].initial && newCustomAttackTeam.players[i].role === "Lateral izquierdo" ? (window.innerWidth / 25) * 4 :
                                                                    newCustomAttackTeam.players[i].initial && newCustomAttackTeam.players[i].role === "Lateral derecho" ? (window.innerWidth / 25) * 4 :
                                                                    (window.innerWidth / 25) * i
                        newCustomAttackTeam.players[i].position.y = newCustomAttackTeam.players[i].initial && newCustomAttackTeam.players[i].role === "Portero" ? (window.innerHeight / 2) - 50 :
                                                                    newCustomAttackTeam.players[i].initial && newCustomAttackTeam.players[i].role === "Central" ? (window.innerHeight / 2) - 50 :
                                                                    newCustomAttackTeam.players[i].initial && newCustomAttackTeam.players[i].role === "Pivote" ? (window.innerHeight / 2) - 50 :
                                                                    newCustomAttackTeam.players[i].initial && newCustomAttackTeam.players[i].role === "Extremo izquierdo" ? 120 :
                                                                    newCustomAttackTeam.players[i].initial && newCustomAttackTeam.players[i].role === "Extremo derecho" ? window.innerHeight - 220 :
                                                                    newCustomAttackTeam.players[i].initial && newCustomAttackTeam.players[i].role === "Lateral izquierdo" ? 150 :
                                                                    newCustomAttackTeam.players[i].initial && newCustomAttackTeam.players[i].role === "Lateral derecho" ? window.innerHeight - 240 :
                                                                    20
                    }
                    let newEmptyPlayersHistory
                    if(action.customTeam.teamName === state.history[state.historyIndex].customDefenseTeam.teamName){
                        newEmptyPlayersHistory = produce(newHistoryWithEmptyPlayers[state.historyIndex], customDefenseTeamHistoryUpdated => {
                            customDefenseTeamHistoryUpdated.defaultPlayersBlue = emptyPlayersBlue;
                            customDefenseTeamHistoryUpdated.defaultPlayersRed = emptyPlayersRed
                            customDefenseTeamHistoryUpdated.customAttackTeam = newCustomAttackTeam
                            customDefenseTeamHistoryUpdated.customDefenseTeam = {
                                "teamName": "",
                                "players": {
                                    
                                }
                            }    
                        })
                    }
                    else {
                        newEmptyPlayersHistory = produce(newHistoryWithEmptyPlayers[state.historyIndex], customDefenseTeamHistoryUpdated => {
                            customDefenseTeamHistoryUpdated.defaultPlayersBlue = emptyPlayersBlue;
                            customDefenseTeamHistoryUpdated.defaultPlayersRed = emptyPlayersRed
                            customDefenseTeamHistoryUpdated.customAttackTeam = newCustomAttackTeam
                        })
                    }
                    newHistoryWithEmptyPlayers.splice(state.historyIndex + 1, newHistoryWithEmptyPlayers.length - state.historyIndex + 1, newEmptyPlayersHistory)
        
                    db.collection("users").where('email', '==', state.currentUser.email).get().then(response => {
                        let id = response.docs[0].id
                        db.collection('users').doc(id).update({
                            historyIndex: state.historyIndex + 1,
                            history: newHistoryWithEmptyPlayers
                        })
                    })
    
                    return {
                        ...state,
                        customDefenseTeam: action.customTeam,
                        history: newHistoryWithEmptyPlayers,
                        historyIndex: state.historyIndex +1
                    }
                }
                if(action.teamSelected === "customDefenseTeam"){
                    let newCustomDefenseTeam = Object.assign({}, action.customTeam)
                    for(let i = 0; i <= Object.keys(newCustomDefenseTeam.players).length - 1; i++){
                        newCustomDefenseTeam.players[i].position.x = newCustomDefenseTeam.players[i].initial && newCustomDefenseTeam.players[i].role === "Portero" ? (window.innerWidth / 25) * 22 :
                                                                newCustomDefenseTeam.players[i].initial && newCustomDefenseTeam.players[i].role === "Central" ? (window.innerWidth / 25) * 13 :
                                                                newCustomDefenseTeam.players[i].initial && newCustomDefenseTeam.players[i].role === "Pivote" ? (window.innerWidth / 25) * 20 :
                                                                newCustomDefenseTeam.players[i].initial && newCustomDefenseTeam.players[i].role === "Extremo izquierdo" ? (window.innerWidth / 25) * 14 :
                                                                newCustomDefenseTeam.players[i].initial && newCustomDefenseTeam.players[i].role === "Extremo derecho" ? (window.innerWidth / 25) * 14 :
                                                                newCustomDefenseTeam.players[i].initial && newCustomDefenseTeam.players[i].role === "Lateral izquierdo" ? (window.innerWidth / 25) * 19 :
                                                                newCustomDefenseTeam.players[i].initial && newCustomDefenseTeam.players[i].role === "Lateral derecho" ? (window.innerWidth / 25) * 19 :
                                                                (window.innerWidth / 25) * i
                        newCustomDefenseTeam.players[i].position.y = newCustomDefenseTeam.players[i].initial && newCustomDefenseTeam.players[i].role === "Portero" ? (window.innerHeight / 2) - 50 :
                                                                newCustomDefenseTeam.players[i].initial && newCustomDefenseTeam.players[i].role === "Central" ? (window.innerHeight / 2) - 50 :
                                                                newCustomDefenseTeam.players[i].initial && newCustomDefenseTeam.players[i].role === "Pivote" ? (window.innerHeight / 2) - 50 :
                                                                newCustomDefenseTeam.players[i].initial && newCustomDefenseTeam.players[i].role === "Extremo izquierdo" ? 120 :
                                                                newCustomDefenseTeam.players[i].initial && newCustomDefenseTeam.players[i].role === "Extremo derecho" ? window.innerHeight - 220 :
                                                                newCustomDefenseTeam.players[i].initial && newCustomDefenseTeam.players[i].role === "Lateral izquierdo" ? 150 :
                                                                newCustomDefenseTeam.players[i].initial && newCustomDefenseTeam.players[i].role === "Lateral derecho" ? window.innerHeight - 240 :
                                                                window.innerHeight - 70
                    }
                    let newEmptyPlayersHistory
                    if(action.customTeam.teamName === state.history[state.historyIndex].customAttackTeam.teamName){
                        newEmptyPlayersHistory = produce(newHistoryWithEmptyPlayers[state.historyIndex], customDefenseTeamHistoryUpdated => {
                            customDefenseTeamHistoryUpdated.defaultPlayersBlue = emptyPlayersBlue;
                            customDefenseTeamHistoryUpdated.defaultPlayersRed = emptyPlayersRed
                            customDefenseTeamHistoryUpdated.customDefenseTeam = newCustomDefenseTeam
                            customDefenseTeamHistoryUpdated.customAttackTeam = {
                                "teamName": "",
                                "players": {
                                    
                                }
                            } 
                        })
                    }
                    else {
                        newEmptyPlayersHistory = produce(newHistoryWithEmptyPlayers[state.historyIndex], customDefenseTeamHistoryUpdated => {
                            customDefenseTeamHistoryUpdated.defaultPlayersBlue = emptyPlayersBlue;
                            customDefenseTeamHistoryUpdated.defaultPlayersRed = emptyPlayersRed
                            customDefenseTeamHistoryUpdated.customDefenseTeam = newCustomDefenseTeam
                        })
                    }
                    newHistoryWithEmptyPlayers.splice(state.historyIndex + 1, newHistoryWithEmptyPlayers.length - state.historyIndex + 1, newEmptyPlayersHistory)
        
                    db.collection("users").where('email', '==', state.currentUser.email).get().then(response => {
                        let id = response.docs[0].id
                        db.collection('users').doc(id).update({
                            historyIndex: state.historyIndex + 1,
                            history: newHistoryWithEmptyPlayers
                        })
                    })
    
                    return {
                        ...state,
                        customDefenseTeam: action.customTeam,
                        history: newHistoryWithEmptyPlayers,
                        historyIndex: state.historyIndex +1
                    }
                }
    

        case actions.UPDATE_OBJECT_POSITION:
            console.log(action)
            let historyWithUpdatedPositions = state.history.slice()
            console.log(historyWithUpdatedPositions)
            console.log(historyWithUpdatedPositions[state.historyIndex])
            let lastHistoryObject = produce(historyWithUpdatedPositions[state.historyIndex], historyUpdated => {
                console.log(historyUpdated)
                if(action.objectType === "customAttackTeam" || action.objectType === "customDefenseTeam"){
                    historyUpdated[action.objectType]["players"][action.elementId].position.x = action.x 
                    historyUpdated[action.objectType]["players"][action.elementId].position.y = action.y 
                }
                else {
                    historyUpdated[action.objectType][action.typeId][action.elementId].position.x = action.x 
                    historyUpdated[action.objectType][action.typeId][action.elementId].position.y = action.y 
                }
            })
            historyWithUpdatedPositions.splice(state.historyIndex + 1, historyWithUpdatedPositions.length - state.historyIndex + 1, lastHistoryObject)

            db.collection("users").where('email', '==', state.currentUser.email).get().then(response => {
                let id = response.docs[0].id
                db.collection('users').doc(id).update({
                    historyIndex: state.historyIndex + 1,
                    history: historyWithUpdatedPositions
                })
            })
            .catch(error => {
                alert(error)
            })

            return {
                ...state,
                history: historyWithUpdatedPositions,
                historyIndex: state.historyIndex + 1
            }

        case actions.UPDATE_CUSTOM_PLAYER_POSITION:
            let historyWithUpdatedCustomPlayerPositions = state.history.slice()
            let lastHistoryCustomPlayer = produce(historyWithUpdatedCustomPlayerPositions[state.historyIndex], customHistoryUpdated => {
                customHistoryUpdated[action.objectType][action.elementId]["players"]["position"]["x"] = action.x 
                customHistoryUpdated[action.objectType][action.elementId]["players"]["position"]["y"] = action.y 
            })
            historyWithUpdatedCustomPlayerPositions.splice(state.historyIndex + 1, historyWithUpdatedCustomPlayerPositions.length - state.historyIndex + 1, lastHistoryCustomPlayer)

            db.collection("users").where('email', '==', state.currentUser.email).get().then(response => {
                let id = response.docs[0].id
                db.collection('users').doc(id).update({
                    historyIndex: state.historyIndex + 1,
                    history: historyWithUpdatedCustomPlayerPositions
                })
            })

            return {
                ...state,
                history: historyWithUpdatedCustomPlayerPositions,
                historyIndex: state.historyIndex + 1
            }

        case actions.UPDATE_EDITED_TEAM:
            let newHistoryWithUpdatedEditedTeam = state.history.slice()
            let lastHistoryWithUpdatedEditedTeam = produce(newHistoryWithUpdatedEditedTeam[newHistoryWithUpdatedEditedTeam.lenght - 1], updatedEditedTeam => {
                updatedEditedTeam[action.teamType] = action.team
            })
            newHistoryWithUpdatedEditedTeam.splice(state.historyIndex + 1, newHistoryWithUpdatedEditedTeam.length - 1, lastHistoryWithUpdatedEditedTeam)

            return {
                history: newHistoryWithUpdatedEditedTeam,
                historyIndex: state.historyIndex + 1
            }

        case actions.DELETE_CUSTOM_TEAM:            
            let newDeleteCustomTeamHistory = produce(state.history, newHistoryUpdated => {
                newHistoryUpdated.push(newHistoryUpdated[newHistoryUpdated.length - 1])
                newHistoryUpdated[newHistoryUpdated.length - 1][action.teamType] = {
                    "teamName": "",
                    "players": {
                        
                    }
                }
            })

            return {
                history: newDeleteCustomTeamHistory,
                historyIndex: state.historyIndex + 1
            }

        case actions.EDIT_STRATEGY:
            let updatedHistory = state.history.slice()
            updatedHistory.splice(state.historyIndex + 1, 0, action.strategy)

            return {
                ...state,
                history: updatedHistory,
                historyIndex: state.historyIndex + 1
            }

        case actions.SET_USER:
            return {
                ...state,
                currentUser: action.currentUser
            }

        case actions.SET_CONFIG_DATA:
            return {
                ...state,
                configData: action.configData
            }

        case actions.SET_USERS_DATA:
            return {
                ...state,
                usersData: action.usersData
            }
            
        case actions.ADD_USER_DATABASE:
            return {
                ...state,
                userDatabase: action.userDatabase
            }

        case actions.ADD_USER_DATABASE_ID:
            return {
                ...state,
                userDatabaseId: action.userDatabaseId
            }

        case actions.TOGGLE_PANEL:
            let newPanels = state.panels.slice()
            newPanels.forEach((panel, id) => {
                if(id !== action.panel){
                    return (
                        panel.panelName,
                        panel.showPanel = false
                    )
                }
                else {
                    return (
                        panel.panelName,
                        panel.showPanel = panel.showPanel ? false : true
                    )
                }
            })
            return {
                ...state,
                panels: newPanels
            }

        case actions.CHANGE_OBJECT_SIZE: 
            return {
                ...state,
                currentObjectSize: action.objectSize,
            }
            
    
        default: return state
    }
}

export default reducer