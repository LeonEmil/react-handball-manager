import * as actions from './actionTypes'

// Activa o desactiva los paneles de las opciones de los menus
const togglePanel = (panel) => {
    return {
        type: actions.TOGGLE_PANEL,
        panel: panel
    }
}

// Guarda una estrategia
const addStrategy = (name, url, metadata) => {
    return {
        type: actions.ADD_STRATEGY,
        strategy: {
            name: name,
            url: url, 
            metadata: metadata
        }
    }
}

// Cambia el tamaño de un objeto o jugador a normal media o pequeño
const changeObjectSize = (objectSize) => {
    return {
        type: actions.CHANGE_OBJECT_SIZE,
        objectSize: objectSize
    }
}

// Cambia el color de una linea
const changeLineColor = (color) => {
    return {
        type: actions.CHANGE_LINE_COLOR,
        color: color
    }
}

// Cambia el tipo de linea
const changeLineType = (lineType) => {
    return {
        type: actions.CHANGE_LINE_TYPE,
        lineType: lineType
    }
}

// Activa o desactiva el dibujado de lineas
const setEnableDraw = (boolean) => {
    return {
        type: actions.SET_ENABLE_DRAW,
        boolean: boolean
    }
}

// Activa o desactiva el dibujado de lineas al interactuar con otros elementos, por ejemplo al arrastrar un jugador
const toggleDrawing = (boolean) => {
    return {
        type: actions.TOGGLE_DRAWING,
        boolean: boolean
    }
}

// Crea una linea con su color y forma
const createLine = (currentLineColor, currentLineType) => {
    return {
        type: actions.CREATE_LINE,
        line: {
            linePoints: [],
            lineColor: currentLineColor,
            lineType: currentLineType
        }
    }
}

// Marca las coordendas de los puntos de una linea siendo creada
const setLine = (x, y) => {
    return {
        type: actions.SET_LINE,
        x: x,
        y: y,
    }
}

// Cambia el history y historyIndex según los parametros dados
const restoreHistory = (history, historyIndex) => {
    return {
        type: actions.RESTORE_HISTORY,
        history: history,
        historyIndex: historyIndex
    }
}

// Retrocede un estado en el history restando 1 al historyIndex
const undo = () => {
    return {
        type: actions.UNDO,
    }
}

// Avanza un estado en el history sumando 1 al historyIndex
const redo = () => {
    return {
        type: actions.REDO
    }
}

// Cambia el historyIndex a 0 volviendo al estado del history en el que no hay ningún elemento en pantalla
const cleanCanvas = () => {
    return {
        type: actions.CLEAN_CANVAS
    }
}

// Activa o descativa la papelera de reciclaje para quitar elementos en cancha
const toggleRecicleByn = (boolean) => {
    return {
        type: actions.TOGGLE_RECICLE_BYN,
        boolean: boolean
    }
}

const setEraser = (boolean) => {
    return {
        type: actions.SET_ERASER,
        boolean: boolean
    }
}

// Cambia la imagen de fondo de la cancha
const changeBackground = (background) => {
    return {
        type: actions.CHANGE_BACKGROUND,
        background: background
    }
}

// Cambia el ángulo de rotación de la imagen de fondo de la cancha dependiendo de si el dispositivo del cual miro la aplicación está en posición horizontal o vertical
const changeBackgroundAngle = (backgroundAngle) => {
    return {
        type: actions.CHANGE_BACKGROUND_ANGLE,
        backgroundAngle: backgroundAngle
    }
}

// Agrega un tipo de jugador azul por defecto a la cancha
const toggleDefaultPlayersBlue = (playersType) => {
    return {
        type: actions.TOGGLE_DEFAULT_PLAYERS_BLUE,
        playersType: playersType
    }
}

// Agrega un tipo de jugador rojo por defecto a la cancha
const toggleDefaultPlayersRed = (playersType) => {
    return {
        type: actions.TOGGLE_DEFAULT_PLAYERS_RED,
        playersType: playersType
    }
}

// Agrega un tipo de objeto a la cancha
const toggleDefaultObjects = (objectType) => {
    return {
        type: actions.TOGGLE_DEFAULT_OBJECTS,
        objectType: objectType
    }
}


const hideTransformBox = () => {
    return {
        type: actions.HIDE_TRANSFORM_BOX,

    }
}

// const addAttackCustomTeam = (customTeam) => {
//     return {
//         type: actions.ADD_ATTACK_CUSTOM_TEAM,
//         customTeam: customTeam
//     }
// }

// const addDefenseCustomTeam = (customTeam) => {
//     return {
//         type: actions.ADD_DEFENSE_CUSTOM_TEAM,
//         customTeam: customTeam
//     }
// }

// Edita una estrategia en el store de redux
const editStrategy = (strategy) => {
    return {
        type: actions.EDIT_STRATEGY,
        strategy: strategy
    }
}

// Guarda un equipo personalizado
const addCustomTeam = (teamSelected, customTeam) => {
    return {
        type: actions.ADD_CUSTOM_TEAM,
        teamSelected: teamSelected,
        customTeam: customTeam
    }
}

// Agrega a la cancha un equipo personalizado creado previamente
const addSelectedCustomTeam = (team) => {
    return {
        type: actions.ADD_SELECTED_CUSTOM_TEAM,
        team: team
    }
}

// Guarda un equipo personalizado como "mi equipo"
const addMyTeam = (team) => {
    return {
        type: actions.ADD_MY_TEAM,
        team: team
    }
}

// Actualiza la posición de un objeto en cancha
const updateObjectPosition = (objectType, typeId, elementId, x, y) => {
    return {
        type: actions.UPDATE_OBJECT_POSITION,
        objectType: objectType,
        typeId: typeId,
        elementId: elementId,
        x: x,
        y: y,
    }
}

// Actualiza la posición de un jugador por defecto en cancha
const updateCustomPlayerPosition = (objectType, elementId, x, y) => {
    return {
        type: actions.UPDATE_CUSTOM_PLAYER_POSITION,
        objectType: objectType,
        elementId: elementId,
        x: x,
        y: y
    }
}

// Actualiza en la cancha un equipo que fue editado
const updateEditedTeam = (team, teamType) => {
    return {
        type: actions.UPDATE_EDITED_TEAM,
        team: team,
        teamType: teamType
    }
}

// Quita uno de los equipos personalizados de la cancha
const deleteCustomTeam = (teamType) => {
    return {
        type: actions.DELETE_CUSTOM_TEAM,
        teamType: teamType
    }
}

// Configura los datos del usuario actual
const setUser = (currentUser) => {
    return {
        type: actions.SET_USER,
        currentUser: currentUser
    }
}

// Configura los datos del panel de usuario y el banner
const setConfigData = (configData) => {
    return {
        type: actions.SET_CONFIG_DATA,
        configData: configData
    }
}

const setUsersData = (usersData) => {
    return {
        type: actions.SET_USERS_DATA,
        usersData: usersData,
    }
}

const addUserDatabase = (userDatabase) => {
    return {
        type: actions.ADD_USER_DATABASE,
        userDatabase: userDatabase
    }
}

const addUserDatabaseId = (userDatabaseId) => {
    return {
        type: actions.ADD_USER_DATABASE_ID,
        userDatabaseId: userDatabaseId
    }
}

export { 
    toggleDefaultPlayersBlue, 
    toggleDefaultPlayersRed, 
    toggleDefaultObjects, 
    hideTransformBox,
    addCustomTeam,
    addSelectedCustomTeam,
    addMyTeam,
    updateObjectPosition,
    updateCustomPlayerPosition,
    updateEditedTeam,
    deleteCustomTeam,
    editStrategy,
    togglePanel, 
    addStrategy, 
    setUser, 
    setConfigData,
    setUsersData,
    addUserDatabase,
    addUserDatabaseId,
    changeObjectSize,
    setEnableDraw,
    toggleDrawing, 
    changeLineColor, 
    changeLineType, 
    createLine, 
    setLine, 
    restoreHistory,
    undo, 
    redo,
    cleanCanvas, 
    setEraser,
    toggleRecicleByn,
    changeBackground, 
    changeBackgroundAngle
}