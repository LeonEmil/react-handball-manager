// Import the functions you need from the SDKs you need
import firebase from 'firebase'
import { getAnalytics } from "firebase/analytics";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyAgerqMcIBestP1ySGV-3AfkiLr2IYlTpY",
  authDomain: "gainberri-493a5.firebaseapp.com",
  projectId: "gainberri-493a5",
  storageBucket: "gainberri-493a5.appspot.com",
  messagingSenderId: "5891547640",
  appId: "1:5891547640:web:e2f3094350bb270996f60d",
  measurementId: "G-7GXP7T2JLP"
};

// Initialize Firebase
firebase.initializeApp(firebaseConfig);

export const db = firebase.firestore()
export const auth = firebase.auth()
export const storage = firebase.storage()

export default firebaseConfig