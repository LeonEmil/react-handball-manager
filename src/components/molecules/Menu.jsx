import React, { useState, useEffect, useRef } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBars, faHome, faDoorOpen, faSave, faFolderOpen, faVideo, faUsers, faEdit, faTrashAlt, faSync, faUndo, faRedo, faStar, faSignOutAlt } from "@fortawesome/free-solid-svg-icons";
import { connect } from 'react-redux'
import { togglePanel, undo, redo } from './../../redux/actionCreators'
import { cleanCanvas, addStrategy, changeLineColor, toggleRecicleByn } from './../../redux/actionCreators'
import { storage } from "./../../firebase/firebase"
import { auth, db } from './../../firebase/firebase'
import 'semantic-ui-css/semantic.min.css'

import MenuPanel from '../Panels/MenuPanel'
import StrategyPanel from '../Panels/StrategyPanel'
import DrawPanel from '../Panels/DrawPanel'
import PlayersPanel from '../Panels/PlayersPanel'
import ObjectsPanel from '../Panels/ObjectsPanel'
import VideoPanel from '../Panels/VideoPanel'
import MailPanel from '../Panels/MailPanel'
import UserPanel from '../Panels/UserPanel'
import ConfigPanel from '../Panels/ConfigPanel'
import { setEraser } from './../../redux/actionCreators'
import alertify from 'alertifyjs'
import { Button, Modal, Form } from 'semantic-ui-react'
import Select from 'react-select'
import Expand from 'react-expand-animated';
import { produce } from 'immer'

const Menu = ({cleanCanvas, undo, redo, togglePanel, currentUser, setEraser, canvasWidth, canvasHeight, recicleByn, toggleRecicleByn, id, history, historyIndex }) => {
    const getFolderNames = () => {
        let folderNames = []
        if(currentUser){
            currentUser.strategies.forEach((folder) => {
                folderNames.push({label: folder.name, value: folder.name})
            })
        }
        return folderNames
    }

    const folderNames = getFolderNames()
    
    const menu = useRef()
    const recicleBynRef = useRef()
    
    const [displayModal, setDisplayModal] = useState("none")
    const [strategyList, setStrategyList] = useState([])
    const [openStrategyForm, setOpenStrategyForm] = useState(false)
    const [expandNewFolderOption, setExpandNewFolderOption] = useState(false)
    const [newFolderNameError, setNewFolderNameError] = useState(false)
    const [repeatedFileNameError, setRepeatedFileNameError] = useState(false)


    let style = 
    canvasHeight < 700 || canvasWidth < 400 ?
        {
            width: "15px",
            height: "15px",
            cursor: "pointer",
            color : "white"
        } 
    :
        {
            width: "30px",
            height: "30px",
            cursor: "pointer",
            color : "white"
        }
    
    const saveImage = () => {
        const canvas = document.querySelectorAll("canvas")[0]
        const canvasLines = document.querySelectorAll("canvas")[1]
        const canvasPlayers = document.querySelectorAll("canvas")[2]
        canvas.getContext("2d").drawImage(canvasLines, 0, 0)
        canvas.getContext("2d").drawImage(canvasPlayers, 0, 0)
        const storageRef = storage.ref()
        const fileName = window.prompt("Nombre de la estrategia")
        let folderName = null
        if (fileName !== null) folderName = window.prompt("Nombre de la carpeta")
        if (fileName !== null && folderName !== null) {
            canvas.toBlob(blob => {
                var image = new Image();
                image.src = blob;
                storageRef.child(`${currentUser.name}/images/${folderName}/${fileName}`).put(blob)
                    .then(async () => {
                        let url = await storageRef.child(`${currentUser.name}/images/${folderName}/${fileName}`).getDownloadURL()
                        let metadata = await storageRef.child(`${currentUser.name}/images/${folderName}/${fileName}`).getMetadata()
                        db.collection('users').where('email', '==', currentUser.email).get().then(response => {
                            let id = response.docs[0].id
                            let doc = response.docs[0].data()
                            let folderIndex = doc.strategies.findIndex(folder => folder.name === folderName)
                            if (folderIndex === -1) {
                                doc.strategies.push({ name: folderName, list: [] })
                                folderIndex = doc.strategies.findIndex(folder => folder.name === folderName)
                            }
                            doc.strategies[folderIndex].list.push({
                                name: fileName,
                                url: url,
                                size: `${Math.floor(metadata.size / 1000)} Kb`,
                                date: metadata.timeCreated.slice(0, 10)
                            })
                            db.collection('users').doc(id).update(doc).then(() => { alert("Estrategia guardada") })
                        })
                    })
                    .catch(error => {
                        alert(error)
                    });
                });
            }
        }
        
        const turnOffRecicleByn = () => {
            toggleRecicleByn(false); 
            recicleBynRef.current.style.backgroundColor = "#085e5f"; 
            document.body.style.cursor = "default"; 
            setEraser(false)
        }
        
        let repeatedFileNameVerification = (array, value) => {
            if(array.includes(value)){
                let index = 1
                console.log(`${value} (${index})`)
                while(array.includes(`${value} (${index})`)){             
                index++
            }    
            array.push(`${value} (${index})`)
            console.log(`Dato guardado como ${value} (${index})`)
        }
        else {
            array.push(value)
            console.log(`Dato guardado como ${value}`)
        }
    }   

    const saveStrategy = (e) => {
        if(newFolderNameError){
            alertify.alert("Por favor escribe otro nombre de carpeta")
        }
        else {
            const user = currentUser
            if(expandNewFolderOption){
                let newStrategies = produce(user.strategies, strategyUpdated => {
                    strategyUpdated.push({
                        name: e.target.newFolder.value,
                        list: []
                    })
                    strategyUpdated[strategyUpdated.length - 1].list.push({
                        name: e.target.name.value,
                        strategy: history[historyIndex]
                    })
                })
                user.strategies = newStrategies
                
                db.collection('users').doc(id).update(user).then(() => {
                    alertify.notify("Estrategia guardada", "success", 5)
                })
                .catch((error) => {
                    alert(error)
                })
            }
            else {
                let folderIndex = folderNames.map(el => el.value).indexOf(e.target.folder.value)
                let newStrategies = produce(user.strategies, strategyUpdated => {
                    strategyUpdated[folderIndex].list.push({
                        name: e.target.name.value,
                        strategy: history[historyIndex]
                    })
                })
                user.strategies = newStrategies
                db.collection('users').doc(id).update(user).then(() => {
                    alertify.notify("Estrategia guardada", 'success', 5)
                })
            }
        }
        setOpenStrategyForm(false)
    }
    
    return (
        <>
        {
            <nav className="menu" ref={menu}>
                <Modal
                    style={{zIndex: 10000}}
                    centered={false}
                    open={openStrategyForm}
                    onClose={() => setOpenStrategyForm(false)}
                    onOpen={() => setOpenStrategyForm(true)}
                >
                    <Modal.Header>Guardar estrategia</Modal.Header>
                    <Modal.Content>
                    <Form onSubmit={saveStrategy}>
                        <Form.Field required>
                            <label htmlFor="name">Nombre de la estrategia</label>
                            <input name="name" required />
                        </Form.Field>
                        {
                            repeatedFileNameError ?
                                <p className="login__alert">Ya existe una estrategia con el mismo nombre en la carpeta seleccionada</p>
                            : null
                        }
                        {
                            !expandNewFolderOption ? 
                                <Form.Field>
                                    <label htmlFor="folder">Nombre de la carpeta</label>
                                    <Select name="folder" defaultValue={folderNames[0]} options={folderNames} required />
                                </Form.Field>
                            :
                                null
                        }
                        <Button role="input" type="button" value="Crear nueva carpeta" className="create-strategy__new-folder-button" onClick={() => setExpandNewFolderOption(expandNewFolderOption ? false : true)}>{expandNewFolderOption ? "Elegir carpeta creada" : "Crear nueva carpeta"}</Button>
                        {
                            expandNewFolderOption ?
                            <Expand open={expandNewFolderOption}>
                                <Form.Field>
                                    <label htmlFor="newFolder">Nombre de la nueva carpeta</label>
                                    <input 
                                        name="newFolder"
                                        className="create-strategy__new-folder-input"
                                        required
                                        onChange={(e) => {
                                            if(folderNames.map(folder => folder.value).includes(e.target.value)){
                                                setNewFolderNameError(true)
                                            }
                                            else {
                                                setNewFolderNameError(false)
                                            }
                                        }}
                                    />
                                    {
                                        newFolderNameError ? 
                                            <span className="login__alert">El nombre de la carpeta ya existe, por favor elija otro nombre</span>
                                        :
                                        null
                                    }
                                </Form.Field>
                            </Expand>
                            : null
                        }
                        <Button type='submit'>Guardar</Button>
                    </Form>
                    </Modal.Content>
                    <Modal.Actions>
                    <Button onClick={() => setOpenStrategyForm(false)}>Cancelar</Button>
                    </Modal.Actions>
                </Modal>
                <ul className="menu__list">
                    <li className="menu__item" data-tooltip-menu="Salir" onClick={() => { auth.signOut(); turnOffRecicleByn()} }>
                        <FontAwesomeIcon icon={faDoorOpen} size="lg" color="white" style={style} />
                    </li>
                    <li className="menu__item" data-tooltip-menu="Guardar" onClick={() => { setOpenStrategyForm(true); turnOffRecicleByn() }}>
                        <div className="menu__icon-save"></div>
                    </li>
                    <li className="menu__item" data-tooltip-menu="Mis estrategias" onClick={() => { togglePanel(0); turnOffRecicleByn() }}>
                        <div className="menu__icon-folder"></div>
                    </li>
                    <li className="menu__item" data-tooltip-menu="Trazar movimientos" onClick={() => { togglePanel(1); turnOffRecicleByn() }}>
                        <div className="menu__icon-edit"></div>
                    </li>
                    <li className="menu__item" data-tooltip-menu="Limpiar cancha" onClick={() => { cleanCanvas(); turnOffRecicleByn() }}>
                        <div className="menu__icon-refresh"></div>
                    </li>
                    <li className="menu__item" data-tooltip-menu="Deshacer" onClick={() => { undo(); turnOffRecicleByn() }}>
                        <div className="menu__icon-undo"></div>
                    </li>
                    <li className="menu__item" data-tooltip-menu="Rehacer" onClick={() => { redo(); turnOffRecicleByn() }}>
                        <div className="menu__icon-redo"></div>
                    </li>
                    <li className="menu__item" data-tooltip-menu="Jugadores" onClick={() => { togglePanel(2); turnOffRecicleByn() }}>
                        <div className="menu__icon-players"></div>
                    </li>
                    <li className="menu__item" data-tooltip-menu="Entrenamientos" onClick={() => { togglePanel(4); turnOffRecicleByn() }}>
                        <div className="menu__icon-star"></div>
                    </li>
                    <li className="menu__item" ref={recicleBynRef} data-tooltip-menu="Borrar elementos" onClick={() => { 
                        
                        if(recicleByn){
                            recicleBynRef.current.style.backgroundColor = "#085e5f"
                            document.body.style.cursor = "default"
                            toggleRecicleByn(false)
                            setEraser(false)
                        }
                        else {
                            recicleBynRef.current.style.backgroundColor = "red"
                            document.body.style.cursor = "move"
                            toggleRecicleByn(true)
                            setEraser(true)
                            window.alert("Arrastre elementos o lineas a la zona de la papelera para borrarlos")
                        }
                    }}>
                        <FontAwesomeIcon icon={faTrashAlt} size="lg" color="white" style={style} />
                    </li>
                </ul>
            </nav>
        }
            {
                currentUser ?
                    <>
                        <StrategyPanel menuRef={menu}/> 
                        <MenuPanel menuRef={menu}/>
                        <DrawPanel menuRef={menu}/> 
                    </>
                :
                null
            }
            <PlayersPanel menuRef={menu}/> 
            <ObjectsPanel menuRef={menu}/> 
        </>
    );
}

const mapStateToProps = (state) => {
    return {
        currentUser: state.currentUser,
        recicleByn: state.recicleByn,
        id: state.currentUser ? state.currentUser.id : undefined,
        history: state.history,
        historyIndex: state.historyIndex
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        undo: () => { dispatch(undo()) },
        redo: () => { dispatch(redo()) },
        cleanCanvas: () => { dispatch(cleanCanvas()) },
        togglePanel: (panel) => { dispatch(togglePanel(panel)) },
        addStrategy: (strategy) => { dispatch(addStrategy(strategy)) },
        changeLineColor: (color) => { dispatch(changeLineColor(color)) },
        setEraser: (boolean) => { dispatch(setEraser(boolean)) },
        toggleRecicleByn: (boolean) => { dispatch(toggleRecicleByn(boolean)) }
    }
}
 
export default connect(mapStateToProps, mapDispatchToProps)(Menu);