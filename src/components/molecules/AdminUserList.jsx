import React, { useState, useEffect, Fragment } from 'react';
import { db, auth } from '../../firebase/firebase'
import { useFormik } from 'formik'
import * as Yup from 'yup'
import { connect } from 'react-redux'
import { setUsersData } from './../../redux/actionCreators'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimes } from '@fortawesome/free-solid-svg-icons'
import { Modal, Form, Button } from 'semantic-ui-react'
import Loader from "react-loader-spinner";
import alertify from 'alertifyjs'

const AdminUserList = ({ currentUser, usersData, setUsersData }) => {
    
    const { handleSubmit, handleChange, handleBlur, values, touched, errors } = useFormik({
        initialValues: {
            name: '',
            email: '',
            role: ''
        },
        validationSchema: Yup.object({
            name: Yup.string().max(10, 'El nombre de usuario debe ser más corto de 10 caracteres').required('Ingrese un nombre de usuario'),
            email: Yup.string().email().required('Por favor ingrese el email de un usuario')
        }),
        onSubmit: ({ name, email, role }) => {
            console.log(name, email, role)
            db.collection('users').where('email', '==', email).get().then(response => {
                console.log(response)
                const id = response.docs[0].id
                db.collection('users').doc(id).update({
                    name: name,
                    role: role
                })
                .then(() => {
                    alert('Usuario actualizado')
                })
                .catch(error => {
                    alert(error)
                })
            })
        }
    })

    const [displayModal, setDisplayModal] = useState(false)

    const [users, setUsers] = useState([])
    const [inputValueRef, setInputValueRef] = useState({value: ""})
    const [saveChangesDisabled, setSaveChangesDisabled] = useState(true)
    const [loading, setLoading] = useState(false)
    
    useEffect(() => {
        db.collection('users').get().then(users => {
            let usersList = []
            let newUsersData = usersData.splice()
            users.docs.forEach(user => {
                usersList.push(user.data())
                newUsersData.push({name: user.data().name, email: user.data().email, role: user.data().role})
            })
            setUsersData(newUsersData)
            setUsers(usersList)
        })
    }, [])

    const changeUsersData = (key, input, data) => {
        let newUsersData = usersData.splice(0)
        console.log(usersData)
        newUsersData[key][input] = data
        setUsersData(newUsersData)
        setSaveChangesDisabled(false)
    }
    
    const saveUsersData = (e) => {
        e.persist()
        e.preventDefault()
        setLoading(true)
        let newUsers = users.splice(0)
        newUsers.map((user, key) => {
            user.name = usersData[key].name
            user.email = usersData[key].email
            user.role = usersData[key].role
        })
        db.collection('users').get().then(response => {
            response.docs.forEach((doc, i) => {
                doc.ref.update(newUsers[i])
            })
            alertify.notify('Usuarios actualizados', 'success', 5)
        })
        setLoading(false)
    }
    
    return (  
        <>
        <Form className="admin__users" onSubmit={saveUsersData}>
            <div className="admin__users-header">
                <p>Nombre</p>
                <p>Email</p>
                <p>Rol</p>
            </div>
            <div>
            {
                usersData.map((user, key) => {
                    return (
                        <div key={key} className="admin__users-content">
                            <Form.Field>
                                <input type="text" value={usersData[key].name} onChange={(e) => { changeUsersData(key, 'name', e.target.value) }} required/> 
                            </Form.Field>
                            <Form.Field>
                                <input type="email" value={usersData[key].email} onChange={(e) => { changeUsersData(key, 'email', e.target.value) }} required/>
                            </Form.Field>
                            <Form.Field>
                                {/* <label htmlFor="" className="form-teams__label">Forma</label> */}
                                <select name="" className="form-teams__select" onChange={(e) => { changeUsersData(key, 'role', e.target.value) }}>
                                    <option value={usersData[key].role}>{usersData[key].role}</option>
                                    <option value="user">user</option>
                                    <option value="admin">admin</option>
                                    <option value="superadmin">superadmin</option>
                                </select>
                            </Form.Field>
                        </div>
                    )
                })
            }
            </div>
            {
                saveChangesDisabled ?
                    <Button onClick={() => { setDisplayModal(true) }} disabled>Guardar cambios</Button>
                : <Button onClick={() => { setDisplayModal(true) }}>Guardar cambios</Button>
            }
        </Form>

        {
            loading ? 
            <Loader
                type="TailSpin"
                color="#00BFFF"
                height={100}
                width={100}
                timeout={900000000} 
                className="config-panel__loader-spinner"
            />
            :
            null
        }
        {/* <div className="admin__modal" style={displayModal ? {transform: 'scaleY(1)'} : {transform: 'scaleY(0)'}}>
            <div className="admin__exit-container" onClick={() => { setDisplayModal(false) }}>
                <FontAwesomeIcon icon={faTimes} color="#fff" size="2x" className="draw-panel__exit-item" />
            </div>
            <form onSubmit={handleSubmit} className="admin__form">
                <label htmlFor="email">Escriba el email de usuario que quiere actualizar</label>
                <input 
                    type="email" 
                    name="email" 
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.email}
                    className="admin__form-input"
                />
                {
                    errors.email && touched.email ?
                        <span className="login__alert">{errors.email}</span>
                        : null
                }
                <label htmlFor="name">Nuevo nombre de usuario</label>
                <input 
                    type="text" 
                    name="name" 
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.name}
                    className="admin__form-input"
                />
                {
                    currentUser.role === "superadmin" ?
                    <>
                    <label htmlFor="role">Nuevo rol de usuario (user, admin o superadmin)</label>
                    <input 
                        type="text"
                        name="role"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.role}
                        className="admin__form-input"
                    />
                    </>
                    :
                    null
                }
                <input type="submit" value="Actualizar"/>
            </form>
        </div> */}

        {/* <Modal
            onClose={() => setDisplayModal(false)}
            onOpen={() => setDisplayModal(true)}
            open={displayModal}
        >
        <Modal.Header>
            <h4>Crear equipo</h4>
        </Modal.Header>
        <Modal.Content>
            <Form onSubmit={handleSubmit} className="admin__form">
                <label htmlFor="email">Escriba el email de usuario que quiere actualizar</label>
                <input 
                    type="email" 
                    name="email" 
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.email}
                    className="admin__form-input"
                />
                {
                    errors.email && touched.email ?
                        <span className="login__alert">{errors.email}</span>
                        : null
                }
                <label htmlFor="name">Nuevo nombre de usuario</label>
                <input 
                    type="text" 
                    name="name" 
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.name}
                    className="admin__form-input"
                />
                {
                    currentUser.role === "superadmin" ?
                    <>
                    <label htmlFor="role">Nuevo rol de usuario (user, admin o superadmin)</label>
                    <input 
                        type="text"
                        name="role"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.role}
                        className="admin__form-input"
                    />
                    </>
                    :
                    null
                }
                <input type="submit" value="Actualizar"/>
            </Form>
        </Modal.Content>
        <Modal.Actions>
            <Button onClick={() => setDisplayModal(false)}>Cancelar</Button>
        </Modal.Actions>
        </Modal> */}
        </>
    );
}

const mapStateToProps = (state) => {
    return {
        currentUser: state.currentUser,
        usersData: state.usersData
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        setUsersData: (usersData) => { dispatch(setUsersData(usersData)) }
    }
}
 
export default connect(mapStateToProps, mapDispatchToProps)(AdminUserList);