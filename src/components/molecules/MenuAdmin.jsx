import React, { useState, useEffect, useRef } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBars, faVolleyballBall, faDoorOpen, faSave, faFolderOpen, faVideo, faUsers, faUserPlus, faImages, faSync, faUndo, faRedo, faStar, faSignOutAlt } from "@fortawesome/free-solid-svg-icons";
import { connect } from 'react-redux'
import { togglePanel, undo, redo } from './../../redux/actionCreators'
import { cleanCanvas, addStrategy, changeLineColor } from './../../redux/actionCreators'
import { auth, db } from './../../firebase/firebase'

import { setEraser } from './../../redux/actionCreators'

const MenuAdmin = ({ canvasWidth, canvasHeight }) => {

    let style =
        canvasHeight < 700 || canvasWidth < 400 ?
            {
                width: "15px",
                height: "15px",
                cursor: "pointer",
                color: "white"
            }
            :
            {
                width: "30px",
                height: "30px",
                cursor: "pointer",
                color: "white"
            }


    return (
        <>
            <nav className="menu">
                <ul className="menu-admin__list">
                    <li className="menu-admin__item" data-tooltip-menu="Mis estrategias">
                        <FontAwesomeIcon icon={faUserPlus} size="lg" color="white" style={style} />
                        <span className="menu__span">Crear usuario</span>   
                    </li>
                    <li className="menu-admin__item" data-tooltip-menu="Trazar movimientos" onClick={() => { togglePanel(1) }}>
                        <FontAwesomeIcon icon={faUsers} size="lg" color="white" style={style} />
                        <span className="menu__span">Lista de usuarios creados</span>                        
                    </li>
                    <li className="menu-admin__item" data-tooltip-menu="Limpiar cancha" onClick={() => { cleanCanvas() }}>
                        <FontAwesomeIcon icon={faImages} size="lg" color="white" style={style} />
                        <span className="menu__span">Configuración de imágenes</span>
                    </li>
                    <li className="menu-admin__item" data-tooltip-menu="Guardar" onClick={() => { }}>
                        <FontAwesomeIcon icon={faVolleyballBall} size="lg" color="white" style={style} />
                        <span className="menu__span">Ir a la aplicación</span>                        
                    </li>
                    <li className="menu-admin__item" data-tooltip-menu="Salir" onClick={() => { auth.signOut() }}>
                        <FontAwesomeIcon icon={faDoorOpen} size="lg" color="white" style={style} />
                        <span className="menu__span">Cerrar sesión</span>
                    </li>
                </ul>
            </nav>

            {

            }
        </>
    );
}

const mapStateToProps = (state) => {
    return {
        currentUser: state.currentUser
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        undo: () => { dispatch(undo()) },
        redo: () => { dispatch(redo()) },
        cleanCanvas: () => { dispatch(cleanCanvas()) },
        togglePanel: (panel) => { dispatch(togglePanel(panel)) },
        addStrategy: (strategy) => { dispatch(addStrategy(strategy)) },
        changeLineColor: (color) => { dispatch(changeLineColor(color)) },
        setEraser: () => { dispatch(setEraser()) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(MenuAdmin);