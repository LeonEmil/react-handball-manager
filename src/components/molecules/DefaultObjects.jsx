import React, { useState, useEffect, useRef } from 'react';
import { Group, Image, Transformer, Rect, Circle, Text } from 'react-konva'
import useImage from 'use-image'

const DefaultObjects = ({ history, historyIndex, currentObjectSize, width, height, updateObjectPosition, eraser }) => {

    const [object1] = useImage('https://res.cloudinary.com/leonemil/image/upload/v1602549258/Handball%20manager/object-1_zdpvbe.png', 'Anonymous')
    const [object2] = useImage('https://res.cloudinary.com/leonemil/image/upload/v1602549253/Handball%20manager/object-2_fgf1ad.png', 'Anonymous')
    const [object3] = useImage('https://res.cloudinary.com/leonemil/image/upload/v1602549253/Handball%20manager/object-3_cylirh.png', 'Anonymous')
    const [object4] = useImage('https://res.cloudinary.com/leonemil/image/upload/v1602549253/Handball%20manager/object-4_djapcd.png', 'Anonymous')
    const [object5] = useImage('https://res.cloudinary.com/leonemil/image/upload/v1602549253/Handball%20manager/object-5_opvwj3.png', 'Anonymous')
    const [object6] = useImage('https://res.cloudinary.com/leonemil/image/upload/v1602549253/Handball%20manager/object-6_zzgsgp.png', 'Anonymous')
    const [object7] = useImage('https://res.cloudinary.com/leonemil/image/upload/v1602549257/Handball%20manager/object-7_sabsou.png', 'Anonymous')
    const [object8] = useImage('https://res.cloudinary.com/leonemil/image/upload/v1602549260/Handball%20manager/object-8_hlipsu.png', 'Anonymous')
    const [object9] = useImage('https://res.cloudinary.com/leonemil/image/upload/v1602549256/Handball%20manager/object-9_sw6ykk.png', 'Anonymous')
    const [object10] = useImage('https://res.cloudinary.com/leonemil/image/upload/v1602549257/Handball%20manager/object-10_tgblke.png', 'Anonymous')
    const [object11] = useImage('https://res.cloudinary.com/leonemil/image/upload/v1602549264/Handball%20manager/object-11_ykdcfy.png', 'Anonymous')
    const [object12] = useImage('https://res.cloudinary.com/leonemil/image/upload/v1602549267/Handball%20manager/object-12_l554hs.png', 'Anonymous')
    const [object13] = useImage('https://res.cloudinary.com/leonemil/image/upload/v1602549264/Handball%20manager/object-13_dbppsz.png', 'Anonymous')
    const [object14] = useImage('https://res.cloudinary.com/leonemil/image/upload/v1602549268/Handball%20manager/object-14_qyygcc.png', 'Anonymous')
    const [object15] = useImage('https://res.cloudinary.com/leonemil/image/upload/v1602549266/Handball%20manager/object-15_ninceu.png', 'Anonymous')
    const [object16] = useImage('https://res.cloudinary.com/leonemil/image/upload/v1602549266/Handball%20manager/object-16_x7mti0.png', 'Anonymous')
    const [object17] = useImage('https://res.cloudinary.com/leonemil/image/upload/v1602549510/Handball%20manager/Player-red-1_bmexdy.png', 'Anonymous')
    const [object18] = useImage('https://res.cloudinary.com/leonemil/image/upload/v1602549512/Handball%20manager/player-red-2_lpiwfe.png', 'Anonymous')
    const [object19] = useImage('https://res.cloudinary.com/leonemil/image/upload/v1602549512/Handball%20manager/player-red-3_suquyh.png', 'Anonymous')
    const [object20] = useImage('https://res.cloudinary.com/leonemil/image/upload/v1602549514/Handball%20manager/player-red-4_gqchxz.png', 'Anonymous')
    const [object21] = useImage('https://res.cloudinary.com/leonemil/image/upload/v1602549514/Handball%20manager/player-red-5_tbmqej.png', 'Anonymous')
    const [object22] = useImage('https://res.cloudinary.com/leonemil/image/upload/v1602549515/Handball%20manager/player-red-6_jurf2s.png', 'Anonymous')
    const [object23] = useImage('https://res.cloudinary.com/leonemil/image/upload/v1602549513/Handball%20manager/player-red-7_cz1i3f.png', 'Anonymous')
    const [object24] = useImage('https://res.cloudinary.com/leonemil/image/upload/v1602549508/Handball%20manager/player-red-8_tpk9am.png', 'Anonymous')
    const [object25] = useImage('https://res.cloudinary.com/leonemil/image/upload/v1602549252/Handball%20manager/object-17_g0zhrn.png', 'Anonymous')

    const size = currentObjectSize === "normal" ? 0.35 :
                 currentObjectSize === "medium" ? 0.25 :
                 currentObjectSize === "small" ? 0.2 :
                 width < 900 || height < 600 ? 0.1 : 0.3

    const fontSize = size === 60 ? 30 :
        size === 45 ? 24 :
            size === 30 ? 17 :
                null

    const textPadding = currentObjectSize === "big" ? 21 :
        currentObjectSize === "medium" ? 17 :
            currentObjectSize === "small" ? 10 :
                width < 900 || height < 600 ? 10 : 21

    const playerRef = useRef()
    const transformRef = useRef()

    const toggleTransformBox = () => {
        if (transformRef.current.getNode()) {
            transformRef.current.nodes([]);
            transformRef.current.getLayer().batchDraw();
        }
        else {
            transformRef.current.nodes([playerRef.current]);
            transformRef.current.getLayer().batchDraw();
        }
    }

    useEffect(() => {
        if(transformRef.current && historyIndex === 0){
            transformRef.current.nodes([]);
            transformRef.current.getLayer().batchDraw();
        }
    })

    const [recicleByn, setRecicleByn] = useState(false)

    return (
        <>
            {
                Object.values(Object.values(history[historyIndex].defaultObjects)[0]).map((object, key) => (
                    <Image
                        key={key}
                        image={object1}
                        x={object.position.x}
                        y={object.position.y}
                        scaleX={size + 0.10}
                        scaleY={size + 0.10}
                        draggable 
                        onDragStart={() => {
                            if(eraser){
                                setRecicleByn(true)
                            }
                        }}
                        onDragEnd={(e) => {
                            let recicleBynPosition = e.target.x() < 200 && e.target.x() > -200 &&
                                                 e.target.y() < (window.innerHeight / 2) + 200 && e.target.y() > (window.innerHeight / 2) - 200 ? true : false
                            if (eraser && recicleBynPosition) {
                                updateObjectPosition('defaultObjects', '0', `${key}`, 9999, 9999)
                            } else {
                                updateObjectPosition('defaultObjects', '0', `${key}`, e.target.x(), e.target.y())
                            }
                            setRecicleByn(false)
                        }}
                        onClick={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                        onTap={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                    />
                ))
            }
            {
                Object.values(Object.values(history[historyIndex].defaultObjects)[1]).map((object, key) => (
                    <Image
                        key={key}
                        image={object2}
                        x={object.position.x}
                        y={object.position.y}
                        scaleX={size + 0.10}
                        scaleY={size + 0.10}
                        draggable 
                        onDragStart={() => {
                            if(eraser){
                                setRecicleByn(true)
                            }
                        }}
                        onDragEnd={(e) => {
                            let recicleBynPosition = e.target.x() < 200 && e.target.x() > -200 &&
                                                 e.target.y() < (window.innerHeight / 2) + 200 && e.target.y() > (window.innerHeight / 2) - 200 ? true : false
                            if (eraser && recicleBynPosition) {
                                updateObjectPosition('defaultObjects', '1', `${key}`, 9999, 9999)
                            } else {
                                updateObjectPosition('defaultObjects', '1', `${key}`, e.target.x(), e.target.y())
                            }
                            setRecicleByn(false)
                        }}
                        onClick={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                        onTap={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                    />
                ))
            }
            {
                Object.values(Object.values(history[historyIndex].defaultObjects)[2]).map((object, key) => (
                    <Image
                        key={key}
                        image={object3}
                        x={object.position.x}
                        y={object.position.y}
                        scaleX={size + 0.10}
                        scaleY={size + 0.10}
                        draggable 
                        onDragStart={() => {
                            if(eraser){
                                setRecicleByn(true)
                            }
                        }}
                        onDragEnd={(e) => {
                            let recicleBynPosition = e.target.x() < 200 && e.target.x() > -200 &&
                                                 e.target.y() < (window.innerHeight / 2) + 200 && e.target.y() > (window.innerHeight / 2) - 200 ? true : false
                            if (eraser && recicleBynPosition) {
                                updateObjectPosition('defaultObjects', '2', `${key}`, 9999, 9999)
                            } else {
                                updateObjectPosition('defaultObjects', '2', `${key}`, e.target.x(), e.target.y())
                            }
                            setRecicleByn(false)
                        }}
                        onClick={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                        onTap={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                    />
                ))
            }
            {
                Object.values(Object.values(history[historyIndex].defaultObjects)[3]).map((object, key) => (
                    <Image
                        key={key}
                        image={object4}
                        x={object.position.x}
                        y={object.position.y}
                        scaleX={size + 0.10}
                        scaleY={size + 0.10}
                        draggable 
                        onDragStart={() => {
                            if(eraser){
                                setRecicleByn(true)
                            }
                        }}
                        onDragEnd={(e) => {
                            let recicleBynPosition = e.target.x() < 200 && e.target.x() > -200 &&
                                                 e.target.y() < (window.innerHeight / 2) + 200 && e.target.y() > (window.innerHeight / 2) - 200 ? true : false
                            if (eraser && recicleBynPosition) {
                                updateObjectPosition('defaultObjects', '3', `${key}`, 9999, 9999)
                            } else {
                                updateObjectPosition('defaultObjects', '3', `${key}`, e.target.x(), e.target.y())
                            }
                            setRecicleByn(false)
                        }}
                        onClick={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                        onTap={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                    />
                ))
            }
            {
                Object.values(Object.values(history[historyIndex].defaultObjects)[4]).map((object, key) => (
                    <Image
                        key={key}
                        image={object5}
                        x={object.position.x}
                        y={object.position.y}
                        scaleX={size + 0.10}
                        scaleY={size + 0.10}
                        draggable 
                        onDragStart={() => {
                            if(eraser){
                                setRecicleByn(true)
                            }
                        }}
                        onDragEnd={(e) => {
                            let recicleBynPosition = e.target.x() < 200 && e.target.x() > -200 &&
                                                 e.target.y() < (window.innerHeight / 2) + 200 && e.target.y() > (window.innerHeight / 2) - 200 ? true : false
                            if (eraser && recicleBynPosition) {
                                updateObjectPosition('defaultObjects', '4', `${key}`, 9999, 9999)
                            } else {
                                updateObjectPosition('defaultObjects', '4', `${key}`, e.target.x(), e.target.y())
                            }
                            setRecicleByn(false)
                        }}
                        onClick={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                        onTap={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                    />
                ))
            }
            {
                Object.values(Object.values(history[historyIndex].defaultObjects)[5]).map((object, key) => (
                    <Image
                        key={key}
                        image={object6}
                        x={object.position.x}
                        y={object.position.y}
                        scaleX={size + 0.10}
                        scaleY={size + 0.10}
                        draggable 
                        onDragStart={() => {
                            if(eraser){
                                setRecicleByn(true)
                            }
                        }}
                        onDragEnd={(e) => {
                            let recicleBynPosition = e.target.x() < 200 && e.target.x() > -200 &&
                                                 e.target.y() < (window.innerHeight / 2) + 200 && e.target.y() > (window.innerHeight / 2) - 200 ? true : false
                            if (eraser && recicleBynPosition) {
                                updateObjectPosition('defaultObjects', '5', `${key}`, 9999, 9999)
                            } else {
                                updateObjectPosition('defaultObjects', '5', `${key}`, e.target.x(), e.target.y())
                            }
                            setRecicleByn(false)
                        }}
                        onClick={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                        onTap={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                    />
                ))
            }
            {
                Object.values(Object.values(history[historyIndex].defaultObjects)[6]).map((object, key) => (
                    <Image
                        key={key}
                        image={object7}
                        x={object.position.x}
                        y={object.position.y}
                        scaleX={size + 0.10}
                        scaleY={size + 0.10}
                        draggable 
                        onDragStart={() => {
                            if(eraser){
                                setRecicleByn(true)
                            }
                        }}
                        onDragEnd={(e) => {
                            let recicleBynPosition = e.target.x() < 200 && e.target.x() > -200 &&
                                                 e.target.y() < (window.innerHeight / 2) + 200 && e.target.y() > (window.innerHeight / 2) - 200 ? true : false
                            if (eraser && recicleBynPosition) {
                                updateObjectPosition('defaultObjects', '6', `${key}`, 9999, 9999)
                            } else {
                                updateObjectPosition('defaultObjects', '6', `${key}`, e.target.x(), e.target.y())
                            }
                            setRecicleByn(false)
                        }}
                        onClick={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                        onTap={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                    />
                ))
            }
            {
                Object.values(Object.values(history[historyIndex].defaultObjects)[7]).map((object, key) => (
                    <Image
                        key={key}
                        image={object8}
                        x={object.position.x}
                        y={object.position.y}
                        scaleX={size + 0.10}
                        scaleY={size + 0.10}
                        draggable 
                        onDragStart={() => {
                            if(eraser){
                                setRecicleByn(true)
                            }
                        }}
                        onDragEnd={(e) => {
                            let recicleBynPosition = e.target.x() < 200 && e.target.x() > -200 &&
                                                 e.target.y() < (window.innerHeight / 2) + 200 && e.target.y() > (window.innerHeight / 2) - 200 ? true : false
                            if (eraser && recicleBynPosition) {
                                updateObjectPosition('defaultObjects', '7', `${key}`, 9999, 9999)
                            } else {
                                updateObjectPosition('defaultObjects', '7', `${key}`, e.target.x(), e.target.y())
                            }
                            setRecicleByn(false)
                        }}
                        onClick={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                        onTap={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                    />
                ))
            }
            {
                Object.values(Object.values(history[historyIndex].defaultObjects)[8]).map((object, key) => (
                    <Image
                        key={key}
                        image={object9}
                        x={object.position.x}
                        y={object.position.y}
                        scaleX={size + 0.10}
                        scaleY={size + 0.10}
                        draggable 
                        onDragStart={() => {
                            if(eraser){
                                setRecicleByn(true)
                            }
                        }}
                        onDragEnd={(e) => {
                            let recicleBynPosition = e.target.x() < 200 && e.target.x() > -200 &&
                                                 e.target.y() < (window.innerHeight / 2) + 200 && e.target.y() > (window.innerHeight / 2) - 200 ? true : false
                            if (eraser && recicleBynPosition) {
                                updateObjectPosition('defaultObjects', '8', `${key}`, 9999, 9999)
                            } else {
                                updateObjectPosition('defaultObjects', '8', `${key}`, e.target.x(), e.target.y())
                            }
                            setRecicleByn(false)
                        }}
                        onClick={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                        onTap={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                    />
                ))
            }
            {
                Object.values(Object.values(history[historyIndex].defaultObjects)[9]).map((object, key) => (
                    <Image
                        key={key}
                        image={object10}
                        x={object.position.x}
                        y={object.position.y}
                        scaleX={size}
                        scaleY={size}
                        draggable 
                        onDragStart={() => {
                            if(eraser){
                                setRecicleByn(true)
                            }
                        }}
                        onDragEnd={(e) => {
                            let recicleBynPosition = e.target.x() < 200 && e.target.x() > -200 &&
                                                 e.target.y() < (window.innerHeight / 2) + 200 && e.target.y() > (window.innerHeight / 2) - 200 ? true : false
                            if (eraser && recicleBynPosition) {
                                updateObjectPosition('defaultObjects', '9', `${key}`, 9999, 9999)
                            } else {
                                updateObjectPosition('defaultObjects', '9', `${key}`, e.target.x(), e.target.y())
                            }
                            setRecicleByn(false)
                        }}
                        onClick={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                        onTap={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                    />
                ))
            }
            {
                Object.values(Object.values(history[historyIndex].defaultObjects)[10]).map((object, key) => (
                    <Image
                        key={key}
                        image={object11}
                        x={object.position.x}
                        y={object.position.y}
                        scaleX={size}
                        scaleY={size}
                        draggable 
                        onDragStart={() => {
                            if(eraser){
                                setRecicleByn(true)
                            }
                        }}
                        onDragEnd={(e) => {
                            let recicleBynPosition = e.target.x() < 200 && e.target.x() > -200 &&
                                                 e.target.y() < (window.innerHeight / 2) + 200 && e.target.y() > (window.innerHeight / 2) - 200 ? true : false
                            if (eraser && recicleBynPosition) {
                                updateObjectPosition('defaultObjects', '10', `${key}`, 9999, 9999)
                            } else {
                                updateObjectPosition('defaultObjects', '10', `${key}`, e.target.x(), e.target.y())
                            }
                            setRecicleByn(false)
                        }}
                        onClick={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                        onTap={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                    />
                ))
            }
            {
                Object.values(Object.values(history[historyIndex].defaultObjects)[11]).map((object, key) => (
                    <Image
                        key={key}
                        image={object12}
                        x={object.position.x}
                        y={object.position.y}
                        scaleX={size}
                        scaleY={size}
                        draggable 
                        onDragStart={() => {
                            if(eraser){
                                setRecicleByn(true)
                            }
                        }}
                        onDragEnd={(e) => {
                            let recicleBynPosition = e.target.x() < 200 && e.target.x() > -200 &&
                                                 e.target.y() < (window.innerHeight / 2) + 200 && e.target.y() > (window.innerHeight / 2) - 200 ? true : false
                            if (eraser && recicleBynPosition) {
                                updateObjectPosition('defaultObjects', '11', `${key}`, 9999, 9999)
                            } else {
                                updateObjectPosition('defaultObjects', '11', `${key}`, e.target.x(), e.target.y())
                            }
                            setRecicleByn(false)
                        }}
                        onClick={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                        onTap={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                    />
                ))
            }
            {
                Object.values(Object.values(history[historyIndex].defaultObjects)[12]).map((object, key) => (
                    <Image
                        key={key}
                        image={object13}
                        x={object.position.x}
                        y={object.position.y}
                        scaleX={size}
                        scaleY={size}
                        draggable 
                        onDragStart={() => {
                            if(eraser){
                                setRecicleByn(true)
                            }
                        }}
                        onDragEnd={(e) => {
                            let recicleBynPosition = e.target.x() < 200 && e.target.x() > -200 &&
                                                 e.target.y() < (window.innerHeight / 2) + 200 && e.target.y() > (window.innerHeight / 2) - 200 ? true : false
                            if (eraser && recicleBynPosition) {
                                updateObjectPosition('defaultObjects', '12', `${key}`, 9999, 9999)
                            } else {
                                updateObjectPosition('defaultObjects', '12', `${key}`, e.target.x(), e.target.y())
                            }
                            setRecicleByn(false)
                        }}
                        onClick={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                        onTap={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                    />
                ))
            }
            {
                Object.values(Object.values(history[historyIndex].defaultObjects)[13]).map((object, key) => (
                    <Image
                        key={key}
                        image={object14}
                        x={object.position.x}
                        y={object.position.y}
                        scaleX={size}
                        scaleY={size}
                        draggable 
                        onDragStart={() => {
                            if(eraser){
                                setRecicleByn(true)
                            }
                        }}
                        onDragEnd={(e) => {
                            let recicleBynPosition = e.target.x() < 200 && e.target.x() > -200 &&
                                                 e.target.y() < (window.innerHeight / 2) + 200 && e.target.y() > (window.innerHeight / 2) - 200 ? true : false
                            if (eraser && recicleBynPosition) {
                                updateObjectPosition('defaultObjects', '13', `${key}`, 9999, 9999)
                            } else {
                                updateObjectPosition('defaultObjects', '13', `${key}`, e.target.x(), e.target.y())
                            }
                            setRecicleByn(false)
                        }}
                        onClick={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                        onTap={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                    />
                ))
            }
            {
                Object.values(Object.values(history[historyIndex].defaultObjects)[14]).map((object, key) => (
                    <Image
                        key={key}
                        image={object15}
                        x={object.position.x}
                        y={object.position.y}
                        scaleX={size / 1.5}
                        scaleY={size / 1.5}
                        draggable 
                        onDragStart={() => {
                            if(eraser){
                                setRecicleByn(true)
                            }
                        }}
                        onDragEnd={(e) => {
                            let recicleBynPosition = e.target.x() < 200 && e.target.x() > -200 &&
                                                 e.target.y() < (window.innerHeight / 2) + 200 && e.target.y() > (window.innerHeight / 2) - 200 ? true : false
                            if (eraser && recicleBynPosition) {
                                updateObjectPosition('defaultObjects', '14', `${key}`, 9999, 9999)
                            } else {
                                updateObjectPosition('defaultObjects', '14', `${key}`, e.target.x(), e.target.y())
                            }
                            setRecicleByn(false)
                        }}
                        onClick={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                        onTap={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                    />
                ))
            }
            {
                Object.values(Object.values(history[historyIndex].defaultObjects)[15]).map((object, key) => (
                    <Image
                        key={key}
                        image={object16}
                        x={object.position.x}
                        y={object.position.y}
                        scaleX={size / 1.5}
                        scaleY={size / 1.5}
                        draggable 
                        onDragStart={() => {
                            if(eraser){
                                setRecicleByn(true)
                            }
                        }}
                        onDragEnd={(e) => {
                            let recicleBynPosition = e.target.x() < 200 && e.target.x() > -200 &&
                                                 e.target.y() < (window.innerHeight / 2) + 200 && e.target.y() > (window.innerHeight / 2) - 200 ? true : false
                            if (eraser && recicleBynPosition) {
                                updateObjectPosition('defaultObjects', '15', `${key}`, 9999, 9999)
                            } else {
                                updateObjectPosition('defaultObjects', '15', `${key}`, e.target.x(), e.target.y())
                            }
                            setRecicleByn(false)
                        }}
                        onClick={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                        onTap={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                    />
                ))
            }
            {
                Object.values(Object.values(history[historyIndex].defaultObjects)[16]).map((object, key) => (
                    <Group
                        key={key}
                        draggable
                        x={object.position.x}
                        y={object.position.y}
                        onDragStart={() => {
                            if (eraser) {
                                setRecicleByn(true)
                            }
                        }}
                        onDragEnd={(e) => {
                            let recicleBynPosition = e.target.x() < 200 && e.target.x() > -200 &&
                                                 e.target.y() < (window.innerHeight / 2) + 200 && e.target.y() > (window.innerHeight / 2) - 200 ? true : false
                            if (eraser && recicleBynPosition) {
                                updateObjectPosition('defaultObjects', '16', `${key}`, 9999, 9999)
                            } else {
                                updateObjectPosition('defaultObjects', '16', `${key}`, e.target.x(), e.target.y())
                            }
                            setRecicleByn(false)
                        }}
                        onClick={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                        onTap={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                    >
                        <Image
                            image={object17}
                            scaleX={size + 0.10}
                            scaleY={size + 0.10}
                        />
                        <Text
                            text={key + 1}
                            align="center"
                            verticalAlign="center"
                            x={textPadding}
                            y={textPadding}
                            fontSize={fontSize}
                            fill="white"
                        />
                    </Group>
                ))
            }
            {
                Object.values(Object.values(history[historyIndex].defaultObjects)[17]).map((object, key) => (
                    <Group
                        key={key}
                        draggable
                        x={object.position.x}
                        y={object.position.y}
                        onDragStart={() => {
                            if (eraser) {
                                setRecicleByn(true)
                            }
                        }}
                        onDragEnd={(e) => {
                            let recicleBynPosition = e.target.x() < 200 && e.target.x() > -200 &&
                                                 e.target.y() < (window.innerHeight / 2) + 200 && e.target.y() > (window.innerHeight / 2) - 200 ? true : false
                            if (eraser && recicleBynPosition) {
                                updateObjectPosition('defaultObjects', '17', `${key}`, 9999, 9999)
                            } else {
                                updateObjectPosition('defaultObjects', '17', `${key}`, e.target.x(), e.target.y())
                            }
                            setRecicleByn(false)
                        }}
                        onClick={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                        onTap={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                    >
                        <Image
                            image={object18}
                            scaleX={size + 0.10}
                            scaleY={size + 0.10}
                        />
                        <Text
                            text={key + 1}
                            align="center"
                            verticalAlign="center"
                            x={textPadding}
                            y={textPadding}
                            fontSize={fontSize}
                            fill="white"
                        />
                    </Group>
                ))
            }
            {
                Object.values(Object.values(history[historyIndex].defaultObjects)[18]).map((object, key) => (
                    <Group
                        key={key}
                        draggable
                        x={object.position.x}
                        y={object.position.y}
                        onDragStart={() => {
                            if (eraser) {
                                setRecicleByn(true)
                            }
                        }}
                        onDragEnd={(e) => {
                            let recicleBynPosition = e.target.x() < 200 && e.target.x() > -200 &&
                                                 e.target.y() < (window.innerHeight / 2) + 200 && e.target.y() > (window.innerHeight / 2) - 200 ? true : false
                            if (eraser && recicleBynPosition) {
                                updateObjectPosition('defaultObjects', '18', `${key}`, 9999, 9999)
                            } else {
                                updateObjectPosition('defaultObjects', '18', `${key}`, e.target.x(), e.target.y())
                            }
                            setRecicleByn(false)
                        }}
                        onClick={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                        onTap={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                    >
                        <Image
                            image={object19}
                            scaleX={size + 0.10}
                            scaleY={size + 0.10}
                        />
                        <Text
                            text={key + 1}
                            align="center"
                            verticalAlign="center"
                            x={textPadding}
                            y={textPadding}
                            fontSize={fontSize}
                            fill="white"
                        />
                    </Group>
                ))
            }
            {
                Object.values(Object.values(history[historyIndex].defaultObjects)[19]).map((object, key) => (
                    <Image
                        key={key}
                        image={object20}
                        x={object.position.x}
                        y={object.position.y}
                        scaleX={size * 1.5}
                        scaleY={size * 1.5}
                        draggable 
                        onDragStart={() => {
                            if(eraser){
                                setRecicleByn(true)
                            }
                        }}
                        onDragEnd={(e) => {
                            let recicleBynPosition = e.target.x() < 200 && e.target.x() > -200 &&
                                                 e.target.y() < (window.innerHeight / 2) + 200 && e.target.y() > (window.innerHeight / 2) - 200 ? true : false
                            if (eraser && recicleBynPosition) {
                                updateObjectPosition('defaultObjects', '19', `${key}`, 9999, 9999)
                            } else {
                                updateObjectPosition('defaultObjects', '19', `${key}`, e.target.x(), e.target.y())
                            }
                            setRecicleByn(false)
                        }}
                        onClick={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                        onTap={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                    />
                ))
            }
            {
                Object.values(Object.values(history[historyIndex].defaultObjects)[20]).map((object, key) => (
                    <Image
                        key={key}
                        image={object21}
                        x={object.position.x}
                        y={object.position.y}
                        scaleX={size * 1.5}
                        scaleY={size * 1.5}
                        draggable 
                        onDragStart={() => {
                            if(eraser){
                                setRecicleByn(true)
                            }
                        }}
                        onDragEnd={(e) => {
                            let recicleBynPosition = e.target.x() < 200 && e.target.x() > -200 &&
                                                 e.target.y() < (window.innerHeight / 2) + 200 && e.target.y() > (window.innerHeight / 2) - 200 ? true : false
                            if (eraser && recicleBynPosition) {
                                updateObjectPosition('defaultObjects', '20', `${key}`, 9999, 9999)
                            } else {
                                updateObjectPosition('defaultObjects', '20', `${key}`, e.target.x(), e.target.y())
                            }
                            setRecicleByn(false)
                        }}
                        onClick={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                        onTap={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                    />
                ))
            }
            {
                Object.values(Object.values(history[historyIndex].defaultObjects)[21]).map((object, key) => (
                    <Image
                        key={key}
                        image={object22}
                        x={object.position.x}
                        y={object.position.y}
                        scaleX={size * 1.5}
                        scaleY={size * 1.5}
                        draggable 
                        onDragStart={() => {
                            if(eraser){
                                setRecicleByn(true)
                            }
                        }}
                        onDragEnd={(e) => {
                            let recicleBynPosition = e.target.x() < 200 && e.target.x() > -200 &&
                                                 e.target.y() < (window.innerHeight / 2) + 200 && e.target.y() > (window.innerHeight / 2) - 200 ? true : false
                            if (eraser && recicleBynPosition) {
                                updateObjectPosition('defaultObjects', '21', `${key}`, 9999, 9999)
                            } else {
                                updateObjectPosition('defaultObjects', '21', `${key}`, e.target.x(), e.target.y())
                            }
                            setRecicleByn(false)
                        }}
                        onClick={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                        onTap={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                    />
                ))
            }
            {
                Object.values(Object.values(history[historyIndex].defaultObjects)[22]).map((object, key) => (
                    <Image
                        key={key}
                        image={object23}
                        x={object.position.x}
                        y={object.position.y}
                        scaleX={size * 1.5}
                        scaleY={size * 1.5}
                        draggable 
                        onDragStart={() => {
                            if(eraser){
                                setRecicleByn(true)
                            }
                        }}
                        onDragEnd={(e) => {
                            let recicleBynPosition = e.target.x() < 200 && e.target.x() > -200 &&
                                                 e.target.y() < (window.innerHeight / 2) + 200 && e.target.y() > (window.innerHeight / 2) - 200 ? true : false
                            if (eraser && recicleBynPosition) {
                                updateObjectPosition('defaultObjects', '22', `${key}`, 9999, 9999)
                            } else {
                                updateObjectPosition('defaultObjects', '22', `${key}`, e.target.x(), e.target.y())
                            }
                            setRecicleByn(false)
                        }}
                        onClick={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                        onTap={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                    />
                ))
            }
            {
                Object.values(Object.values(history[historyIndex].defaultObjects)[23]).map((object, key) => (
                    <Image
                        key={key}
                        image={object24}
                        x={object.position.x}
                        y={object.position.y}
                        scaleX={size * 1.5}
                        scaleY={size * 1.5}
                        draggable 
                        onDragStart={() => {
                            if(eraser){
                                setRecicleByn(true)
                            }
                        }}
                        onDragEnd={(e) => {
                            let recicleBynPosition = e.target.x() < 200 && e.target.x() > -200 &&
                                                 e.target.y() < (window.innerHeight / 2) + 200 && e.target.y() > (window.innerHeight / 2) - 200 ? true : false
                            if (eraser && recicleBynPosition) {
                                updateObjectPosition('defaultObjects', '23', `${key}`, 9999, 9999)
                            } else {
                                updateObjectPosition('defaultObjects', '23', `${key}`, e.target.x(), e.target.y())
                            }
                            setRecicleByn(false)
                        }}
                        onClick={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                        onTap={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                    />
                ))
            }
            {
                Object.values(Object.values(history[historyIndex].defaultObjects)[24]).map((object, key) => (
                    <Image
                        key={key}
                        image={object25}
                        x={object.position.x}
                        y={object.position.y}
                        scaleX={size}
                        scaleY={size}
                        draggable 
                        onDragStart={() => {
                            if(eraser){
                                setRecicleByn(true)
                            }
                        }}
                        onDragEnd={(e) => {
                            let recicleBynPosition = e.target.x() < 200 && e.target.x() > -200 &&
                                                 e.target.y() < (window.innerHeight / 2) + 200 && e.target.y() > (window.innerHeight / 2) - 200 ? true : false
                            if (eraser && recicleBynPosition) {
                                updateObjectPosition('defaultObjects', '24', `${key}`, 9999, 9999)
                            } else {
                                updateObjectPosition('defaultObjects', '24', `${key}`, e.target.x(), e.target.y())
                            }
                            setRecicleByn(false)
                        }}
                        onClick={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                        onTap={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                    />
                ))
            }
            <Transformer
                ref={transformRef}
                centeredScaling={false}
                resizeEnabled={false}
                rotationSnapTolerance={10}
                rotationSnaps={[0, 90, 180, 270]}
                borderStroke="black"
            >
            </Transformer>
            {
                recicleByn ?
                    <>
                        <Rect
                            x={0}
                            y={0}
                            width={window.innerWidth}
                            height={window.innerHeight}
                            fill="hsla(0, 0%, 0%, 0.5)"
                        >
                        </Rect>
                        <Circle
                            radius={200}
                            x={0}
                            y={(window.innerHeight / 2)}
                            fill="hsla(0, 100%, 50%, 0.5)"
                        />
                    </>
                    : null
            }
        </>
    );
}

export default DefaultObjects;