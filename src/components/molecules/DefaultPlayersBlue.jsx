import React, { useState, useEffect, useRef } from 'react';
import { Rect, Circle, Image, Group, Text, Transformer } from 'react-konva'
import useImage from 'use-image'

const DefaultPlayersBlue = ({ history, historyIndex, currentObjectSize, width, height, updateObjectPosition, eraser }) => {

    const [player1] = useImage('https://res.cloudinary.com/leonemil/image/upload/a_90/v1602549511/Handball%20manager/player-blue-1_j6ofta.png', 'Anonymous')
    const [player2] = useImage('https://res.cloudinary.com/leonemil/image/upload/a_90/v1602549509/Handball%20manager/player-blue-2_v0q2iu.png', 'Anonymous')
    const [player3] = useImage('https://res.cloudinary.com/leonemil/image/upload/v1602549507/Handball%20manager/player-blue-3_fibk72.png', 'Anonymous')
    const [player4] = useImage('https://res.cloudinary.com/leonemil/image/upload/a_90/v1602549507/Handball%20manager/player-blue-4_xo9bse.png', 'Anonymous')
    const [player5] = useImage('https://res.cloudinary.com/leonemil/image/upload/a_90/v1602549511/Handball%20manager/player-blue-5_qsjpyh.png', 'Anonymous')
    const [player6] = useImage('https://res.cloudinary.com/leonemil/image/upload/a_90/v1602549507/Handball%20manager/player-blue-6_nvzx0g.png', 'Anonymous')
    const [player7] = useImage('https://res.cloudinary.com/leonemil/image/upload/a_90/v1602549509/Handball%20manager/player-blue-7_qwycfi.png', 'Anonymous')
    const [player8] = useImage('https://res.cloudinary.com/leonemil/image/upload/a_90/v1602549510/Handball%20manager/player-blue-8_y7w3kk.png', 'Anonymous')

    const transformRef = useRef()
    const playerRef = useRef()

    const [recicleByn, setRecicleByn] = useState(false)

    const size = currentObjectSize === "normal" ? 60 :
                 currentObjectSize === "medium" ? 45 :
                 currentObjectSize === "small" ? 30 :
                  width < 900 || height < 600 ? 30 : 60

    const fontSize = size === 60 ? 30 :
                     size === 45 ? 24 :
                     size === 30 ? 17 : 
                     null

    const textPadding = currentObjectSize === "big" ? 21 :
                        currentObjectSize === "medium" ? 17 :
                        currentObjectSize === "small" ? 10 :
                        width < 900 || height < 600 ? 10 : 21

    const rotationAngle = 90

    const toggleTransformBox = () => {
        if(transformRef.current.getNode()){
            transformRef.current.nodes([]);
            transformRef.current.getLayer().batchDraw();
        }
        else {
            transformRef.current.nodes([playerRef.current]);
            transformRef.current.getLayer().batchDraw();
        }
    }

    useEffect(() => {
        if(transformRef.current && historyIndex === 0){
            transformRef.current.nodes([]);
            transformRef.current.getLayer().batchDraw();
        }
    })

    return (
        <>
        {
            Object.values(Object.values(history[historyIndex].defaultPlayersBlue)['0']).map((player, key) => (
                <Group 
                    key={key} 
                    draggable 
                    x={player.position.x}
                    y={player.position.y}
                    onDragStart={() => {
                        if(eraser){
                            setRecicleByn(true)
                        }
                    }}
                    onDragEnd={(e) => {
                        let recicleBynPosition = e.target.x() < 200 && e.target.x() > -200 &&
                                                 e.target.y() < (window.innerHeight / 2) + 200 && e.target.y() > (window.innerHeight / 2) - 200 ? true : false
                            if (eraser && recicleBynPosition) {
                                updateObjectPosition('defaultPlayersBlue', '0', `${key}`, 9999, 9999)
                            } else {
                                updateObjectPosition('defaultPlayersBlue', '0', `${key}`, e.target.x(), e.target.y())
                            }
                        setRecicleByn(false)
                    }}
                    onClick={(e) => {
                        playerRef.current = e.currentTarget
                        toggleTransformBox()
                    }}
                    onTap={(e) => {
                        playerRef.current = e.currentTarget
                        toggleTransformBox()
                    }}
                >   
                    <Image
                        image={player1}
                        width={size}
                        height={size} 
                        />
                    <Text 
                        text={key + 1}
                        align="center"
                        verticalAlign="center"
                        x={textPadding}
                        y={textPadding}
                        fontSize={fontSize}
                        fill="white"
                    /> 
                </Group>
            ))
        }
            {
                Object.values(Object.values(Object.values(history[historyIndex].defaultPlayersBlue))[1]).map((player, key) => (
                    <Group 
                        draggable 
                        key={key} 
                        x={player.position.x}
                        y={player.position.y}
                        onDragStart={() => {
                            if (eraser) {
                                setRecicleByn(true)
                            }
                        }}
                        onDragEnd={(e) => {
                            let recicleBynPosition = e.target.x() < 200 && e.target.x() > -200 &&
                                                 e.target.y() < (window.innerHeight / 2) + 200 && e.target.y() > (window.innerHeight / 2) - 200 ? true : false
                            if (eraser && recicleBynPosition) {
                                updateObjectPosition('defaultPlayersBlue', '1', `${key}`, 9999, 9999)
                            } else {
                                updateObjectPosition('defaultPlayersBlue', '1', `${key}`, e.target.x(), e.target.y())
                            }
                            setRecicleByn(false)
                        }}
                        onClick={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                        onTap={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                    >
                        <Image
                            image={player2}
                            width={size}
                            height={size}
                        />
                        <Text
                            text={key + 1}
                            x={textPadding}
                            y={textPadding}
                            fontSize={fontSize}
                            fill="white"
                        />
                    </Group> 
                ))
            }
            {
                Object.values(Object.values(history[historyIndex].defaultPlayersBlue)[2]).map((player, key) => (
                    <Group 
                        key={key} 
                        draggable 
                        x={player.position.x}
                        y={player.position.y}
                        onDragStart={() => {
                            if (eraser) {
                                setRecicleByn(true)
                            }
                        }}
                        onDragEnd={(e) => {
                            let recicleBynPosition = e.target.x() < 200 && e.target.x() > -200 &&
                                                 e.target.y() < (window.innerHeight / 2) + 200 && e.target.y() > (window.innerHeight / 2) - 200 ? true : false
                            if (eraser && recicleBynPosition) {
                                updateObjectPosition('defaultPlayersBlue', '2', `${key}`, 9999, 9999)
                            } else {
                                updateObjectPosition('defaultPlayersBlue', '2', `${key}`, e.target.x(), e.target.y())
                            }
                            setRecicleByn(false)
                        }}
                        onClick={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                        onTap={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                    >
                        <Image
                            image={player3}
                            width={size}
                            height={size}
                        />
                        <Text
                            text={key + 1}
                            x={textPadding}
                            y={textPadding}
                            fontSize={fontSize}
                            fill="white"
                        />
                    </Group>
                ))
            }
            {
                Object.values(Object.values(history[historyIndex].defaultPlayersBlue)[3]).map((player, key) => (     
                    <Image
                        key={key}
                        image={player4}
                        x={player.position.x}
                        y={player.position.y}
                        width={size}
                        height={size}
                        draggable
                        offsetX={20}
                        offsetY={20}
                        onDragStart={() => {
                            if (eraser) {
                                setRecicleByn(true)
                            }
                        }}
                        onDragEnd={(e) => {
                            let recicleBynPosition = e.target.x() < 200 && e.target.x() > -200 &&
                                                 e.target.y() < (window.innerHeight / 2) + 200 && e.target.y() > (window.innerHeight / 2) - 200 ? true : false
                            if (eraser && recicleBynPosition) {
                                updateObjectPosition('defaultPlayersBlue', '3', `${key}`, 9999, 9999)
                            } else {
                                updateObjectPosition('defaultPlayersBlue', '3', `${key}`, e.target.x(), e.target.y())
                            }
                            setRecicleByn(false)
                        }}
                        onClick={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                        onTap={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                    />
                ))
            }
            {
                Object.values(Object.values(history[historyIndex].defaultPlayersBlue)[4]).map((player, key) => (
                    <Image
                        key={key}
                        image={player5}
                        x={player.position.x}
                        y={player.position.y}
                        width={size}
                        height={size}
                        draggable
                        offsetX={20}
                        offsetY={20}
                        onDragStart={() => {
                            if (eraser) {
                                setRecicleByn(true)
                            }
                        }}
                        onDragEnd={(e) => {
                            let recicleBynPosition = e.target.x() < 200 && e.target.x() > -200 &&
                                                 e.target.y() < (window.innerHeight / 2) + 200 && e.target.y() > (window.innerHeight / 2) - 200 ? true : false
                            if (eraser && recicleBynPosition) {
                                updateObjectPosition('defaultPlayersBlue', '4', `${key}`, 9999, 9999)
                            } else {
                                updateObjectPosition('defaultPlayersBlue', '4', `${key}`, e.target.x(), e.target.y())
                            }
                            setRecicleByn(false)
                        }}
                        onClick={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                        onTap={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                    />
                ))
            }
            {
                Object.values(Object.values(history[historyIndex].defaultPlayersBlue)[5]).map((player, key) => (
                    <Image
                        key={key}
                        image={player6}
                        x={player.position.x}
                        y={player.position.y}
                        width={size}
                        height={size}
                        draggable
                        offsetX={20}
                        offsetY={20}
                        onDragStart={() => {
                            if (eraser) {
                                setRecicleByn(true)
                            }
                        }}
                        onDragEnd={(e) => {
                            let recicleBynPosition = e.target.x() < 200 && e.target.x() > -200 &&
                                                 e.target.y() < (window.innerHeight / 2) + 200 && e.target.y() > (window.innerHeight / 2) - 200 ? true : false
                            if (eraser && recicleBynPosition) {
                                updateObjectPosition('defaultPlayersBlue', '5', `${key}`, 9999, 9999)
                            } else {
                                updateObjectPosition('defaultPlayersBlue', '5', `${key}`, e.target.x(), e.target.y())
                            }
                            setRecicleByn(false)
                        }}
                        onClick={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                        onTap={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                    />
                ))
            }
            {
                Object.values(Object.values(history[historyIndex].defaultPlayersBlue)[6]).map((player, key) => (
                    <Image
                        key={key}
                        image={player7}
                        x={player.position.x}
                        y={player.position.y}
                        width={size}
                        height={size}
                        draggable
                        offsetX={20}
                        offsetY={20}
                        onDragStart={() => {
                            if (eraser) {
                                setRecicleByn(true)
                            }
                        }}
                        onDragEnd={(e) => {
                            let recicleBynPosition = e.target.x() < 200 && e.target.x() > -200 &&
                                                 e.target.y() < (window.innerHeight / 2) + 200 && e.target.y() > (window.innerHeight / 2) - 200 ? true : false
                            if (eraser && recicleBynPosition) {
                                updateObjectPosition('defaultPlayersBlue', '6', `${key}`, 9999, 9999)
                            } else {
                                updateObjectPosition('defaultPlayersBlue', '6', `${key}`, e.target.x(), e.target.y())
                            }
                            setRecicleByn(false)
                        }}
                        onClick={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                        onTap={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                    />
                ))
            }
            {
                Object.values(Object.values(history[historyIndex].defaultPlayersBlue)[7]).map((player, key) => (
                    <Image
                        key={key}
                        image={player8}
                        x={player.position.x}
                        y={player.position.y}
                        width={size}
                        height={size}
                        draggable
                        offsetX={20}
                        offsetY={20}
                        onDragStart={() => {
                            if (eraser) {
                                setRecicleByn(true)
                            }
                        }}
                        onDragEnd={(e) => {
                            let recicleBynPosition = e.target.x() < 200 && e.target.x() > -200 &&
                                                 e.target.y() < (window.innerHeight / 2) + 200 && e.target.y() > (window.innerHeight / 2) - 200 ? true : false
                            if (eraser && recicleBynPosition) {
                                updateObjectPosition('defaultPlayersBlue', '7', `${key}`, 9999, 9999)
                            } else {
                                updateObjectPosition('defaultPlayersBlue', '7', `${key}`, e.target.x(), e.target.y())
                            }
                            setRecicleByn(false)
                        }}
                        onClick={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                        onTap={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                    />
                ))
            }
            <Transformer
                ref={transformRef}
                centeredScaling={false}
                resizeEnabled={false}
                borderStroke="black"
            >
            </Transformer> 
            {
                recicleByn ?
                <>
                    <Rect
                        x={0}
                        y={0}
                        width={window.innerWidth}
                        height={window.innerHeight}
                        fill="hsla(0, 0%, 0%, 0.5)"
                    >
                    </Rect>
                    <Circle
                        radius={200}
                        x={0}
                        y={(window.innerHeight / 2)}
                        fill="hsla(0, 100%, 50%, 0.5)"
                    />
                </>
                : null
            }
        </>
    );
}
export default DefaultPlayersBlue;