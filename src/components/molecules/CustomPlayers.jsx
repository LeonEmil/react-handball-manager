import React, { useState, useEffect, useRef } from 'react';
import { Image, Group, Text, Transformer, Rect, Circle } from 'react-konva'
import Konva from 'konva';
import useImage from 'use-image'

const CustomPlayers = ({ history, historyIndex, currentObjectSize, width, height, updateObjectPosition, eraser }) => {

    const [circle] = useImage('https://res.cloudinary.com/leonemil/image/upload/v1632064314/Handball%20manager/player-circle-white_maepm1.png', 'Anonymous')
    const [square] = useImage('https://res.cloudinary.com/leonemil/image/upload/v1632064314/Handball%20manager/player-square-white_syv3gu.png', 'Anonymous')
    const [triangle] = useImage('https://res.cloudinary.com/leonemil/image/upload/v1632064314/Handball%20manager/player-triangle-white_bcfsct.png', 'Anonymous')
    const [player1] = useImage('https://res.cloudinary.com/leonemil/image/upload/a_90/v1632871013/Handball%20manager/player-white-4-body_ryidaa.png', 'Anonymous')
    const [player2] = useImage('https://res.cloudinary.com/leonemil/image/upload/a_90/v1632870764/Handball%20manager/player-white-5-body_rwkv7w.png', 'Anonymous')
    const [playerWithBall1] = useImage('https://res.cloudinary.com/leonemil/image/upload/a_90/v1632064315/Handball%20manager/player-white-6-body_klkxfa.png', 'Anonymous')
    const [playerWithBall2] = useImage('https://res.cloudinary.com/leonemil/image/upload/a_90/v1632064315/Handball%20manager/player-white-7-body_jmqlud.png', 'Anonymous')
    const [goalkeeper] = useImage('https://res.cloudinary.com/leonemil/image/upload/a_90/v1632872226/Handball%20manager/player-white-8-body_hghrpn.png', 'Anonymous')
    
    const [player1Defense] = useImage('https://res.cloudinary.com/leonemil/image/upload/a_270/v1632871013/Handball%20manager/player-white-4-body_ryidaa.png', 'Anonymous')
    const [player2Defense] = useImage('https://res.cloudinary.com/leonemil/image/upload/a_270/v1632870764/Handball%20manager/player-white-5-body_rwkv7w.png', 'Anonymous')
    const [playerWithBall1Defense] = useImage('https://res.cloudinary.com/leonemil/image/upload/a_270/v1632064315/Handball%20manager/player-white-6-body_klkxfa.png', 'Anonymous')
    const [playerWithBall2Defense] = useImage('https://res.cloudinary.com/leonemil/image/upload/a_270/v1632064315/Handball%20manager/player-white-7-body_jmqlud.png', 'Anonymous')
    const [goalkeeperDefense] = useImage('https://res.cloudinary.com/leonemil/image/upload/a_270/v1632872226/Handball%20manager/player-white-8-body_hghrpn.png', 'Anonymous')

    const [player1Parts] = useImage('https://res.cloudinary.com/leonemil/image/upload/a_90/v1632870765/Handball%20manager/player-white-4-parts_j0akbv.png', 'Anonymous')
    const [player2Parts] = useImage('https://res.cloudinary.com/leonemil/image/upload/a_90/v1632870765/Handball%20manager/player-white-5-parts_utmxak.png', 'Anonymous')
    const [playerWithBall1Parts] = useImage('https://res.cloudinary.com/leonemil/image/upload/a_90/v1632064315/Handball%20manager/player-white-6-parts_i1zfwb.png', 'Anonymous')
    const [playerWithBall2Parts] = useImage('https://res.cloudinary.com/leonemil/image/upload/a_90/v1632064316/Handball%20manager/player-white-7-parts_z5evfk.png', 'Anonymous')
    const [goalkeeperParts] = useImage('https://res.cloudinary.com/leonemil/image/upload/a_90/v1632872227/Handball%20manager/player-white-8-parts_h1btge.png', 'Anonymous')
    
    const [player1DefenseParts] = useImage('https://res.cloudinary.com/leonemil/image/upload/a_270/v1632064315/Handball%20manager/player-white-4-parts_j0akbv.png', 'Anonymous')
    const [player2DefenseParts] = useImage('https://res.cloudinary.com/leonemil/image/upload/a_270/v1632064315/Handball%20manager/player-white-5-parts_utmxak.png', 'Anonymous')
    const [playerWithBall1DefenseParts] = useImage('https://res.cloudinary.com/leonemil/image/upload/a_270/v1632064315/Handball%20manager/player-white-6-parts_i1zfwb.png', 'Anonymous')
    const [playerWithBall2DefenseParts] = useImage('https://res.cloudinary.com/leonemil/image/upload/a_270/v1632064316/Handball%20manager/player-white-7-parts_z5evfk.png', 'Anonymous')
    const [goalkeeperDefenseParts] = useImage('https://res.cloudinary.com/leonemil/image/upload/a_270/v1632064315/Handball%20manager/player-white-8-parts_h1btge.png', 'Anonymous')

    const [transparent] = useImage('https://www.cfenterprises.org/wp-content/uploads/2019/04/Snow-PNG-Transparent-Image.png')

    const getImage = (value, position) => {
        if(value === "Círculo") return circle
        if(value === "Cuadrado") return square
        if(value === "Triángulo") return triangle
        if(value === "Jugador (con brazo derecho)" && position === "defense") return player1Defense
        if(value === "Jugador (con brazo izquierdo)" && position === "defense") return player2Defense
        if(value === "Jugador (con pelota en brazo derecho)" && position === "defense") return playerWithBall1Defense
        if(value === "Jugador (con pelota en brazo izquierdo)" && position === "defense") return playerWithBall2Defense
        if(value === "Jugador (con brazo derecho)") return player1
        if(value === "Jugador (con brazo izquierdo)") return player2
        if(value === "Jugador (con pelota en brazo derecho)") return playerWithBall1
        if(value === "Jugador (con pelota en brazo izquierdo)") return playerWithBall2
        if(value === "Portero" && position === "defense") return goalkeeperDefense
        if(value === "Portero") return goalkeeper
    }

    const getSecondImage = (value, position) => {
        if(value === "Círculo") return transparent
        if(value === "Cuadrado") return transparent
        if(value === "Triángulo") return transparent
        if(value === "Jugador (con brazo derecho)" && position === "defense") return player1DefenseParts
        if(value === "Jugador (con brazo izquierdo)" && position === "defense") return player2DefenseParts
        if(value === "Jugador (con pelota en brazo derecho)" && position === "defense") return playerWithBall1DefenseParts
        if(value === "Jugador (con pelota en brazo izquierdo)" && position === "defense") return playerWithBall2DefenseParts
        if(value === "Jugador (con brazo derecho)") return player1Parts
        if(value === "Jugador (con brazo izquierdo)") return player2Parts
        if(value === "Jugador (con pelota en brazo derecho)") return playerWithBall1Parts
        if(value === "Jugador (con pelota en brazo izquierdo)") return playerWithBall2Parts
        if(value === "Portero" && position === "defense") return goalkeeperDefenseParts
        if(value === "Portero") return goalkeeperParts
    }

    const transformRef = useRef()
    const playerRef = useRef()
    const filterAttackRefs = useRef([])
    const filterDefenseRefs = useRef([])
    filterAttackRefs.current = Object.values(history[historyIndex]["customAttackTeam"].players).map((element, i) => filterAttackRefs.current[i] ?? React.createRef())
    filterDefenseRefs.current = Object.values(history[historyIndex]["customDefenseTeam"].players).map((element, i) => filterDefenseRefs.current[i] ?? React.createRef())
    
    const [recicleByn, setRecicleByn] = useState(false)

    const size = currentObjectSize === "normal" ? 60 :
                 currentObjectSize === "medium" ? 45 :
                 currentObjectSize === "small" ? 30 :
                  width < 900 || height < 600 ? 30 : 60

    const fontSize = size === 60 ? 30 :
                     size === 45 ? 24 :
                     size === 30 ? 17 : 
                     null

    const textPadding = currentObjectSize === "big" ? 21 :
                        currentObjectSize === "medium" ? 17 :
                        currentObjectSize === "small" ? 10 :
                        width < 900 || height < 600 ? 10 : 21

    const toggleTransformBox = () => {
        if(transformRef.current.getNode()){
            transformRef.current.nodes([]);
            transformRef.current.getLayer().batchDraw();
        }
        else {
            transformRef.current.nodes([playerRef.current]);
            transformRef.current.getLayer().batchDraw();
        }
    }

    useEffect(() => {
        Object.values(history[historyIndex]["customAttackTeam"].players).map((element, i) => {
            if(filterAttackRefs.current[i].current){
                filterAttackRefs.current[i].current.cache();
                filterAttackRefs.current[i].current.red(parseInt(history[historyIndex]["customAttackTeam"].color.substring(1,3), 16))
                filterAttackRefs.current[i].current.green(parseInt(history[historyIndex]["customAttackTeam"].color.substring(3,5), 16))
                filterAttackRefs.current[i].current.blue(parseInt(history[historyIndex]["customAttackTeam"].color.substring(5,7), 16))
            }
        })
        Object.values(history[historyIndex]["customDefenseTeam"].players).map((element, i) => {
            if(filterDefenseRefs.current[i].current){
                filterDefenseRefs.current[i].current.cache();
                filterDefenseRefs.current[i].current.red(parseInt(history[historyIndex]["customDefenseTeam"].color.substring(1,3), 16))
                filterDefenseRefs.current[i].current.green(parseInt(history[historyIndex]["customDefenseTeam"].color.substring(3,5), 16))
                filterDefenseRefs.current[i].current.blue(parseInt(history[historyIndex]["customDefenseTeam"].color.substring(5,7), 16))
            }
        })
        if(transformRef.current && historyIndex === 0){
            transformRef.current.nodes([]);
            transformRef.current.getLayer().batchDraw();
        }
    })

    return (
        <>
            {
            Object.values(history[historyIndex]["customAttackTeam"]["players"]).map((player, key) => (
                <Group 
                    key={key} 
                    draggable 
                    x={player.position.x}
                    y={player.position.y}
                    onDragStart={() => {
                        if(eraser){
                            setRecicleByn(true)
                        }
                    }}
                    onDragEnd={(e) => {
                        let recicleBynPosition = e.target.x() < 200 && e.target.x() > -200 &&
                                                 e.target.y() < (window.innerHeight / 2) + 200 && e.target.y() > (window.innerHeight / 2) - 200 ? true : false
                            if (eraser && recicleBynPosition) {
                                updateObjectPosition('customAttackTeam', '0', `${key}`, 9999, 9999)
                            } else {
                                updateObjectPosition('customAttackTeam', '0', `${key}`, e.target.x(), e.target.y())
                            }
                        setRecicleByn(false)
                    }}
                    onClick={(e) => {
                        playerRef.current = e.currentTarget
                        toggleTransformBox()
                    }}
                    onTap={(e) => {
                        playerRef.current = e.currentTarget
                        toggleTransformBox()
                    }}
                >   
                    <Image
                        image={getImage(player.image, "attack")}
                        width={size}
                        height={size}
                        filters={[Konva.Filters.RGB]} 
                        ref={filterAttackRefs.current[key]}
                    />
                    <Image
                        image={getSecondImage(player.image, "attack")}
                        width={size}
                        height={size}
                    />
                    <Text 
                        text={player.number}
                        align="center"
                        verticalAlign="center"
                        x={textPadding}
                        y={textPadding}
                        fontSize={fontSize}
                        fill="white"
                        stroke="black"
                        strokeWidth={0.5}
                    />
                    <Text 
                        text={player.name}
                        align="center"
                        verticalAlign="center"
                        x={0}
                        y={size}
                        width={size}
                        fontSize={12}
                        fill="black"
                    /> 
                </Group>
            ))
            }
            {
            Object.values(history[historyIndex]["customDefenseTeam"]["players"]).map((player, key) => (
                <Group 
                    key={key} 
                    draggable 
                    x={player.position.x}
                    y={player.position.y}
                    onDragStart={() => {
                        if(eraser){
                            setRecicleByn(true)
                        }
                    }}
                    onDragEnd={(e) => {
                        let recicleBynPosition = e.target.x() < 200 && e.target.x() > -200 &&
                                                 e.target.y() < (window.innerHeight / 2) + 200 && e.target.y() > (window.innerHeight / 2) - 200 ? true : false
                            if (eraser && recicleBynPosition) {
                                updateObjectPosition('customDefenseTeam', '0', `${key}`, 9999, 9999)
                            } else {
                                updateObjectPosition('customDefenseTeam', '0', `${key}`, e.target.x(), e.target.y())
                            }
                        setRecicleByn(false)
                    }}
                    onClick={(e) => {
                        playerRef.current = e.currentTarget
                        console.log(playerRef.current)
                        toggleTransformBox()
                    }}
                    onTap={(e) => {
                        playerRef.current = e.currentTarget
                        toggleTransformBox()
                    }}
                >   
                    <Image
                        image={getImage(player.image, "defense")}
                        width={size}
                        height={size} 
                        filters={[Konva.Filters.RGB]} 
                        ref={filterDefenseRefs.current[key]}
                    />
                    <Image
                        image={getSecondImage(player.image, "defense")}
                        width={size}
                        height={size} 
                    />
                    <Text 
                        text={key + 1}
                        align="center"
                        verticalAlign="center"
                        x={textPadding}
                        y={textPadding}
                        fontSize={fontSize}
                        fill="white"
                        stroke="black"
                        strokeWidth={0.5}
                    />
                    <Text 
                        text={player.name}
                        align="center"
                        verticalAlign="center"
                        x={0}
                        y={size}
                        width={size}
                        fontSize={12}
                        fill="black"
                    /> 
                </Group>
            ))
            }
            <Transformer
                ref={transformRef}
                centeredScaling={false}
                resizeEnabled={false}
                borderStroke="black"
            >
            </Transformer> 
            {
                recicleByn ?
                <>
                    <Rect
                        x={0}
                        y={0}
                        width={window.innerWidth}
                        height={window.innerHeight}
                        fill="hsla(0, 0%, 0%, 0.5)"
                    >
                    </Rect>
                    <Circle
                        radius={200}
                        x={0}
                        y={(window.innerHeight / 2)}
                        fill="hsla(0, 100%, 50%, 0.5)"
                    />
                </>
                : null
            }
        </>
    );
}


export default CustomPlayers;