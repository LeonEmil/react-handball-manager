import React, { useState, useEffect, useRef } from 'react';
import { Stage, Layer, Group, Shape, Line, Rect, Circle, Image } from 'react-konva';
import Background from './Background'

import { connect } from 'react-redux'
import DefaultPlayersBlue from './DefaultPlayersBlue';
import DefaultPlayersRed from './DefaultPlayersRed';
import DefaultObjects from './DefaultObjects';
import CustomPlayers from './../molecules/CustomPlayers'
import { createLine, setLine, toggleDrawing, updateObjectPosition, updateCustomPlayerPosition } from './../../redux/actionCreators'

const Canvas = (props) => {

    const [recicleByn, setRecicleByn] = useState(false)

    const stageRef = useRef()
    const [orientation, setOrientation] = useState()

    useEffect(() => {
        if(window.matchMedia(`orientation: landscape`)){
            setOrientation('landscape')
        }   
        else {
            setOrientation('portait')
            console.log('portrait')
        }      
    })

    const handleMouseDown = () => {
        if(!props.eraser && props.enableDraw){
            props.createLine(props.currentLineColor, props.currentLineType)
            props.toggleDrawing(true)
        }
    }
    
    const handleMouseMove = e => {
        if(props.isDrawing && !props.eraser){
            const stage = stageRef.current.getStage();
            const point = stage.getPointerPosition();
            props.setLine(point.x, point.y)
        }
    }
    
    const handleMouseUp = () => {
        props.toggleDrawing(false)
    }

    const arrowSize = props.canvasWidth < 900 || props.canvasHeight < 600 ? 10 : 20
    return (
        <>
        <Stage
            width={props.canvasWidth}
            height={props.canvasHeight}  
            ref={stageRef}
        >
            <Layer>
                <Background
                    backgroundPath={props.currentBackgroundPath}
                    backgroundAngle={props.currentBackgroundAngle}
                    backgroundImage={props.currentBackgroundCanvas}
                    height={props.canvasHeight}
                    width={props.canvasWidth}
                />  
            </Layer>
            <Layer
                onMouseDown={handleMouseDown}
                onMouseMove={handleMouseMove}
                onMouseUp={handleMouseUp}
                onTouchStart={handleMouseDown}
                onTouchMove={handleMouseMove}
                onTouchEnd={handleMouseUp}
                rotation={props.currentBackgroundAngle === "a_0" ? 0 : 270}
                y={props.currentBackgroundAngle === "a_0" ? 0 : props.canvasHeight}
                scaleX={props.currentBackgroundAngle === 'a_0' ? 1 : 0.9}
            >
                <Background 
                    backgroundPath={props.currentBackgroundPath}
                    backgroundAngle={props.currentBackgroundAngle}
                    backgroundImage={props.currentBackgroundCanvas}
                    height={props.canvasHeight}
                    width={props.canvasWidth}
                />                                     
                
                {
                    Object.values(props.history[props.historyIndex].lines).map((line, i) => (
                        <Group 
                            key={i} 
                            draggable={props.eraser ? true : false}
                            onDragStart={() => {
                                if (props.eraser) {
                                    setRecicleByn(true)
                                }
                            }}
                            onDragEnd={(e) => {
                                let recicleBynPosition = e.target.x() < 1500 && e.target.x() > -1500 &&
                                                         e.target.y() < 1500 && e.target.y() > -1500 ? true : false
                                if (props.eraser && recicleBynPosition) {
                                    e.target.setAttr('x', 10000)
                                }
                                setRecicleByn(false)
                            }}
                            >
                            <Line 
                                points={Object.values(line.linePoints)} 
                                strokeWidth={props.canvasWidth < 900 || props.canvasHeight < 600 ? 2 : 5} 
                                stroke={line.lineColor} 
                                dash={parseInt(line.lineType.slice(-1)) % 2 === 0 ? [10,10] : []} 
                            />
                            <Line 
                                points={Object.values(line.linePoints)} 
                                strokeWidth={25} 
                                stroke="hsla(0, 0%, 100%, 0)" 
                            />
                            <Shape
                                sceneFunc={(context, shape) => {
                                    let startX = Object.values(line.linePoints)[Object.values(line.linePoints).length - 4] 
                                    let startY = Object.values(line.linePoints)[Object.values(line.linePoints).length - 3] 
                                    let endX = Object.values(line.linePoints)[Object.values(line.linePoints).length - 2]
                                    let endY = Object.values(line.linePoints)[Object.values(line.linePoints).length - 1]
                                    let deltaX = endX - startX
                                    let deltaY = endY - startY
                                    let angle = Math.atan2(deltaY, deltaX)
                                    
                                    context.translate(endX, endY)
                                    context.rotate(angle)
                                    
                                    switch (line.lineType) {
                                        case "linea 3":
                                            context.beginPath();
                                            context.moveTo(0, 0);
                                            context.lineTo(-arrowSize, arrowSize);
                                            context.moveTo(0, 0)
                                            context.lineTo(-arrowSize, -arrowSize);
                                            context.strokeStyle = line.lineColor
                                            context.lineWidth = props.canvasWidth < 900 || props.canvasHeight < 600 ? 2 : 5
                                            context.stroke()
                                            context.closePath();
                                            break;

                                        case "linea 4":
                                            context.beginPath();
                                            context.moveTo(0, 0);
                                            context.lineTo(-arrowSize, arrowSize);
                                            context.moveTo(0, 0)
                                            context.lineTo(-arrowSize, -arrowSize);
                                            context.strokeStyle = line.lineColor
                                            context.lineWidth = props.canvasWidth < 900 || props.canvasHeight < 600 ? 2 : 5
                                            context.stroke()
                                            context.closePath();
                                            break;
                                            
                                        case "linea 5":
                                            context.beginPath();
                                            context.moveTo(0, 0);
                                            context.lineTo(0, -arrowSize);
                                            context.moveTo(0, 0)
                                            context.lineTo(0, arrowSize);
                                            context.strokeStyle = line.lineColor
                                            context.lineWidth = props.canvasWidth < 900 || props.canvasHeight < 600 ? 2 : 5
                                            context.stroke()
                                            context.closePath();
                                            break;
                                            
                                        case "linea 6":
                                            context.beginPath();
                                            context.moveTo(0, 0);
                                            context.lineTo(0, -arrowSize);
                                            context.moveTo(0, 0)
                                            context.lineTo(0, arrowSize);
                                            context.strokeStyle = line.lineColor
                                            context.lineWidth = props.canvasWidth < 900 || props.canvasHeight < 600 ? 2 : 5
                                            context.stroke()
                                            context.closePath();
                                            break;

                                        case "linea 7":
                                            context.beginPath();
                                            context.moveTo(0, 0);
                                            context.lineTo(-arrowSize, -arrowSize);
                                            context.moveTo(0, 0)
                                            context.lineTo(-arrowSize, arrowSize);
                                            context.moveTo(0, 0)
                                            context.lineTo(arrowSize, -arrowSize);
                                            context.moveTo(0, 0)
                                            context.lineTo(arrowSize, arrowSize);
                                            context.strokeStyle = line.lineColor
                                            context.lineWidth = props.canvasWidth < 900 || props.canvasHeight < 600 ? 2 : 5
                                            context.stroke()
                                            context.closePath();
                                            break;

                                        case "linea 8":
                                            context.beginPath();
                                            context.moveTo(0, 0);
                                            context.lineTo(-arrowSize, -arrowSize);
                                            context.moveTo(0, 0)
                                            context.lineTo(-arrowSize, arrowSize);
                                            context.moveTo(0, 0)
                                            context.lineTo(arrowSize, -arrowSize);
                                            context.moveTo(0, 0)
                                            context.lineTo(arrowSize, arrowSize);
                                            context.strokeStyle = line.lineColor
                                            context.lineWidth = props.canvasWidth < 900 || props.canvasHeight < 600 ? 2 : 5
                                            context.stroke()
                                            context.closePath();
                                            break;
                                            
                                            default:
                                            break;
                                    }
                                    
                                    context.rotate(0)
                                    context.translate(0, 0)
                                    context.fillStrokeShape(shape);
                                }}
                                stroke={line.color}
                                strokeWidth={5}
                            />
                        </Group>
                    ))
                }
            </Layer>
            
            <Layer
                rotation={props.currentBackgroundAngle === "a_0" ? 0 : 270}
                y={props.currentBackgroundAngle === "a_0" ? 0 : props.canvasHeight}
                scaleX={props.currentBackgroundAngle === 'a_0' ? 1 : 0.9}
            >
                <DefaultPlayersBlue 
                    history={props.history}
                    historyIndex={props.historyIndex} 
                    toggleDrawing={props.toggleDrawing} 
                    eraser={props.eraser}
                    width={props.canvasWidth} 
                    height={props.canvasHeight}
                    currentObjectSize={props.currentObjectSize}
                    updateObjectPosition={props.updateObjectPosition}  
                    currentBackgroundAngle={props.currentBackgroundAngle} 
                    recicleByn={props.recicleByn}               
                />
                <DefaultPlayersRed 
                    history={props.history}
                    historyIndex={props.historyIndex} 
                    toggleDrawing={props.toggleDrawing}
                    eraser={props.eraser} 
                    width={props.canvasWidth} 
                    height={props.canvasHeight}
                    currentObjectSize={props.currentObjectSize}
                    updateObjectPosition={props.updateObjectPosition}
                    currentBackgroundAngle={props.currentBackgroundAngle}
                    recicleByn={props.recicleByn}
                />
                <DefaultObjects 
                    history={props.history}
                    historyIndex={props.historyIndex}
                    toggleDrawing={props.toggleDrawing} 
                    eraser={props.eraser}
                    width={props.canvasWidth} 
                    height={props.canvasHeight}
                    currentObjectSize={props.currentObjectSize}
                    updateObjectPosition={props.updateObjectPosition}
                    currentBackgroundAngle={props.currentBackgroundAngle}
                    recicleByn={props.recicleByn}
                />
                <CustomPlayers 
                    history={props.history}
                    historyIndex={props.historyIndex}
                    toggleDrawing={props.toggleDrawing} 
                    eraser={props.eraser}
                    width={props.canvasWidth} 
                    height={props.canvasHeight}
                    currentObjectSize={props.currentObjectSize}
                    updateObjectPosition={props.updateObjectPosition}
                    currentBackgroundAngle={props.currentBackgroundAngle}
                    recicleByn={props.recicleByn}
                />
                    {
                        recicleByn ?
                            <>
                                <Rect
                                    x={0}
                                    y={0}
                                    width={window.innerWidth}
                                    height={window.innerHeight}
                                    fill="hsla(0, 0%, 0%, 0.5)"
                                >
                                </Rect>
                                <Circle
                                    radius={200}
                                    x={0}
                                    y={(window.innerHeight / 2)}
                                    fill="hsla(0, 100%, 50%, 0.5)"
                                />
                            </>
                            : null
                    }
            </Layer>
        </Stage>
        </>
    )
}

const mapStateToProps = state => {
    return {
        currentUser: state.currentUser,
        history: state.history,
        historyIndex: state.historyIndex,
        currentObjectSize: state.currentObjectSize,
        currentLineColor: state.currentLineColor,
        currentLineType: state.currentLineType,
        currentBackgroundPath: state.currentBackgroundPath,
        currentBackgroundAngle: state.currentBackgroundAngle,
        currentBackgroundCanvas: state.currentBackgroundCanvas,
        enableDraw: state.enableDraw,
        isDrawing: state.isDrawing,
        eraser: state.eraser,
        recicleByn: state.recicleByn
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        createLine: (currentLineColor, currentLineType) => { dispatch(createLine(currentLineColor, currentLineType)) },
        setLine: (x, y) => { dispatch(setLine(x, y)) },
        toggleDrawing: (boolean) => { dispatch(toggleDrawing(boolean)) },
        updateObjectPosition: (objectType, typeId, elementId, x, y) => { dispatch(updateObjectPosition(objectType, typeId, elementId, x, y)) },
        updateCustomPlayerPosition: (objectType, elementId, x, y) => { dispatch(updateCustomPlayerPosition(objectType, elementId, x, y)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Canvas)