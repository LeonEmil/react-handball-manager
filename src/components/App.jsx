/*
 Resumen de la aplicación y sus partes:

 La aplicación de handball fue creada usando las siguientes herramientas principalmente:
 - React
 - Redux
 - React router dom
 - Sass
 - Firebase
 - Konvajs
 - Semantic ui react
 - Alertify

El flujo de ejecución general del programa funciona de la siguiente manera:
1 - Al comenzar el programa comprueba si hay un usuario logeado verificando la propiedad currentUser en el store de redux.
2 - Si el usuario es undefined el usuario es redirigido a /ingresar. De otra manera es dirigido a la aplicación
3 - Se hace una petición asincrona a la base de datos de firebase para traer todos los datos del usuario actual incluyendo:
  • Nombre, 
  • Email, 
  • Foto (avatar), 
  • Estrategias, 
  • Rol de usuario (role), 
  • Equipos personalizados (customTeams), 
  • Equipo personalizado por defecto (myTeam)
  • Otros datos (otherData).
4 - Los datos del usuario son guardado en el store de redux para poder usarlos en diferentes partes de la aplicación
5 - Los datos del usuario se envian los diferentes paneles que aparecen al activar las opciones de los menus de la aplicación que incluyen: 
  • Config panel (muestra las opciones de configuracion de la aplicación y permite elegir, crear y editar equipos personalizados)
  • Draw panel (muestra una lista de formas y colores de lineas que pueden activarse o desactivarse desde este panel)
  • Mail panel (permite al usuario enviar un mail mediante un formulario)
  • Objects panel (muestra una lista de tipos de objetos que el usuario puede colocar en la cancha)
  • Players panel (muestra una lista de tipos de jugadores que el usuario puede colocar en la cancha)
  • Strategy panel (muestra una lista de estrategias guardadas por el usuario que están ordenadas en carpetas y pueden ser editadas o borradas)
  • User panel (muestra las opciones de configuración del usuario)
6 - Se renderiza la cancha y los jugadores por defecto

Funcionamiento de la renderización de la cancha y los jugadores:
Los gráficos y animaciones que permiten dibujar en pantalla la cancha y los jugadores son creados mediante la libreria konvajs. Esta libreria permite
crear gráficos y animaciones usando la api canvas y componentes de react que permiten crear imágenes, texto, animaciones, filtros y otras herramientas.
Konvajs funciona mediante un componente llamado stage dentro del cual se pueden crear capas llamando al componente layer. Estas capas son elementos canvas
que son creados automáticamente el el DOM al renderizarse el componente.
 La aplicación tiene tres capas en las que se renderizan el diseño de la cancha en la primera, las lineas en la segunda, los jugadores y objetos en la tercera.
 Esto es así para que el usuario pueda interactuar con cada capa independientemente y la interacción de una no afecte a las demás, por ejemplo al arrastrar
 jugadores o dibujar lineas.
 Todo lo anterior descrito se encuentra en el archivo src/components/molecules/Canvas.jsx

Funcionamiento del history:
 History es un array que permite guardar todas las acciones del usuario en la cancha y retroceder o avanzar en ellas. Contiene objetos que guardan el estado de 
cada linea, jugador y objeto junto con su posición, forma, color y otras propiedades.
 El array history está guardado en el store de redux y es llamado para renderizar cada linea, jugador y objeto en su posición en un momento en el tiempo.
El history funciona junto con otra propiedad del store de redux llamado historyIndex. HistoryIndex es un número que funciona como índice que permite
recorrer el history cambiando de un estado a otro en el tiempo.
Al iniciar la aplicación el history tiene dos objetos. El primero en el índice 0 no contiene ningún jugador, objeto ni linea. El segundo contiene 
los jugadores azul y rojo por defecto a los lados de la cancha. 
 El historyIndex al iniciar la aplicación es igual a 1, por eso se muestran los jugadores rojo y azul por defecto al iniciar.
 El botón "limpiar cancha" en el menú principal de la aplicación lo que hace realmente es igualar el historyIndex a 0 que es el estado en el que no hay 
 nada.
 Este estado inicial del history se guarda en el archivo src/js/history.js que es importado al reducer como valor inicial del history en el store de redux.
 Con cada acción realizada por el usuario en la cancha se crea un nuevo objeto en el history y se actualiza el historyIndex sumando 1 para leer el último 
 estado del history.
 Los botones "retroceder" o "avanzar" del menú principal permiten recorrer todos los estados creados por el usuario en el history sumando o restando 1 
 al historyIndex.
 El history y el historyIndex no solo son guardados en el store de redux sino también en la base de datos de firebase. Esto permite guardar y recuperar
 la última estrategia creada por el usuario.
 Al inciar la aplicación se pregunta al usuario si desea cargar la última estrategia guardada. Al aceptar se carga en el store la última estrategia 
 guardada por el usuario en su anterior sesión cargando todo el history y historyIndex con los valores que tenia en ese momento.

Soluciones a problemas encontrados en la aplicación:
- Function DocumentReference.set() called with invalid data. Nested arrays are not supported.
 Este error aparece en consola al intentar guardar arrays anidados en la base de datos de firebase que no son soportados en versiones recientes.
 Para solucionar este problema se convierten los arrays anidados en objetos usando el método Object.assing() para guardarlos en firebase. Luego estos
 objetos pueden volver a convertirse en arrays iterables usando Object.values() y pueden leerse al traerlos de la base de datos.
*/

import React from 'react';
import { BrowserRouter as Router, Route, Switch, Redirect} from 'react-router-dom'
import Home from './pages/Home'
import Login from './pages/Login'
import Register from './pages/Register'
import NotFound from './molecules/NotFound'
import './../sass/styles.scss'
import { connect } from 'react-redux'
import { auth, db } from './../firebase/firebase'
import { setUser, changeBackgroundAngle, setConfigData, restoreHistory } from './../redux/actionCreators'
import initialHistory from './../js/history'
import alertify from 'alertifyjs'

// Configuración de la libreria alertify 

alertify.defaults = {
   // dialogs defaults
   autoReset:true,
   basic:false,
   closable:true,
   closableByDimmer:true,
   invokeOnCloseOff:false,
   frameless:false,
   defaultFocusOff:false,
   maintainFocus:true, // <== global default not per instance, applies to all dialogs
   maximizable:true,
   modal:true,
   movable:true,
   moveBounded:false,
   overflow:true,
   padding: true,
   pinnable:true,
   pinned:true,
   preventBodyShift:false, // <== global default not per instance, applies to all dialogs
   resizable:true,
   startMaximized:false,
   transition:'pulse',
   transitionOff:false,
   tabbable:'button:not(:disabled):not(.ajs-reset),[href]:not(:disabled):not(.ajs-reset),input:not(:disabled):not(.ajs-reset),select:not(:disabled):not(.ajs-reset),textarea:not(:disabled):not(.ajs-reset),[tabindex]:not([tabindex^="-"]):not(:disabled):not(.ajs-reset)',  // <== global default not per instance, applies to all dialogs

   // notifier defaults
   notifier:{
   // auto-dismiss wait time (in seconds)  
       delay:5,
   // default position
       position:'bottom-right',
   // adds a close button to notifier messages
       closeButton: false,
   // provides the ability to rename notifier classes
       classes : {
           base: 'alertify-notifier',
           prefix:'ajs-',
           message: 'ajs-message',
           top: 'ajs-top',
           right: 'ajs-right',
           bottom: 'ajs-bottom',
           left: 'ajs-left',
           center: 'ajs-center',
           visible: 'ajs-visible',
           hidden: 'ajs-hidden',
           close: 'ajs-close'
       }
   },

   // language resources 
   glossary:{
       // dialogs default title
       title:'Handball manager',
       // ok button text
       ok: 'Aceptar',
       // cancel button text
       cancel: 'Cancelar'            
   },

   // theme settings
   theme:{
       // class name attached to prompt dialog input textbox.
       input:'ajs-input',
       // class name attached to ok button
       ok:'ajs-ok',
       // class name attached to cancel button 
       cancel:'ajs-cancel'
   },
   // global hooks
   hooks:{
       // invoked before initializing any dialog
       preinit:function(instance){},
       // invoked after initializing any dialog
       postinit:function(instance){},
   },
}

class App extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      width: window.innerWidth,
      height: window.innerHeight
    }
  }
  
  updateDimensions() {
    if (this.state.width !== window.innerWidth) {
      this.setState({ width: window.innerWidth, height: window.innerHeight });
    } 
  }
  
  componentDidMount() {

    // Redimensionamiento inicial del canvas
    this.updateDimensions()
    window.addEventListener("resize", this.updateDimensions.bind(this));
    window.addEventListener("orientationchange", this.updateDimensions.bind(this));

    // Configuración del ángulo del fondo de pantalla
    if(this.state.width < this.state.height){
      this.props.changeBackgroundAngle("a_270")
    }
    else {
      this.props.changeBackgroundAngle("a_0")
    }

    //Petición de datos del panel de administración
    db.collection('data').doc('kjjf8CeDzGMRBM7h5NLH').get().then(response => {
      let configData = {
        bannerAlt: response.data().bannerAlt,
        bannerUrl: response.data().bannerUrl,
        logoUrl: response.data().logoUrl
      }
      this.props.setConfigData(configData)
    })

    // Comprobación y guardado de datos del usuario en el store de redux
    auth.onAuthStateChanged(user => {
      if (user) {
        db.collection('users').where('email', '==', `${user.email}`).get()
        .then(response => {
          if(response.docs[0]){
            let currentUser = {
              name: response.docs[0].data().name,
              email: response.docs[0].data().email,
              avatar: response.docs[0].data().avatar,
              strategies: response.docs[0].data().strategies,
              videos: response.docs[0].data().videos,
              role: response.docs[0].data().role,
              myTeam: response.docs[0].data().myTeam,
              customTeams: response.docs[0].data().customTeams,
              otherData: response.docs[0].data().otherData,
              id: response.docs[0].data().id
            }
            this.props.setUser(currentUser)

            // Cargar última formación
            if(response.docs[0].data().history !== initialHistory && this.props.currentUser && document.location.href.slice(-6) !== '/admin'){
              alertify.confirm("¿Desea cargar la última formación guardada?", () => {
                this.props.restoreHistory(response.docs[0].data().history, response.docs[0].data().historyIndex)
              })
            }
          }
        })
        .catch((e) => {
          console.log(e)
        })
      }
      else {
        this.props.setUser(user)
      }
    })
  }
  
  componentDidUpdate(){
    // Redimensionamiento del canvas cuando se detecta un redimensionamiento o cambio en la orientación de la pantalla
    this.updateDimensions()
    window.addEventListener("resize", () => {
      this.updateDimensions.bind(this);
      if (this.state.width < this.state.height) {
        this.props.changeBackgroundAngle("a_270")
      }
      else {
        this.props.changeBackgroundAngle("a_0")
      }
    })
    window.addEventListener("orientationchange", (e) => {
      e.preventDefault()
      this.updateDimensions.bind(this)
      if (this.state.width < this.state.height){
        this.props.changeBackgroundAngle("a_270")
      }
      else {
        this.props.changeBackgroundAngle("a_0")
      }
    });

    // Configuración del ángulo del fondo de pantalla
    
    // Comprobación y guardado de datos del usuario en el store de redux
    auth.onAuthStateChanged(user => {
      if (user) {
        db.collection('users').where('email', '==', `${user.email}`).get()
          .then(response => {
              if(response.docs[0]){
                let currentUser = {
                  name: response.docs[0].data().name,
                  email: response.docs[0].data().email,
                  avatar: response.docs[0].data().avatar,
                  strategies: response.docs[0].data().strategies,
                  customTeams: response.docs[0].data().customTeams,
                  myTeam: response.docs[0].data().myTeam,
                  videos: response.docs[0].data().videos,
                  otherData: response.docs[0].data().otherData,
                  role: response.docs[0].data().role,
                  id: response.docs[0].data().id
                }
                this.props.setUser(currentUser)
              }
          })
          .catch((e) => {
            console.log(e)
          })
      }
      else {
        this.props.setUser(user)
      }
    })
  }
  
  render(){
    return this.props.currentUser ? (
      <Router basename={process.env.PUBLIC_URL}>  
        <Switch>
          <Route exact path="/">
            <Home canvasWidth={this.state.width} canvasHeight={this.state.height}/>
          </Route>
          <Route path="/ingresar">
            <Login />
          </Route>
          <Route path="/admin">
            <Register canvasWidth={this.state.width} canvasHeight={this.state.height} currentUser={this.props.currentUser}/>
          </Route>
          <Route component={() => { return <NotFound /> }}></Route>
        </Switch>
      </Router>
    ) :
    (
        <Router basename={process.env.PUBLIC_URL}>
          <Switch>
            <Route exact path="/">
              <Home canvasWidth={this.state.width} canvasHeight={this.state.height} />
            </Route>
            <Route path="/ingresar">
              <Login />
            </Route>
            <Route path="/admin">
              <Register canvasWidth={this.state.width} canvasHeight={this.state.height} currentUser={this.props.currentUser}/>
            </Route>
            <Route component={() => { return <NotFound /> }}></Route>
          </Switch>
          <Redirect to="/ingresar" />
        </Router>
    )
  }
};

const mapStateToProps = (state) => {
  return {
    currentUser: state.currentUser,
    currentBackgroundAngle: state.currentBackgroundAngle
  }
}

const mapDispatchToProps = dispatch => {
  return {
    setUser: (currentUser) => { dispatch(setUser(currentUser)) },
    changeBackgroundAngle: (angle) => { dispatch(changeBackgroundAngle(angle)) },
    setConfigData: (data) => { dispatch(setConfigData(data)) },
    restoreHistory: (history, historyIndex) => { dispatch(restoreHistory(history, historyIndex)) }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App)