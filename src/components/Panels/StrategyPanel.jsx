import React, { useState, useEffect, useRef } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimes } from '@fortawesome/free-solid-svg-icons'
import { connect } from 'react-redux'
import StrategyList from './StrategyList'
import { togglePanel } from './../../redux/actionCreators'
import alertify from 'alertifyjs'
import 'alertifyjs/build/css/alertify.css';
import { db } from './../../firebase/firebase'
import { produce } from 'immer'
import { editStrategy, restoreHistory } from './../../redux/actionCreators'
import { Button, Modal, Form, Popup } from 'semantic-ui-react'
import Loader from "react-loader-spinner";
import Select from 'react-select'
import Expand from 'react-expand-animated';

let strategies = [
    {
        name: "Estrategia 1",
        date: "23 de julio de 2020",
        size: "365 Kb",
        link: "https://res.cloudinary.com/leonemil/image/upload/v1603566280/Handball%20manager/strategy_d934du.png"
    },
    {
        name: "Estrategia 2",
        date: "23 de julio de 2020",
        size: "365 Kb",
        link: "https://res.cloudinary.com/leonemil/image/upload/v1603566280/Handball%20manager/strategy_d934du.png"
    },
    {
        name: "Estrategia 3",
        date: "23 de julio de 2020",
        size: "365 Kb",
        link: "https://res.cloudinary.com/leonemil/image/upload/v1603566280/Handball%20manager/strategy_d934du.png"
    },
    {
        name: "Estrategia 4",
        date: "23 de julio de 2020",
        size: "365 Kb",
        link: "https://res.cloudinary.com/leonemil/image/upload/v1603566280/Handball%20manager/strategy_d934du.png"
    },
]

const StrategyPanel = ({panels, togglePanel, currentUser, menuRef, configData, editStrategy, id, history, historyIndex, restoreHistory}) => {

    const panelSize = window.innerWidth < 576 ? '-300px' : '-500px'
    const [loading, setLoading] = useState(false)
    const [currentKeyFolder, setCurrentKeyFolder] = useState(0)
    const [currentKeyItem, setCurrentKeyItem] = useState(0)
    const [openStrategyForm, setOpenStrategyForm] = useState(false)
    const [repeatedFileNameError, setRepeatedFileNameError] = useState(false)
    const [repeatedFolderNameError, setRepeatedFolderNameError] = useState(false)
    const [newFolderNameError, setNewFolderNameError] = useState(false)
    const [editFolderNameError, setEditFolderNameError] = useState(false)
    
    // Cosas que necesito para el guardado y edición de estrategias y carpetas
    const [editStrategyKey, setEditStrategyKey] = useState(0)                   // Indice de la estrategia a editar
    const [editStrategyFolderKey, setEditStrategyFolderKey] = useState(0)       // Indice de la carpeta de la estrategia a editar
    const [originalStrategyFolderKey, setOriginalStrategyFolderKey] = useState()//Indice original de la carpeta a editar
    const [editStrategyName, setEditStrategyName] = useState("")                // Nombre de la estrategia a editar
    const [originalStrategyName, setOriginalStrategyName] = useState()          // Nombre original de la estrategia a editar
    const [expandNewFolderOption, setExpandNewFolderOption] = useState(false)   // Variable que indica si va a crearse una nueva carpeta
    const [currentEditStrategy, setCurrentEditStrategy] = useState({})          // Objeto estrategia para editar y visualizar en cancha

    const [displaySaveStrategyButton, setDisplayStrategyButton] = useState('none')// Display del botón de guardar estrategia
    const [editAndShowConfig, setEditAndShowConfig] = useState(false)
    const [keyStrategyFolder, setKeyStrategyFolder] = useState()

    const [editStrategyFolderModal, setEditStrategyFolderModal] = useState(false)

    const [currentSelectedFolder, setCurrentSelectedFolder] = useState()

    const [selectedStrategy, setSelectedStrategy] = useState()

    const editStrategyModalRef = useRef()

    useEffect(() => {
        if(editStrategyFolderModal === false){
            setEditFolderNameError(false)
        }

        if(openStrategyForm === false){
            setRepeatedFileNameError(false)
            setNewFolderNameError(false)
        }
    })

    const getFolderNames = () => {
        let folderNames = []
        let folderItem = {label: '', value: ''}
        currentUser.strategies.forEach((folder) => {
            folderNames.push({label: folder.name, value: folder.name})
        })
        return folderNames
    }

    const folderNames = getFolderNames()

    const [configPanel, setConfigPanel] = useState({
        display: 'none',
        name: '',
        folder: ''
    })

    function sizeOfObject( object ) {

        var objectList = [];
        var stack = [ object ];
        var bytes = 0;
    
        while ( stack.length ) {
            var value = stack.pop();
    
            if ( typeof value === 'boolean' ) {
                bytes += 4;
            }
            else if ( typeof value === 'string' ) {
                bytes += value.length * 2;
            }
            else if ( typeof value === 'number' ) {
                bytes += 8;
            }
            else if
            (
                typeof value === 'object'
                && objectList.indexOf( value ) === -1
            )
            {
                objectList.push( value );
    
                for( var i in value ) {
                    stack.push( value[ i ] );
                }
            }
        }
        return bytes;
    }
    
    const editStrategyFormation = () => {
        togglePanel(0)
        setDisplayStrategyButton('block')
        //editStrategy(currentEditStrategy)
        alertify.alert("Edite la estrategia y presione guardar estrategia al finalizar")
        setConfigPanel({...configPanel, display: 'none'})
    }

    const editStrategyModal = (strategyName) => {
        setEditStrategyName(strategyName)
    }

    const handleEdit = (e) => {
        e.preventDefault()
        e.persist()
        const user = currentUser

        if(expandNewFolderOption){
            user.strategies[editStrategyFolderKey].list.splice(editStrategyKey, 1)
            let newStrategies = produce(user.strategies, strategyUpdated => {
                strategyUpdated.push({
                    name: e.target.newFolder.value,
                    list: []
                })
    
                strategyUpdated[strategyUpdated.length - 1].list.push({
                    name: e.target.name.value,
                    strategy: currentEditStrategy
                })
            })
            user.strategies = newStrategies

            setEditStrategyFolderKey(user.strategies.length - 1)
            setEditStrategyKey(user.strategies[user.strategies.length - 1].list.length - 1)
        }
        else {
            user.strategies[editStrategyFolderKey].list.splice(editStrategyKey, 1)
            let newFolderIndex = getFolderNames().map(folder => folder.value).indexOf(e.target.folder.value)
            user.strategies[newFolderIndex].list.push({
                name: e.target.name.value,
                strategy: currentEditStrategy
            })
            setEditStrategyFolderKey(newFolderIndex)
            setEditStrategyKey(user.strategies[newFolderIndex].list.length - 1)
        }
        
        setOpenStrategyForm(false)
        if(editAndShowConfig){
            togglePanel(0)
        }
        db.collection('users').doc(id).update(user).then(() => {
            if(editAndShowConfig){
                setEditAndShowConfig(false)
            }
            else {
                alertify.notify("Estratetegia actualizada", "success", 5)
            }
        })

        setConfigPanel({...configPanel, display: 'none'})
    }

    const handleEditFolder = (e) => {
        const user = currentUser
        user.strategies[editStrategyFolderKey].name = e.target.name.value
        db.collection('users').doc(id).update(user).then(() => {
            setEditStrategyFolderModal(false)
            alertify.notify("Carpeta actualizada")
        })
    }
    
    const handleDelete = (keyFolder, keyItem) => {
        let confirm = window.confirm("¿Está seguro de borrar esta estrategia?")
        if(confirm){
            db.collection('users').where('email', '==', currentUser.email).get().then(response => {
                const id = response.docs[0].id
                const user = response.docs[0].data()
                user.strategies[keyFolder].list.splice(keyItem, 1)
                console.log(user, keyFolder, keyItem)
                db.collection('users').doc(id).update(user).then(response => console.log(response))
            })
        }
    }

    const handleDeleteFolder = (key) => {
        alertify.confirm("¿Está seguro que desea borrar esta carpeta?", () => {
            const user = currentUser
            user.strategies.splice(key, 1)
            db.collection('users').doc(id).update(user).then(() => {
                alertify.notify("Carpeta borrada")
            })
        })
    }

    // Función que se llama al hacer click en el botón guardar estrategia
    const updateStrategy = () => {
        setLoading(true)
        const user = currentUser 
        user.strategies[editStrategyFolderKey].list[editStrategyKey].strategy = history[historyIndex]
        setDisplayStrategyButton('none')
        db.collection('users').doc(id).update(user).then(() => {
            alertify.notify("Estrategia actualizada", "success", 5)
            setLoading(false)
        })
    }
    
    const openStrategy = (strategy) => {
        let newHistory = history.slice()
        newHistory.push(strategy)
        restoreHistory(newHistory, newHistory.length - 1)
    }
    
    const handleFolderChange = async (value) => {
        await setEditStrategyFolderKey(currentUser.strategies.map(strategy => strategy.name).indexOf(value.value))
        // if(currentSelectedFolder === undefined){ 
            //     setCurrentSelectedFolder(editStrategyFolderKey)
            //     setRepeatedFileNameError(false)
            //     return
        // }
        if(editStrategyFolderKey === originalStrategyFolderKey && editStrategyName === originalStrategyName){
            setRepeatedFileNameError(false)
            return
        }
        if(currentUser.strategies[currentUser.strategies.map(folder => folder.name).indexOf(value.value)].list.map(strategy => strategy.name).includes(editStrategyName)){
            setRepeatedFileNameError(true)
        }
        else {
            setRepeatedFileNameError(false)
        }
    }
    
    return (
        <>  
        <Button style={{display: displaySaveStrategyButton, position: 'absolute', top: '10px', right: '10px', zIndex: '100000'}} onClick={() => { updateStrategy() }}>Guardar estrategia</Button>
        <Modal
            style={{zIndex: 10000}}
            centered={false}
            open={openStrategyForm}
            onClose={() => setOpenStrategyForm(false)}
            onOpen={() => setOpenStrategyForm(true)}
            ref={editStrategyModalRef}
        >
            <Modal.Header>Editar estrategia</Modal.Header>
            <Modal.Content>
            <Form onSubmit={handleEdit}>
                <Form.Field required>
                    <label htmlFor="name">Nombre de la estrategia</label>
                    <input 
                        name="name" 
                        value={editStrategyName} 
                        onChange={async (e) => { 
                            setEditStrategyName(e.target.value)
                            // if(currentSelectedFolder === undefined){ 
                            //     await setCurrentSelectedFolder(editStrategyFolderKey)
                            //     setRepeatedFileNameError(false)
                            // }
                            if(editStrategyFolderKey === originalStrategyFolderKey && originalStrategyName === e.target.value.trim()){
                                setRepeatedFileNameError(false)
                                return
                            }
                            else {
                                if(currentUser.strategies[editStrategyFolderKey].list.map(strategy => strategy.name).includes(e.target.value.trim())){
                                    setRepeatedFileNameError(true)
                                }
                                else {
                                    setRepeatedFileNameError(false)
                                }
                            }
                        }}
                        required 
                    />
                </Form.Field>
                {
                    repeatedFileNameError ?
                        <p className="login__alert">Ya existe una estrategia con el mismo nombre en la carpeta seleccionada, por favor elija otro nombre</p>
                    : null
                }
                {
                    !expandNewFolderOption ? 
                        <Form.Field>
                            <label htmlFor="folder">Nombre de la carpeta</label>
                            <Select name="folder" defaultValue={folderNames[editStrategyFolderKey]} options={folderNames} required onChange={handleFolderChange}/>
                        </Form.Field>
                    :
                        null
                }
                <Button basic color="grey" role="input" type="button" value="Crear nueva carpeta" className="create-strategy__new-folder-button" onClick={() => {            
                    setExpandNewFolderOption(expandNewFolderOption ? false : true)
                    if(expandNewFolderOption ? false : true){
                        setRepeatedFileNameError(false)
                        setNewFolderNameError(false)
                    }
                    else {
                        if(editStrategyFolderKey === originalStrategyFolderKey && originalStrategyName === editStrategyName) {
                            setRepeatedFileNameError(false)
                            return
                        }
                        else {
                            if(currentUser.strategies[editStrategyFolderKey].list.map(strategy => strategy.name).includes(editStrategyName)){
                                setRepeatedFileNameError(true)
                            }
                            else {
                                setRepeatedFileNameError(false)
                            }
                        }
                    }
                }}>{expandNewFolderOption ? "Elegir carpeta creada" : "Crear nueva carpeta"}</Button>
                {
                    expandNewFolderOption ?
                    <Expand open={expandNewFolderOption}>
                        <Form.Field>
                            <label htmlFor="newFolder">Nombre de la nueva carpeta</label>
                            <input 
                                name="newFolder"
                                className="create-strategy__new-folder-input"
                                required
                                onChange={(e) => {
                                    if(folderNames.map(folder => folder.value).includes(e.target.value.trim())){
                                        setNewFolderNameError(true)
                                    }
                                    else {
                                        setNewFolderNameError(false)
                                    }
                                }}
                            />
                            {
                                newFolderNameError ? 
                                    <span className="login__alert">El nombre de la carpeta ya existe, por favor elija otro nombre</span>
                                :
                                null
                            }
                        </Form.Field>
                    </Expand>
                    : null
                }
                <Button  disabled={repeatedFileNameError || newFolderNameError} type='submit' style={{marginBottom: '1em', marginTop: '3em'}}>Guardar y cerrar</Button>
            </Form>
            </Modal.Content>
            <Modal.Actions>
                <Button onClick={() => setOpenStrategyForm(false)}>Cancelar</Button>
            </Modal.Actions>
        </Modal>

        <Modal
            style={{zIndex: 10000}}
            centered={false}
            open={editStrategyFolderModal}
            onClose={() => setEditStrategyFolderModal(false)}
            onOpen={() => setEditStrategyFolderModal(true)}
        >
            <Modal.Header>Editar carpeta</Modal.Header>
            <Modal.Content>
            <Form onSubmit={handleEditFolder}>
                <Form.Field required>
                    <label htmlFor="name">Nombre de la carpeta</label>
                    <input name="name" required value={editStrategyName} onChange={(e) => { 
                        setEditStrategyName(e.target.value)
                        if(folderNames.map((folder, id) => {if(id !== editStrategyFolderKey) return folder.value}).includes(e.target.value.trim())){
                            setEditFolderNameError(true)
                        }
                        else {
                            setEditFolderNameError(false)
                        }
                     }}/>
                </Form.Field>
                {
                    editFolderNameError ?
                        <p className="login__alert">Ya existe una carpeta con el mismo nombre, por favor elija otro nombre</p>
                    : null
                }
                <Button disabled={editFolderNameError} type='submit'>Guardar</Button>
            </Form>
            </Modal.Content>
            <Modal.Actions>
                <Button onClick={() => {setEditStrategyFolderModal(false)}}>Cancelar</Button>
            </Modal.Actions>
        </Modal>
        
        <div className="strategy-panel" style={panels[0].showPanel ? { left: `${menuRef.current.offsetWidth}px`, zIndex: 10 } : { left: panelSize }}>
            <div className="draw-panel__exit-container" onClick={() => { togglePanel(0) }}>
                <FontAwesomeIcon icon={faTimes} color="#075f60" size="2x" className="draw-panel__exit-item" />
            </div>
                {
                    currentUser.strategies.map((folder, keyFolder) => {
                        return (
                            <React.Fragment key={keyFolder}>
                                <div className="strategy-panel__folder">
                                    <span className="strategy-panel__folder-name" onClick={() => { setKeyStrategyFolder(keyStrategyFolder === keyFolder ? undefined : keyFolder) }}>{folder.name}</span>
                                    <button onClick={() => { setEditStrategyFolderModal(true); setEditStrategyFolderKey(keyFolder); setEditStrategyName(folder.name) }} className="strategy-panel__folder-edit">Editar carpeta</button>
                                    <button onClick={() => { handleDeleteFolder(keyFolder) }} className="strategy-panel__folder-delete">Borrar</button>
                                </div>
                                <Expand className="strategy-panel__folder-expand" open={keyStrategyFolder === keyFolder ? true : false}>
                                {
                                    folder.list.map((item, keyItem) => (
                                        <div className={selectedStrategy === keyItem ? "strategy-panel__item strategy-panel__item--selected" : "strategy-panel__item"} onClick={(e) => { setSelectedStrategy(keyItem) }} key={keyItem}>
                                            <a href={item.url} onClick={() => { openStrategy(item.strategy) }} target="_blank" rel="noopener noreferrer" className="strategy-panel__item-link">
                                                <span className={`draw-panel__item-span-${keyItem + 1}`}>{item.name}</span>
                                                <span className={`draw-panel__item-span-${keyItem + 1} text-gray`}>{item.date}</span>
                                                <span className={`draw-panel__item-span-${keyItem + 1} text-gray`}>{sizeOfObject(item)} bytes</span>
                                            </a>
                                            <Popup content='Renombrar' trigger={<div className="strategy-panel__item-button--rename" onClick={() => { editStrategyModal(item.name); setOpenStrategyForm(true); setEditStrategyKey(keyItem); setEditStrategyFolderKey(keyFolder); setOriginalStrategyFolderKey(keyFolder); setCurrentEditStrategy(item.strategy); setOriginalStrategyName(item.name)}}></div>} />
                                            <Popup content='Editar en cancha' trigger={<div className="strategy-panel__item-button--edit" onClick={() => { editStrategyModal(item.name); setEditStrategyKey(keyItem); setEditStrategyFolderKey(keyFolder); setOriginalStrategyFolderKey(keyFolder); setCurrentEditStrategy(item.strategy); setOriginalStrategyName(item.name); editStrategyFormation()} }></div>} />
                                            <Popup content='Eliminar' trigger={<div className="strategy-panel__item-button--delete" onClick={() => { handleDelete(keyFolder, keyItem) }}></div>} />
                                        </div>
                                    ))
                                }
                                </Expand>
                            </React.Fragment>
                        )
                    })
                }
            {
                configData.bannerUrl !== "" ?
                    <img src={configData.bannerUrl} alt={configData.BannerAlt} className="banner-panel"/>
                : null
            }
        </div>
            {
                loading ? 
                <Loader
                    type="TailSpin"
                    color="#00BFFF"
                    height={100}
                    width={100}
                    timeout={900000000} 
                    className="config-panel__loader-spinner"
                />
                :
                null
            }
        </>
    );
}

const mapStateToProps = state => {
    return {
        currentUser: state.currentUser,
        panels: state.panels,
        configData: state.configData,
        user: state.currentUser,
        id: state.currentUser.id,
        history: state.history,
        historyIndex: state.historyIndex,
        state: state
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        togglePanel: (panel) => { dispatch(togglePanel(panel)) },
        editStrategy: (strategy) => { dispatch(editStrategy(strategy)) },
        restoreHistory: (history, historyIndex) => { dispatch(restoreHistory(history, historyIndex)) }
    }
}
 
export default connect(mapStateToProps, mapDispatchToProps)(StrategyPanel);