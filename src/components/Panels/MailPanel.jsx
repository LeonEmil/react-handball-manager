import React, { useRef } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimes } from '@fortawesome/free-solid-svg-icons'
import { connect } from 'react-redux'
import { togglePanel } from '../../redux/actionCreators';
import emailjs from 'emailjs-com'
import { init } from 'emailjs-com'

const MailPanel = ({panels, togglePanel }) => {

    const form = useRef();

    const sendEmail = (e) => {
        e.preventDefault();

        init("user_bX4iywCvy6anKCSGfeqnL")
        emailjs.send("service_5ofv86m","template_obif4gn", {
            name: e.target.name.value,
            email: e.target.email.value,
            subject: e.target.subject.value,
            message: e.target.message.value,
        })
        .then(() => {
            alert("Mensaje enviado")
        })
        .catch(error => {
            alert(error.text)
        })
    };

    return (  
        <div className="mail-panel" style={panels[5].showPanel ? { transform: "scaleX(1)", zIndex: 10 } : { transform: "scaleX(0)" }}>
            <div className="draw-panel__exit-container" onClick={() => {togglePanel(5)}}>
                <FontAwesomeIcon icon={faTimes} color="#075f60" size="2x" className="draw-panel__exit-item" />
            </div>
            <form ref={form} onSubmit={sendEmail} className="mail__form">
                <label htmlFor="name" className="mail__label">Nombre</label>
                <input 
                    type="text" 
                    className="mail__input" 
                    name="name"
                    required
                />

                <label htmlFor="email" className="mail__label">E-mail</label>
                <input 
                    type="email" 
                    className="mail__input" 
                    name="email"
                    required
                />

                <label htmlFor="subject" className="mail__label">Asunto</label>
                <input 
                    type="text" 
                    className="mail__input" 
                    name="subject"
                    required
                />

                <label htmlFor="message" className="mail__label">Mensaje</label>
                <textarea 
                    type="text" 
                    className="mail__input mail__input-textarea" 
                    name="message"
                    required
                />

                <input type="submit" value="Enviar" className="mail__button"/>
            </form>
        </div>
    );
}

const mapStateToProps = state => {
    return {
        currentUser: state.currentUser,
        panels: state.panels
    }
}

const mapDispatchToProps = dispatch => {
    return {
        togglePanel: (panel) => {dispatch(togglePanel(panel))}
    }
}
 
export default connect(mapStateToProps, mapDispatchToProps)(MailPanel);

// import React, { useRef } from 'react';
// import emailjs from 'emailjs-com';

// export const ContactUs = () => {
//   const form = useRef();

//   const sendEmail = (e) => {
//     e.preventDefault();

//     emailjs.sendForm('YOUR_SERVICE_ID', 'YOUR_TEMPLATE_ID', form.current, 'YOUR_USER_ID')
//       .then((result) => {
//           console.log(result.text);
//       }, (error) => {
//           console.log(error.text);
//       });
//   };

//   return (
//     <form ref={form} onSubmit={sendEmail}>
//       <label>Name</label>
//       <input type="text" name="user_name" />
//       <label>Email</label>
//       <input type="email" name="user_email" />
//       <label>Message</label>
//       <textarea name="message" />
//       <input type="submit" value="Send" />
//     </form>
//   );
// };