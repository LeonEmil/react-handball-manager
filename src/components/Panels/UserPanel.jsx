import React, { useState } from 'react';
import { Link } from 'react-router-dom'
import { useFormik } from 'formik'
import * as Yup from 'yup'
//import FormLogin from './../FormLogin'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimes } from '@fortawesome/free-solid-svg-icons'
import { connect } from 'react-redux'
import { storage, db, auth } from './../../firebase/firebase'
import { togglePanel } from './../../redux/actionCreators'
import { Form, Button } from 'semantic-ui-react'

const UserPanel = ({ panels, togglePanel, currentUser }) => {

    const [displayModal, setDisplayModal] = useState(false)

    const { handleSubmit, handleChange, handleBlur, values, touched, errors } = useFormik({
        initialValues: {
            email: '',
            password: '',
            confirmPassword: ''
        },
        validationSchema: Yup.object({
            email: Yup.string().email(),
            password: Yup.string().min(6, 'La contraseña debe tener como mínimo 6 caracteres'),
            confirmPassword: Yup.string().oneOf([Yup.ref('password'), null], 'Las contraseñas deben coincidir')
        }),
        onSubmit: ({ email, password }) => {
            let user = auth.currentUser
            if (email !== '') {
                user.updateEmail(email).then(() => {
                    db.collection('users').where('email', '==', currentUser.email).get().then(response => {
                        const id = response.docs[0].id
                        db.collection('users').doc(id).update({
                            email: auth.currentUser.email
                        })
                    })
                })
                    .then(() => {
                        alert("Email actualizado")
                    })
                    .catch((error) => {
                        if (error.message === "This operation is sensitive and requires recent authentication. Log in again before retrying this request.") {
                            alert("Esta operación es sensible y requiere autenticación reciente. Ingrese nuevamente e intentelo de nuevo")
                        }
                    })
            }
            if (password !== '') {
                user.updatePassword(password)
                    .then(() => {
                        alert("Contraseña actualizada")
                    })
                    .catch((error) => {
                        alert(error)
                    })
            }
        }
    })

    const handleSubmitPhoto = async (e) => {
        e.preventDefault()
        console.log(e.target)
        const file = e.target.avatar.files[0]
        const fileRef = storage.ref().child(file.name)
        await fileRef.put(file)
        db.collection('users').where('email', '==', currentUser.email).get().then(async response => {
            const id = response.docs[0].id
            const doc = response.docs[0].data()
            doc.avatar = await fileRef.getDownloadURL()
            db.collection('users').doc(id).update(doc)
        })
    }

    const handleDelete = () => {
        db.collection('users').where('email', '==', currentUser.email).get().then(async response => {
            const id = response.docs[0].id
            const doc = response.docs[0].data()
            doc.avatar = 'https://res.cloudinary.com/leonemil/image/upload/v1602549579/Handball%20manager/user-image_mvzyz8.jpg'
            db.collection('users').doc(id).update(doc)
        })
    }

    const handleOtherData = (e) => {
        e.persist()
        e.preventDefault()
        console.log(e)
        db.collection('users').where('email', '==', currentUser.email).get().then(async response => {
            const id = response.docs[0].id
            const doc = response.docs[0].data()
            const data = {
                key: e.target.key.value,
                value: e.target.value.value
            }
            doc.otherData.push(data)
            db.collection('users').doc(id).update(doc)
        })
    }

    return (
        <div className="user-panel" style={panels[7].showPanel ? { transform: "scaleX(1)", zIndex: 10 } : { transform: "scaleX(0)" }}>
            <div className="draw-panel__exit-container" onClick={() => { togglePanel(7) }}>
                <FontAwesomeIcon icon={faTimes} color="#075f60" size="2x" className="draw-panel__exit-item" />
            </div>
            <div className="user-panel__container">
                <h2 className="user-panel__subtitle">Perfil de usuario</h2>
                <button className="user-panel__button" onClick={() => { setDisplayModal(true) }}>Editar</button>
            </div>
            <div className="user-panel__image-container">
                <div className="user-panel__image-container">
                    {
                        currentUser ?
                            <img src={currentUser.avatar} alt="User" className="user-panel__image" />
                            : null
                    }
                </div>
                <form onSubmit={handleSubmitPhoto}>
                    <label htmlFor="avatar"></label>
                    <input type="file" className="user-panel__button" name="avatar" id="avatar" />
                    <input type="submit" className="user-panel__button" value="Guardar cambios" />
                </form>
                <button className="user-panel__button" onClick={handleDelete}>Borrar foto</button>
            </div>
            <div className="user-panel__user-data">
                <h3 className="user-panel__subtitle">Nombre de usuario</h3>
                <span className="user-panel__span">{currentUser ? currentUser.name : ""}</span>
                <h3 className="user-panel__subtitle">Correo electrónico</h3>
                <span className="user-panel__span">{currentUser ? currentUser.email : ""}</span>
            </div>
            {
                currentUser.role === "admin" ?
                    <Link to="/admin">
                        <button className="user-panel__button">Ir al panel de administración</button>
                    </Link>
                    : null
            }
            <button className="user-panel__button" onClick={() => { auth.signOut() }}>Cerrar sesión</button>
            <ul className="user-panel__other-data">
                {
                    currentUser.otherData.map((data, key) => {
                        return (
                            <li className="user-panel__other-data-item" key={key}>{`${data.key}: ${data.value}`}</li>
                        )
                    })
                }
            </ul>
            <h3 className="user-panel__subtitle">Agregar otros datos</h3>
            <Form onSubmit={handleOtherData}>
                <Form.Input style={{display: 'flex', flexDirection: 'column'}}>
                    <label htmlFor="key">Tipo de dato:</label>
                    <input type="text" name="key" id="key" />
                </Form.Input>
                <Form.Input style={{display: 'flex', flexDirection: 'column'}}>
                    <label htmlFor="value">Valor:</label>
                    <input type="text" name="value" id="value" />
                </Form.Input>
                <Button>Guardar dato</Button>
            </Form>

            {/* // Formulario de configuración  */}
            <div className="admin__modal" style={displayModal ? { transform: 'scaleY(1)', display: 'block' } : { transform: 'scaleY(0)', display: 'none' }}>
                <div className="admin__exit-container" onClick={() => { setDisplayModal(false) }}>
                    <FontAwesomeIcon icon={faTimes} color="#fff" size="2x" className="draw-panel__exit-item" />
                </div>
                <form onSubmit={handleSubmit} className="admin__form">
                    <label htmlFor="email">Nuevo email de usuario</label>
                    <input
                        type="email"
                        name="email"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.email}
                        className="admin__form-input"
                    />
                    {
                        errors.email && touched.email ?
                            <span className="login__alert">{errors.email}</span>
                            : null
                    }
                    <label htmlFor="password">Nueva contraseña</label>
                    <input
                        type="password"
                        name="password"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.password}
                        className="admin__form-input"
                    />
                    {
                        errors.password && touched.password ?
                            <span className="login__alert">{errors.password}</span>
                            : null
                    }
                    <input type="submit" value="Actualizar" />
                </form>
            </div>
        </div>
    )
}

const mapStateToProps = state => {
    return {
        currentUser: state.currentUser,
        panels: state.panels
    }
}

const mapDispatchToProps = dispatch => {
    return {
        togglePanel: (panel) => { dispatch(togglePanel(panel)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(UserPanel);
