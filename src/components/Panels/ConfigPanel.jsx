import React, { useState, useEffect } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimes } from '@fortawesome/free-solid-svg-icons'
import { connect } from 'react-redux'
import { db } from './../../firebase/firebase'
import { configButton1Active, configButton1Inactive, configButton2Active, configButton2Inactive } from './../../js/images'
import alertify from 'alertifyjs'
import Select, { components } from 'react-select'
import Loader from "react-loader-spinner";
import produce from "immer"
import Expand from 'react-expand-animated';
import { Modal, Form, Button, Accordion, Icon } from 'semantic-ui-react'

import { changeBackground, togglePanel, changeObjectSize, addCustomTeam, addSelectedCustomTeam, addMyTeam, updateEditedTeam, deleteCustomTeam } from './../../redux/actionCreators'

let backgroundFormat = [
    {
        event: "/v1602549201/Handball%20manager/canvas-1_rbigwq.jpg"
    },
    {
        event: "/v1605608953/Handball%20manager/cancha4_anaat0.png"
    },
    {
        event: "/v1605608956/Handball%20manager/cancha5_vofblr.png"
    },
    {
        event: "/v1605608955/Handball%20manager/cancha6_lwyjfv.png"
    },
    {
        event: '/v1634421158/Handball%20manager/EBG20_06_Graficos_App_Pizarra_v4-19_btddhz.jpg',
    },
    {
        event: "/v1602548589/Handball%20manager/canvas-2_ao97dx.jpg",
    },
    {
        event: "/v1602548592/Handball%20manager/canvas-3_uowaon.jpg",
    },
    {
        event: "/v1634421378/Handball%20manager/EBG20_06_Graficos_App_Pizarra_v4-20_avwfqa.jpg"
    },
    {
        event: '/v1633747718/Handball%20manager/blue-background-part_f0uz1a.jpg',
    },
    {
        event: '/v1633747718/Handball%20manager/green-background-part_bw6y5q.jpg',
    },
]

const ConfigPanel = ({panels, togglePanel, changeBackground, currentUser, changeObjectSize, addCustomTeam, updateEditedTeam, deleteCustomTeam, history, historyIndex, id}) => {
   
    // Create team variables
    const [loading, setLoading] = useState(false)
    const [openCreateTeam, setOpenCreateTeam] = useState(false)
    

    const [players, setPlayers] = useState({
        // "0": {
        //     name: "",
        //     number: 0,
        //     role: "Portero",
        //     initial: true,
        //     image: "",
        //     position: {
        //         x: 0,
        //         y: 0
        //     }
        // },
    })

    const addPlayer = async () => {
        setLoading(true)
        let index = Object.keys(players).length
        let lastPlayer = {
            [index]: {
                name: "",
                number: 0,
                role: "Portero",
                initial: false,
                image: "Círculo",
                position: {
                    x: 0,
                    y: 0
                }
            }
        }
        let newPlayers = Object.assign(players, lastPlayer)
        await setPlayers(newPlayers)
        setLoading(false)
        setOpenAddPlayerModal(false)
    }
    
    const createTeam = (e) => {
        e.preventDefault()
        e.persist()
        setLoading(true)
        let user = currentUser
        for(let i = 0; i < Object.values(players).length; i++){
            console.log(e.target)
            players[i].name = e.target[`name${i + 1}`].value
            players[i].number = e.target[`number${i + 1}`].value
            players[i].role = e.target[`role${i + 1}`].value
            players[i].initial = e.target[`initial${i + 1}`].checked
            players[i].image = e.target[`image${i + 1}`].value
            players[i].position.x = 0
            players[i].position.y = 0
        }
        let index = Object.values(user.customTeams).length 
        user.customTeams = Object.assign(user.customTeams, {[index]: {
            teamName: e.target.teamName.value,
            color: e.target.color.value,
            players
        }})
        db.collection('users').doc(id).update(user).then(() => {
            setLoading(false)
            alertify.success("Equipo creado")
            setOpenCreateTeam(false)
        })
    }

    const handleDeletePlayer = (playerId) => {
        setLoading(true)
        let newPlayers = Object.assign({}, players)

        delete newPlayers[playerId]
        newPlayers = Object.values(newPlayers)
        newPlayers = Object.assign({}, newPlayers)

        setPlayers(newPlayers)
        setLoading(false)
    }

    const handlePositionCreateTeam = (position, key) => {
        let newPlayers = players
        newPlayers[key].role = position.label
        setPlayers(newPlayers)
        if(Object.values(players).map((player, id) => {if(id !== key && player.initial) return player.role}).includes(position.label) && players[key].initial) {
            newPlayers[key].repeatedPositionError = true
        }
        else {
            newPlayers[key].repeatedPositionError = false
        }
        setPlayers(newPlayers)
    }

    const positions = [
        {label: 'Portero', value: 'Portero'},
        {label: 'Central', value: 'Central'},
        {label: 'Pivote', value: 'Pivote'},
        {label: 'Extremo izquierdo', value: 'Extremo izquierdo'},
        {label: 'Extremo derecho', value: 'Extremo derecho'},
        {label: 'Lateral izquierdo', value: 'Lateral izquierdo'},
        {label: 'Lateral derecho', value: 'Lateral derecho'},
    ]

    // Config panel variables
    const [panel, setPanel] = useState("General")
    const [teams, setTeams] = useState([])
    const [myTeam, setMyTeam] = useState()
    const [MyTeamOptions, setMyTeamOptions] = useState([]) 
    const [expandPlayer, setExpandPlayer] = useState()
    const [openAddPlayerModal, setOpenAddPlayerModal] = useState(false)
    const [repeatedNumberError, setRepeatedNumberError] = useState(false)
    const [repeatedPositionError, setRepeatedPositionError] = useState(false)
    const [updateColor, setUpdateColor] = useState()
    const [error, setError] = useState({
        repeatedNumberInEditForm: {status: false, msg: 'Ya existe un jugador con el número seleccionado, por favor elija otro', color: 'red', ids: []},
        repeatedNumberInAddPlayerForm: {status: false, msg: 'Ya existe un jugador con el número seleccionado, por favor elija otro', color: 'red', ids: []},
        repeatedNumberInCreateTeamForm: {status: false, msg: 'Ya existe un jugador con el número seleccionado, por favor elija otro', color: 'red', ids: []},
        repeatedPositionInEditForm: {status: false, msg: 'Ya existe un jugador con la posición seleccionada, por favor elija otra', color: 'red', ids: []},
        repeatedPositionInAddPlayerForm: {status: false, msg: 'Ya existe un jugador con la posición seleccionada, por favor elija otra', color: 'red', ids: []},
        repeatedPositionInCreateTeamForm: {status: false, msg: 'Ya existe un jugador con la posición seleccionada, por favor elija otra', color: 'red', ids: []},
    })
    const [addPlayerTeam, setAddPlayerTeam] = useState({
        name: "",
        number: "",
        role: "",
        initial: false,
        image: "",
        position: {
            x: 0,
            y: 0
        }
    })  

    const handleSubmit = (e) => {
        e.preventDefault()
        e.persist()
        setLoading(true)
        let user = currentUser
        let teamId
        for(let i = 0; i < Object.values(user.customTeams).length; i++){
            if(user.customTeams[i].teamName === myTeam.teamName){
                teamId = i
                console.log(user.customTeams, i, teamId, myTeam)
                break;
            } 
        }
        user.customTeams[teamId] = myTeam
        Object.defineProperty(user.customTeams[teamId], 'color', {
            value: e.target.teamColor.value
        })
        console.log(e.target.teamColor.value)
        for(let i = 0; i < Object.values(user.customTeams[teamId].players).length; i++){
            Object.defineProperty(user.customTeams[teamId].players[i], 'name', {
                value: e.target[`name${i}`].value
            })
            Object.defineProperty(user.customTeams[teamId].players[i], 'number', {
                value: e.target[`number${i}`].value
            })
            Object.defineProperty(user.customTeams[teamId].players[i], 'role', {
                value: e.target[`role${i}`].value
            })
            Object.defineProperty(user.customTeams[teamId].players[i], 'initial', {
                value: e.target[`initial${i}`].checked
            })
            Object.defineProperty(user.customTeams[teamId].players[i], 'image', {
                value: e.target[`image${i}`].value
            })
        }
        if(user.customTeams[teamId].teamName === history[historyIndex]["customAttackTeam"].teamName){
            addCustomTeam("customAttackTeam", user.customTeams[teamId])
        }
        if(user.customTeams[teamId].teamName === history[historyIndex]["customDefenseTeam"].teamName){
            addCustomTeam("customDefenseTeam", user.customTeams[teamId])
        }
        db.collection('users').doc(id).update(user).then(() => {
            alertify.notify('Equipo actualizado', 'success', 5, function(){ });
            setLoading(false)
        })   
    }

    const handleMyTeam = (e) => {
        e.preventDefault()
        e.persist()
        setLoading(true)
        const user = currentUser
        const newTeam = Object.values(currentUser.customTeams).filter(el => el.teamName === e.target.team.value)
        user.myTeam = newTeam[0]
        db.collection('users').doc(id).update(user).then(() => {
            setLoading(false)
            alertify.alert("Mi equipo actualizado")
        })
    }

    const addMyTeamPlayer = async (e) => {
        e.preventDefault()
        e.persist()
        setLoading(true)

        let user = currentUser
        let teamId
        let myNewTeam = myTeam
        for(let i = 0; i < Object.values(user.customTeams).length; i++){
            if(user.customTeams[i].teamName === myTeam.teamName){
                console.log(user.customTeams, i, myTeam)
                teamId = i
                break;
            } 
        }

        
        let index = Object.values(myTeam.players).length
        let lastPlayer = {
            [index]: {
                name: e.target.name.value,
                number: e.target.number.value,
                role: e.target.role.value,
                initial: e.target.initial.checked,
                image: e.target.image.value,
                position: {
                    x: 0,
                    y: 0
                }
            }
        }
        let newPlayers = Object.assign(myTeam.players, lastPlayer)
        user.customTeams[teamId].players = newPlayers
        await setMyTeam({...myTeam, players: newPlayers})
        if(user.customTeams[teamId].teamName === history[historyIndex]["customAttackTeam"].teamName){
            addCustomTeam("customAttackTeam", user.customTeams[teamId])
        }
        if(user.customTeams[teamId].teamName === history[historyIndex]["customDefenseTeam"].teamName){
            addCustomTeam("customDefenseTeam", user.customTeams[teamId])
        }
        db.collection('users').doc(id).update(user).then(() => {
            setLoading(false)
            setOpenAddPlayerModal(false)
        })
    }

    const handleDeleteTeam = (teamId) => {
        alertify.confirm("¿Está seguro que desea borrar el equipo?",
            () => { 
                setLoading(true)
                let user = currentUser
                
                delete user.customTeams[teamId]
                user.customTeams = Object.values(user.customTeams)
                user.customTeams = Object.assign({}, user.customTeams)
                db.collection("users").doc(id).update(user).then(() => {
                    setLoading(false)
                    alertify.success('Equipo borrado')
                })
    
                if(user.customTeams[teamId].teamName === myTeam.teamName){
                    setMyTeam(undefined)
                }
                if(user.customTeams[teamId].teamName === history[historyIndex].customAttackTeam.teamName) deleteCustomTeam("customAttackTeam")
                if(user.customTeams[teamId].teamName === history[historyIndex].customDefenseTeam.teamName) deleteCustomTeam("customDefenseTeam")
            }
        );
    }

    const handlePositionEditTeam = (position, key) => {
        let newPlayers = myTeam.players
        newPlayers[key].role = position.label
        setMyTeam({...myTeam, players: newPlayers})
        if(Object.values(myTeam.players).map((player, id) => {if(id !== key && player.initial) return player.role}).includes(position.label) && myTeam.players[key].initial) {
            newPlayers[key].repeatedPositionError = true
        }
        else {
            newPlayers[key].repeatedPositionError = false
        }
        setMyTeam({...myTeam, players: newPlayers})
    }

    const handlePositionAddPlayer = (position) => {
        let newAddPlayerTeam = addPlayerTeam   
        newAddPlayerTeam.role = position.label               
        if(Object.values(myTeam.players).map((player, id) => {if(player.initial) return player.role}).includes(position.label) && addPlayerTeam.initial) {
            newAddPlayerTeam.repeatedPositionError = true
        }
        else {
            newAddPlayerTeam.repeatedPositionError = false
        }
        setAddPlayerTeam(newAddPlayerTeam)
    }

    const handleDeleteMyPlayer = (playerId) => {
        setLoading(true)
        let user = currentUser
        let teamId
        let myNewTeam = myTeam
        for(let i = 0; i < Object.values(user.customTeams).length; i++){
            if(user.customTeams[i].teamName === myTeam.teamName){
                teamId = i
                break;
            } 
        }

        delete user.customTeams[teamId].players[playerId]
        user.customTeams[teamId].players = Object.values(user.customTeams[teamId].players)
        user.customTeams[teamId].players = Object.assign({}, user.customTeams[teamId].players)

        delete myNewTeam.players[playerId]
        myNewTeam.players = Object.values(myNewTeam.players)
        myNewTeam.players = Object.assign({}, myNewTeam.players)
        setMyTeam(myNewTeam)

        if(user.customTeams[teamId].teamName === history[historyIndex]["customAttackTeam"].teamName){
            addCustomTeam("customAttackTeam", user.customTeams[teamId])
        }
        if(user.customTeams[teamId].teamName === history[historyIndex]["customDefenseTeam"].teamName){
            addCustomTeam("customDefenseTeam", user.customTeams[teamId])
        }

        db.collection("users").doc(id).update(user).then(response => {
            alertify.warning("Jugador borrado")
            setLoading(false)
        })
    }

    const handleTeamOptions = (user) => {
        let teamsData = {teams: [], myTeam: {label: user.myTeam ? user.myTeam.teamName : '', value: user.MyTeam ? user.myTeam.teamName : ''}}
        let data = {label: "", value: ""}
        for(let i = 0; i < Object.values(user.customTeams).length; i++){
            data = {label: Object.values(user.customTeams)[i].teamName, value: Object.values(user.customTeams)[i].teamName}
            teamsData.teams.push(data)
        }
        return teamsData
    }

    const handlePlayerImages = (player) => {
        let playerImageList = {currentImage: [{label: player.image, value: player.image}], images: [
            {label: "Círculo", value: "Círculo"},
            {label: 'Cuadrado', value: 'Cuadrado'}, 
            {label: 'Triángulo', value: 'Triángulo'}, 
            {label: 'Jugador (con brazo izquierdo)', value: 'Jugador (con brazo izquierdo)'}, 
            {label: 'Jugador (con brazo derecho)', value: 'Jugador (con brazo derecho)'}, 
            {label: 'Jugador (con pelota en brazo izquierdo)', value: 'Jugador (con pelota en brazo izquierdo)'}, 
            {label: 'Jugador (con pelota en brazo derecho)', value: 'Jugador (con pelota en brazo derecho)'}, 
            {label: "Portero", value: "Portero"}
        ]}
        return playerImageList
    }

    const handlePlayerPositions = (player) => {
        let playerPositionList = {currentPosition: [{label: player.role, value: player.role}], positions: [
            {label: 'Portero', value: 'Portero'},
            {label: 'Central', value: 'Central'},
            {label: 'Pivote', value: 'Pivote'},
            {label: 'Extremo izquierdo', value: 'Extremo izquierdo'},
            {label: 'Extremo derecho', value: 'Extremo derecho'},
            {label: 'Lateral izquierdo', value: 'Lateral izquierdo'},
            {label: 'Lateral derecho', value: 'Lateral derecho'},
        ]}
        return playerPositionList
    }

    return (  
        <>

        {/* ----------------------- Create team modal -------------------- */}
        <Modal
            onClose={() => setOpenCreateTeam(false)}
            onOpen={() => setOpenCreateTeam(true)}
            open={openCreateTeam}
        >
        <Modal.Header><h4>Crear equipo</h4></Modal.Header>
        <Modal.Content>
        <Form onSubmit={createTeam}>
            <div className="form-teams__create-player">
                <Form.Field>
                    <label htmlFor="teamName" className="form-teams__label">Nombre del equipo</label>
                    <input 
                        type="text" 
                        name="teamName" 
                        id="teamName" 
                        required
                    />
                </Form.Field>
                <Form.Field>
                    <label htmlFor="color" className="form-teams__label">Color del equipo</label>
                    <input 
                        type="color" 
                        name="color" 
                        id="color" 
                        required
                    />
                </Form.Field>
            </div>
            {
                Object.values(players).map((player, key) => {
                    return (
                        <React.Fragment key={key}>
                        <div className="form-teams__create-player" key={key}>
                            <input type="button" className="form-teams__delete-player" value="Borrar jugador" onClick={() => {handleDeletePlayer(key)}} />
                            <Form.Field>
                                <label htmlFor={`name${key + 1}`} className="form-teams__label">Nombre del jugador</label>
                                <input
                                    type="text" 
                                    name={`name${key + 1}`} 
                                    className="form-teams__input"
                                    id={`name${key + 1}`} 
                                    required
                                />
                            </Form.Field>
                            <Form.Field>
                                <label htmlFor={`number${key + 1}`} className="form-teams__label">Número del jugador</label>
                                <input
                                    type="number" 
                                    name={`number${key + 1}`} 
                                    className="form-teams__input"
                                    id={`number${key + 1}`} 
                                    required
                                    onChange={(e) => {
                                        let newPlayers = players
                                        newPlayers[key].number = e.target.value
                                        setPlayers(newPlayers)
            
                                        if(Object.values(players).map((player, id) => {if(id !== key) return player.number}).includes(e.target.value)){
                                            Object.values(newPlayers)[key].repeatedNumberError = true
                                            setPlayers(newPlayers)
                                        }
                                        else {
                                            Object.values(newPlayers)[key].repeatedNumberError = false
                                            setPlayers(newPlayers)
                                        }
                                    }}
                                />
                            </Form.Field>
                            {
                                player.repeatedNumberError ?
                                    <p style={{color: 'red'}}>Ya existe un jugador con el número seleccionado, por favor elija otro</p>
                                : null
                            }
                            <Form.Field>
                                <label htmlFor={`role${key + 1}`} className="form-teams__label">Posición del jugador</label>
                                <Select options={positions} defaultValue={{label: "Portero", value: "Portero"}} name={`role${key + 1}`} className="form-teams__select" onChange={(position) => handlePositionCreateTeam(position, key)}/>
                            </Form.Field>
                            <Form.Field>
                                <label htmlFor={`initial${key + 1}`} className="form-teams__label">¿Jugador inicial?</label>
                                <input
                                    type="checkbox"
                                    name={`initial${key + 1}`} 
                                    id={`initial${key + 1}`} 
                                    className="form-teams__checkbox"
                                    onChange={(e) => {
                                        e.persist()
                                        let newPlayers = players
                                        newPlayers[key].initial = e.target.checked
                                        setPlayers(newPlayers)
                                        if(Object.values(players).map((player, id) => {if(id !== key && player.initial) return player.role}).includes(player.role) && e.target.checked) {
                                            newPlayers[key].repeatedPositionError = true
                                        }
                                        else {
                                            newPlayers[key].repeatedPositionError = false
                                        }
                                        setPlayers(newPlayers)
                                    }}
                                />
                            </Form.Field>
                            {
                                player.repeatedPositionError ?
                                    <p style={{color: 'red'}}>Ya existe un jugador inicial con la posición seleccionada, por favor elija otra</p>
                                : null
                            }
                            <Form.Field>
                                <label htmlFor={`image${key + 1}`} className="form-teams__label">Forma</label>
                                <select name={`image${key + 1}`} id="" className="form-teams__select" required>
                                    <option value="Círculo">Círculo</option>
                                    <option value="Cuadrado">Cuadrado</option>
                                    <option value="Triángulo">Triángulo</option>
                                    <option value="Jugador (con brazo derecho)">Jugador (con brazo derecho)</option>
                                    <option value="Jugador (con brazo izquierdo)">Jugador con brazo izquierdo)</option>
                                    <option value="Jugador (con pelota en brazo derecho)">Jugador (con pelota en brazo derecho)</option>
                                    <option value="Jugador (con pelota en brazo izquierdo)">Jugador (con pelota en brazo izquierdo)</option>
                                    <option value="Portero">Portero</option>
                                </select>
                            </Form.Field>
                        </div>
                        </React.Fragment>
                    )
                })
            }
            <input type="button" className="form-teams__add-player" onClick={() => { addPlayer() }} value="Añadir jugador"/>
            <Button type="submit" disabled={Object.values(players).map(player => (player.repeatedNumberError)).includes(true) || Object.values(players).map(player => (player.repeatedPositionError)).includes(true)}>Crear equipo</Button>
            {
                loading ? 
                <Loader
                    type="TailSpin"
                    color="#00BFFF"
                    height={100}
                    width={100}
                    timeout={900000000} 
                    className="config-panel__loader-spinner"
                />
                :
                null
            }
        </Form>
        </Modal.Content>
        <Modal.Actions>
            <Button onClick={() => setOpenCreateTeam(false)}>Cancelar</Button>
        </Modal.Actions>
        </Modal>

        {/* ------------------- Add player modal ---------------- */}
        <Modal
            onClose={() => setOpenAddPlayerModal(false)}
            onOpen={() => setOpenAddPlayerModal(true)}
            open={openAddPlayerModal}
        >
        <Modal.Header>
            <h4>Añadir jugador</h4>
        </Modal.Header>
        <Modal.Content>
        <Form onSubmit={addMyTeamPlayer}>
            <div className="form-teams__create-player">
                <Form.Field>
                    <label htmlFor={`name`} className="form-teams__label">Nombre del jugador</label>
                    <input
                        type="text" 
                        name={`name`} 
                        className="form-teams__input"
                        id={`name`} 
                        required
                    />
                </Form.Field>
                <Form.Field>
                    <label htmlFor={`number`} className="form-teams__label">Número del jugador</label>
                    <input
                        type="number" 
                        name="number"
                        className="form-teams__input"
                        id={`number`} 
                        required
                        onChange={(e) => {   
                            let newAddPlayerTeam = addPlayerTeam                     
                            if(Object.values(myTeam.players).map((player, id) => player.number).includes(e.target.value)) {
                                newAddPlayerTeam.repeatedNumberError = true
                            }
                            else {
                                newAddPlayerTeam.repeatedNumberError = false
                            }
                            setAddPlayerTeam(newAddPlayerTeam)
                        }}
                    />
                </Form.Field>
                {
                    addPlayerTeam.repeatedNumberError ? 
                        <p style={{color: 'red'}}>Ya existe un jugador con el número seleccionado, por favor elija otro.</p>
                    : null
                }
                <Form.Field>
                    <label htmlFor={`role`} className="form-teams__label">Posición del jugador</label>
                    <Select options={positions} defaultValue={{label: "Portero", value: "Portero"}} name={`role`} className="form-teams__select" onChange={(position) => handlePositionAddPlayer(position)} />
                </Form.Field>
                <Form.Field>
                    <label htmlFor={`initial`} className="form-teams__label">¿Jugador inicial?</label>
                    {
                        addPlayerTeam.initial ?
                            <input
                                type="checkbox"
                                name={`initial`} 
                                id={`initial`} 
                                className="form-teams__checkbox"
                                defaultChecked
                                onChange={(e) => {
                                    e.persist()
                                    let newAddPlayerTeam = addPlayerTeam
                                    newAddPlayerTeam.initial = e.target.checked
                                    setAddPlayerTeam(newAddPlayerTeam)                     
                                    if(Object.values(myTeam.players).map((player, id) => {if(player.initial) return player.role}).includes(addPlayerTeam.role) &&  addPlayerTeam.initial) {
                                        newAddPlayerTeam.repeatedPositionError = true
                                    }
                                    else {
                                        newAddPlayerTeam.repeatedPositionError = false
                                    }
                                    setAddPlayerTeam(newAddPlayerTeam)
                                }}
                            />
                        :
                        <input
                            type="checkbox"
                            name={`initial`} 
                            id={`initial`} 
                            className="form-teams__checkbox"
                            onChange={(e) => {
                                e.persist()
                                let newAddPlayerTeam = addPlayerTeam
                                newAddPlayerTeam.initial = e.target.checked
                                setAddPlayerTeam(newAddPlayerTeam)                     
                                if(Object.values(myTeam.players).map((player, id) => {if(player.initial) return player.role}).includes(addPlayerTeam.role) &&  addPlayerTeam.initial) {
                                    newAddPlayerTeam.repeatedPositionError = true
                                }
                                else {
                                    newAddPlayerTeam.repeatedPositionError = false
                                }
                                setAddPlayerTeam(newAddPlayerTeam)
                            }}
                        />
                    }
                    
                </Form.Field>
                {
                    addPlayerTeam.repeatedPositionError ? 
                        <p style={{color: 'red'}}>Ya existe un jugador con la posición inicial seleccionada, por favor elija otra</p>
                    : null
                }
                <Form.Field>
                    <label htmlFor={`image`} className="form-teams__label">Forma</label>
                    <select name={`image`} id="" className="form-teams__select" required>
                        <option value="Círculo">Círculo</option>
                        <option value="Cuadrado">Cuadrado</option>
                        <option value="Triángulo">Triángulo</option>
                        <option value="Jugador (con brazo derecho)">Jugador (con brazo derecho)</option>
                        <option value="Jugador (con brazo izquierdo)">Jugador con brazo izquierdo)</option>
                        <option value="Jugador (con pelota en brazo derecho)">Jugador (con pelota en brazo derecho)</option>
                        <option value="Jugador (con pelota en brazo izquierdo)">Jugador (con pelota en brazo izquierdo)</option>
                        <option value="Portero">Portero</option>
                    </select>
                </Form.Field>
            </div>
            <Button disabled={addPlayerTeam.repeatedNumberError || addPlayerTeam.repeatedPositionError}>Añadir jugador</Button>
            {
                loading ? 
                <Loader
                    type="TailSpin"
                    color="#00BFFF"
                    height={100}
                    width={100}
                    timeout={900000000} 
                    className="config-panel__loader-spinner"
                />
                :
                null
            }
        </Form>
        </Modal.Content>
        <Modal.Actions>
            <Button onClick={() => setOpenAddPlayerModal(false)}>Cancel</Button>
        </Modal.Actions>
        </Modal>

        <div className="config-panel" style={panels[6].showPanel ? { transform: "scaleX(1)", zIndex: 10 } : { transform: "scaleX(0)" }}>
            <div className="draw-panel__exit-container" onClick={() => {togglePanel(6)}}>
                <FontAwesomeIcon icon={faTimes} color="#075f60" size="2x" className="draw-panel__exit-item" />
            </div>
            <div className="config-panel__buttons-container">
                <button 
                    className="config-panel__button-1" 
                    style={
                        panel === "General" ? {backgroundImage: `url(${configButton1Active})`}
                                            : { backgroundImage: `url(${configButton1Inactive})`}
                    } 
                    onClick={() => {setPanel("General")}}
                >
                    General
                </button>
                <button 
                    className="config-panel__button-2" 
                    style={
                        panel === "Equipos" ? { backgroundImage: `url(${configButton2Active})`}
                                            : { backgroundImage: `url(${configButton2Inactive})`}
                    } 
                    onClick={() => {setPanel("Equipos")}}
                >
                    Equipos
                </button>
            </div>
            <div className="config-panel__general" style={panel === "General" ? {display: "block"} : {display: "none"}}>
                <h2 className="config-panel__subtitle">Formato y color de pista</h2>
                <ul className="config-panel__list">
                    {
                        backgroundFormat.map((image, key) => {
                            return (
                                <li className="config-panel__item" onClick={() => { changeBackground(image.event) }} key={key}>
                                    <div className={`config-panel__item-image-${key + 1}`}></div>
                                </li>
                            )
                        })
                    }
                </ul>
                <h2 className="config-panel__subtitle">Jugadores</h2>
                <ul>

                </ul>
                <h2 className="config-panel__subtitle">Tamaño</h2>
                <ul className="config-panel__size-list">
                    <li className="config-panel__size-item" onClick={() => { changeObjectSize("normal") } }>
                        <div className="config-panel__size-item big">Normal</div>
                    </li>
                    <li className="config-panel__size-item" onClick={() => { changeObjectSize("medium") } }>
                        <div className="config-panel__size-item medium">Mediano</div>
                    </li>
                    <li className="config-panel__size-item" onClick={() => { changeObjectSize("small") } }>
                        <div className="config-panel__size-item small">Pequeño</div>
                    </li>
                </ul>
            </div>

            {/* ---------------------------------- Panel de configuración de equipos --------------------------- */}

            <div className="config-panel__teams" style={panel === "Equipos" ? { display: "block" } : { display: "none" }}>
                <h2 className="config-panel__subtitle">Gestionar equipos</h2>
                <Button onClick={() => { setOpenCreateTeam(true); setPlayers({}); setMyTeam() }}>Crear equipo</Button>

                <h2 className="config-panel__subtitle">Mi equipo</h2>
                {
                    currentUser.myTeam ? 
                        <div className="config-panel__my-team">
                            <span className="config-panel__my-team-span">{currentUser.myTeam.teamName}</span>
                            <Button onClick={() => { addCustomTeam("customAttackTeam", currentUser.myTeam); setMyTeam() }}>Equipo atacante</Button> 
                            <Button onClick={() => { addCustomTeam("customDefenseTeam", currentUser.myTeam); setMyTeam() }}>Equipo defensor</Button>
                            <Button href="#edit-teams" onClick={() => { setMyTeam(currentUser.myTeam); setUpdateColor(currentUser.myTeam.color) }}>Editar equipo</Button>
                        </div>
                    : null
                }
                        <>
                        <br/>
                        <p>Elija uno de los equipos creados para configurarlo como mi equipo</p>
                        <form onSubmit={handleMyTeam}>
                            <Select name="team" defaultValue={{label: currentUser.myTeam ? currentUser.myTeam.teamName : '', value: currentUser.myTeam ? currentUser.myTeam.teamName : ''}} options={handleTeamOptions(currentUser).teams} className="config-panel__select-my-team" />
                            <Button>Guardar</Button>
                        </form>
                        </>
                

                <h2 className="config-panel__subtitle">Lista de equipos creados</h2>
                <ul className="">
                    {
                        currentUser ?
                        Object.values(currentUser.customTeams).map((team, key) => {
                            return (
                                <li key={key} className="config-panel__teams-item">
                                    <span className="config-panel__teams-title">{team.teamName}</span> 
                                    <Button onClick={() => { addCustomTeam("customAttackTeam", team); setMyTeam() }}>Equipo atacante</Button> 
                                    <Button onClick={() => { addCustomTeam("customDefenseTeam", team); setMyTeam() }}>Equipo defensor</Button>
                                    <Button href="#edit-teams" className="config-panel__teams-button" onClick={() => { setMyTeam(team); setUpdateColor(team.color) }}>Editar equipo</Button>
                                    <Button onClick={() => { handleDeleteTeam(key); setMyTeam() }}>Borrar equipo</Button>
                                </li>
                            )
                        })
                        : null
                    }
                </ul>
                <div className="config-panel__edit" id="edit-teams">
                {
                    myTeam ? 
                        <>
                        <h2 className="config-panel__subtitle">Gestionar posiciones</h2>
                        <form className="" onSubmit={(e) => handleSubmit(e)}>
                             <h3>{myTeam.teamName}</h3> 
                             <label htmlFor={`teamColor`} className="form-teams__label">Color del equipo</label>
                             <input
                                 type="color" 
                                 name={`teamColor`} 
                                 className="form-teams__input"
                                 id={`teamColor`} 
                                 value={updateColor}
                                 onChange={(e) => { setUpdateColor(e.target.value) }}
                             />
                             <Accordion className="form-teams__player">
                                {
                                    myTeam.players ?
                                    Object.values(myTeam.players).map((player, key) => {
                                        return (
                                            <React.Fragment key={key}>
                                                <Accordion.Title
                                                    active={expandPlayer === key}
                                                    index={key}
                                                    onClick={() => { 
                                                        Object.values(myTeam.players).map(player => player.repeatedNumberError).includes(true) || Object.values(myTeam.players).map(player => player.repeatedPositionError).includes(true) ? 
                                                            alertify.alert("Por favor corrija los errores para continuar editando el equipo")
                                                        : expandPlayer === key ? setExpandPlayer(undefined) : setExpandPlayer(key)}}
                                                >
                                                    <div className="form-teams__dropdown-group">
                                                        <Icon name='dropdown' />
                                                        <p>{player.name}</p>
                                                    </div>
                                                    <p><span>{player.role}, </span><span>{player.number}</span></p>
                                                </Accordion.Title>
                                                <Accordion.Content active={expandPlayer === key}>
                                                    <input type="button" value="Borrar jugador" className="form-teams__delete" onClick={() => handleDeleteMyPlayer(key)}/>
                                                    <label htmlFor={`name${key}`} className="form-teams__label">Nombre del jugador</label>
                                                    <input
                                                        type="text" 
                                                        name={`name${key}`} 
                                                        className="form-teams__input"
                                                        id={`name${key}`} 
                                                        value={player.name}
                                                        onChange={(e) => {
                                                            console.log(e)
                                                            let newTeam = produce(myTeam, myTeamUpdated => {
                                                                myTeamUpdated.players[key].name = e.target.value
                                                            })

                                                            setMyTeam(newTeam)
                                                        }}
                                                    />
                                                    <label htmlFor={`number${key}`} className="form-teams__label">Número del jugador</label>
                                                    <input
                                                        type="number" 
                                                        name={`number${key}`} 
                                                        className="form-teams__input"
                                                        id={`number${key}`} 
                                                        value={player.number}
                                                        onChange={(e) => {
                                                            let newTeam = myTeam
                                                            newTeam.players[key].number = e.target.value
                                                            if(Object.values(myTeam.players).map((player, id) => {if(id !== key) return player.number}).includes(e.target.value)) {
                                                                newTeam.players[key].repeatedNumberError = true
                                                            }
                                                            else {
                                                                newTeam.players[key].repeatedNumberError = false
                                                            }
                                                            setMyTeam(newTeam)
                                                        }}
                                                    />
                                                    {
                                                        myTeam.players[key].repeatedNumberError ?
                                                            <p style={{color: 'red'}}>Ya existe un jugador con el número seleccionado, por favor elija otro</p>
                                                        : null
                                                    }
                                                    <label htmlFor={`role${key}`} className="form-teams__label">Posición</label>
                                                    <Select name={`role${key}`} defaultValue={handlePlayerPositions(player).currentPosition} options={handlePlayerPositions(player).positions} onChange={(position) => handlePositionEditTeam(position, key)} />
                                                    <label htmlFor={`initial${key}`} className="form-teams__label">¿Jugador inicial?</label>
                                                    {
                                                        player.initial ?
                                                        <input
                                                            type="checkbox"
                                                            name={`initial${key}`} 
                                                            id={`initial${key}`} 
                                                            className="form-teams__checkbox"
                                                            defaultChecked
                                                            onChange={(e) => {
                                                                e.persist()
                                                                let newPlayers = myTeam.players
                                                                newPlayers[key].initial = e.target.checked
                                                                setMyTeam({...myTeam, players: newPlayers})
                                                                if(Object.values(myTeam.players).map((player, id) => {if(id !== key && player.initial) return player.role}).includes(player.role) && e.target.checked) {
                                                                    newPlayers[key].repeatedPositionError = true
                                                                }
                                                                else {
                                                                    newPlayers[key].repeatedPositionError = false
                                                                }
                                                                setMyTeam({...myTeam, players: newPlayers})
                                                            }}
                                                        />
                                                        :
                                                        <input
                                                            type="checkbox"
                                                            name={`initial${key}`} 
                                                            id={`initial${key}`} 
                                                            className="form-teams__checkbox"
                                                            onChange={(e) => {
                                                                e.persist()
                                                                let newPlayers = myTeam.players
                                                                newPlayers[key].initial = e.target.checked
                                                                setMyTeam({...myTeam, players: newPlayers})
                                                                if(Object.values(myTeam.players).map((player, id) => {if(id !== key && player.initial) return player.role}).includes(player.role) && e.target.checked) {
                                                                    newPlayers[key].repeatedPositionError = true
                                                                }
                                                                else {
                                                                    newPlayers[key].repeatedPositionError = false
                                                                }
                                                                setMyTeam({...myTeam, players: newPlayers})
                                                            }}
                                                        />
                                                    }
                                                    {
                                                        myTeam.players[key].repeatedPositionError ?
                                                            <p style={{color: 'red'}}>Ya existe un jugador inicial con la posición seleccionada, por favor elija otra</p>
                                                        : null
                                                    }
                                                    <label htmlFor={`image${key}`} className="form-teams__label">Forma</label>
                                                    <Select name={`image${key}`} defaultValue={handlePlayerImages(player).currentImage} options={handlePlayerImages(player).images}/>
                                                </Accordion.Content>
                                        </React.Fragment>
                                        )
                                    })
                                    : console.log(myTeam)
                                }
                             </Accordion>
                            <input 
                                type="button" 
                                value="Añadir jugador" 
                                className="form-teams__add-player"  
                                style={Object.values(myTeam.players).map(player => player.repeatedNumberError).includes(true) || Object.values(myTeam.players).map(player => player.repeatedPositionError).includes(true) ? {display: 'none'} : {display: 'block'}} 
                                onClick={() => { 
                                    setOpenAddPlayerModal(true) 
                                    setAddPlayerTeam({
                                        name: "",
                                        number: "",
                                        role: "Portero",
                                        initial: false,
                                        image: "Círculo",
                                        repeatedPositionError: false,
                                        repeatedNumberError: false,
                                        position: {
                                            x: 0,
                                            y: 0
                                        }
                                    }) 
                                }}
                            />
                            <Button disabled={Object.values(myTeam.players).map(player => player.repeatedNumberError).includes(true) || Object.values(myTeam.players).map(player => player.repeatedPositionError).includes(true)}>Guardar cambios</Button>
                        </form>
                        </>
                    :
                        <>
                        <h2 className="config-panel__subtitle">Gestionar posiciones</h2>
                        <p>Seleccione un equipo para editarlo con el botón "Editar equipo"</p>
                        </>
                }
                </div>
            </div>
        </div>
        {
            loading ? 
            <Loader
                type="TailSpin"
                color="#00BFFF"
                height={100}
                width={100}
                timeout={900000000} 
                className="config-panel__loader-spinner"
            />
            :
            null
        }
        </>
    );
}

const mapStateToProps = state => {
    return {
        panels: state.panels,
        currentUser: state.currentUser,
        selectedCustomTeam: state.selectedCustomTeam,
        history: state.history,
        historyIndex: state.historyIndex,
        user: state.currentUser,
        id: state.currentUser.id
    }
}

const mapDispatchToProps = dispatch => {
    return {
        changeBackground: (background) => { dispatch(changeBackground(background)) },
        togglePanel: (panel) => {dispatch(togglePanel(panel))},
        changeObjectSize: (size) => { dispatch(changeObjectSize(size)) },
        addCustomTeam: (selectedTeam, customTeam) => { dispatch(addCustomTeam(selectedTeam, customTeam)) },
        addSelectedCustomTeam: (team) => { dispatch(addSelectedCustomTeam(team)) },
        addMyTeam: (team) => { dispatch(addMyTeam(team)) },
        updateEditedTeam: (team, teamType) => { dispatch(updateEditedTeam(team, teamType))},
        deleteCustomTem: (teamType) => { dispatch(deleteCustomTeam(teamType)) }
    }
}
 
export default connect(mapStateToProps, mapDispatchToProps)(ConfigPanel);
