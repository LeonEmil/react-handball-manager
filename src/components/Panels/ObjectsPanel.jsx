import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimes } from '@fortawesome/free-solid-svg-icons'
import { connect } from 'react-redux'
import { togglePanel, toggleDefaultObjects } from './../../redux/actionCreators'

let objects = [
    {
        event: '0',
    },
    {
        event: '1',
    },
    {
        event: '2',
    },
    {
        event: '3',
    },
    {
        event: '4',
    },
    {
        event: '5',
    },
    {
        event: '6',
    },
    {
        event: '7',
    },
    {
        event: '8',
    },
    {
        event: '9',
    },
    {
        event: '10',
    },
    {
        event: '11',
    },
    {
        event: '12',
    },
    {
        event: '13',
    },
    {
        event: '14',
    },
    {
        event: '15',
    },
    {
        event: '16',
    },
    {
        event: '17',
    },
    {
        event: '18',
    },
    {
        event: '19',
    },
    {
        event: '20',
    },
    {
        event: '21',
    },
    {
        event: '22',
    },
    {
        event: '23',
    },
    {
        event: '24',
    },
]


const ObjectsPanel = ({panels, togglePanel, toggleDefaultObjects, menuRef, defaultObjects, configData}) => {
    const panelSize = window.innerWidth < 576 ? '-300px' : '-500px'
    return (  
        <div className="objects-panel" style={panels[4].showPanel ? { left: `${menuRef.current.offsetWidth}px`, zIndex: 10 } : { left: panelSize }}>
            <div className="draw-panel__exit-container" onClick={() => { togglePanel(4) }}>
                <FontAwesomeIcon icon={faTimes} color="#075f60" size="2x" className="draw-panel__exit-item" />
            </div>
            <h2 className="objects-panel__subtitle">Entrenamientos</h2>
            <ul className="objects-panel__list">
                {
                    objects.map((object, key) => {
                        return (
                            <li className="object-panel__item" onClick={() => { toggleDefaultObjects(object.event) }} key={key}>
                                <span className="object-panel__item-counter">{Object.keys(defaultObjects[object.event]).length}</span>
                                <div className={`object-panel__item-image-${key + 1}`}></div>
                            </li>
                        )
                    })
                }
            </ul>
        </div>
    );
}

const mapStateToProps = (state) => {
    return {
        panels: state.panels,
        defaultObjects: state.history[state.historyIndex].defaultObjects,
        configData: state.configData
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        toggleDefaultObjects: (objectType) => {dispatch(toggleDefaultObjects(objectType))},
        togglePanel: (panel) => { dispatch(togglePanel(panel)) }
    }
}
 
export default connect(mapStateToProps, mapDispatchToProps)(ObjectsPanel);