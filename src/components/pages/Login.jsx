import React, { useState } from 'react';
import { Redirect } from 'react-router-dom'
import { auth, db } from './../../firebase/firebase'
import { setUser, restoreHistory } from './../../redux/actionCreators'
import initialHistory from './../../js/history'
import alertify from 'alertifyjs'
import { useFormik } from 'formik'
import * as Yup from 'yup'

const Login = () => {

    const [redirect, setRedirect] = useState(false)

    const { handleSubmit, handleChange, handleBlur, values, errors, touched } = useFormik({
        initialValues: {
            email: 'test@email.com',
            password: '123456'
        },
        validationSchema: Yup.object({
            email: Yup.string().email("Por favor ingrese un email válido").required('Por favor ingrese su email'),
            password: Yup.string().required('Por favor ingrese su contraseña')
        }),
        onSubmit: ({ email, password }) => {
            auth.signInWithEmailAndPassword(email, password)
                .then(() => {
                    db.collection('users').where('email', '==', email).get()
                    .then(response => {
                        if(response.docs[0]){
                            let currentUser = {
                            name: response.docs[0].data().name,
                            email: response.docs[0].data().email,
                            avatar: response.docs[0].data().avatar,
                            strategies: response.docs[0].data().strategies,
                            videos: response.docs[0].data().videos,
                            role: response.docs[0].data().role,
                            myTeam: response.docs[0].data().myTeam,
                            customTeams: response.docs[0].data().customTeams,
                            otherData: response.docs[0].data().otherData,
                            id: response.docs[0].data().id
                            }
                            setUser(currentUser)
                
                            // Cargar última formación
                            if(response.docs[0].data().history !== initialHistory && this.props.currentUser){
                                alertify.confirm("¿Desea cargar la última formación guardada?", () => {
                                    restoreHistory(response.docs[0].data().history, response.docs[0].data().historyIndex)
                                })
                            }
                        }
                    })
                    .catch(() => {
                        alertify.alert('Parece que hay problemas con su conexión a internet. Por favor recargue la página o espere unos minutos')
                    })

                    setRedirect(true)
                    window.location.reload()
                })
                .catch(error => {
                    switch (error.message) {
                        case "There is no user record corresponding to this identifier. The user may have been deleted.":
                            alert("No se encontró al usuario correspondiente a los datos proporcionados. Intentelo nuevamente")
                            break;
                        case "The password is invalid or the user does not have a password.": 
                            alert("La contraseña es incorrecta. Intentelo nuevamente")
                            break;
                        default:
                            alert(error)
                            break;
                    }
                })
        }
    })

    return redirect ? (<Redirect to="/" />) : (
        <div className="login-page">
            <div className="form-image"></div>
            <form className="login__form" onSubmit={handleSubmit}>
                <h1 className="login__title">Ingresar</h1>

                <label htmlFor="email" className="login__label">Email</label>
                <input
                    onChange={handleChange}
                    onBlur={handleBlur}
                    type="email"
                    className="login__input"
                    id="login-email"
                    name="email"
                    value={values.email}
                    required
                />
                {
                    errors.email && touched.email ?
                        <span className="login__alert">{errors.email}</span>
                        : null
                }

                <label htmlFor="password" className="login__label">Contraseña</label>
                <input
                    onChange={handleChange}
                    onBlur={handleBlur}
                    type="password"
                    className="login__input"
                    id="login-password"
                    name="password"
                    value={values.password}
                    required
                />
                {
                    errors.password && touched.password ?
                        <span className="login__alert">{errors.password}</span>
                        : null
                }

                <input
                    type="submit"
                    value="Continuar"
                    className="login__button"
                />
            </form>
        </div>
    );
}

export default Login;
